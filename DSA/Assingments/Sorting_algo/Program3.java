import java.util.*;

class MergeSortedArrays {


	int[] merge(int arr1[],int arr2[]){
		
		int i=0,j=0,k=0;
		int arr3[] = new int[arr1.length+arr2.length];

		while(i<arr1.length && j<arr2.length){
		
			if(arr1[i] < arr2[j]){
			
				arr3[k] = arr2[j];
				j++;
			}else{
			
				arr3[k] = arr1[i];
				i++;
			}
			k++;
		}

		while(i<arr1.length){
		
			arr3[k] = arr1[i];
			i++;
			k++;
		}
		while(j<arr2.length){
		
			arr3[k] = arr2[j];
			j++;
			k++;
		}

		return arr3;

	}

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size for first Array");
		int size1 = sc.nextInt();
		
		System.out.println("Enter Size for second Array");
		int size2 = sc.nextInt();

		int arr1[] = new int[size1];
		int arr2[] = new int[size2];

		System.out.println("Enter Elements for first array in sorted order[ascending]");
		for(int i=0; i<size1; i++){
		
			arr1[i] = sc.nextInt();
		}
		System.out.println("Enter Elements for second array in sorted order[ascending]");
		for(int i=0; i<size2; i++){
		
			arr2[i] = sc.nextInt();
		}

		MergeSortedArrays obj = new MergeSortedArrays();

		arr1 = obj.merge(arr1,arr2);
		
		System.out.println("Sorted Array : ");
		for(int i=0; i<arr1.length; i++){
		
			System.out.print(arr1[i]+"  ");
		}
		System.out.println();
	}
}

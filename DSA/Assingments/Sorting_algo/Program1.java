import java.util.*;

class BubbleSort {

	void bubbleSort(int arr[]) {
	
		for(int i=0; i<arr.length; i++){
			
			// for(int j=0; j,arr.length-i-1; j++){
			for(int j=0; j<arr.length-1; j++){
			
				if(arr[j] > arr[j+1]){
				
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
	}

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size for Array");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements for array");
		for(int i=0; i<size; i++){
		
			arr[i] = sc.nextInt();
		}

		BubbleSort obj = new BubbleSort();

		obj.bubbleSort(arr);
		
		System.out.println("Sorted Array : ");
		for(int i=0; i<size; i++){
		
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}

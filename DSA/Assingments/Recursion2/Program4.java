// WAP to print sum of odd numbers up to given number

import java.util.*;

class Demo {
	
	int sum1(int num){
	
		int sum = 0;
		for(int i=1; i<=num; i++){
			if(i%2 == 1)
				sum = sum+i;
		}
		return sum;
	}

	int sum2(int num,int sum) {
			
		if(num == 0)
			return sum;

		if(num%2 == 1)
			sum = sum+num;
		return sum2(--num,sum);
	}
	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		
		int sum = 0;
		Demo obj = new Demo();
		int sum1 = obj.sum1(num);
		int sum2 = obj.sum2(num,sum);
		
		System.out.println(sum1);
		System.out.println(sum2);
	}
}



// WAP to print the given number is strong or not
// 
import java.util.*;

class Demo {

	int strong1(int num) {
		int sum  =0;
		while(num!=0){
			int fact = 1;
			int rem = num%10;

			while(rem!=0){
				fact = fact*rem;
				rem--;
			}
			sum = sum+fact;
			num=num/10;
		}
		return sum;
	}
	static int calculateFactorial(int num,int fact){
	
		if(num == 0)
			return fact;

		fact = fact*num;
		return calculateFactorial(--num,fact);
	}
	int strong2(int num,int sum){

		if(num==0)
			return sum;
		else {
			int rem = num%10;
			int fact =1;
			fact = calculateFactorial(rem,fact);
			sum = sum+fact;
			
			return strong2(num/10,sum);
		}
	}
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		
		int sum = 0;
		Demo obj = new Demo();
		int ret1 = obj.strong1(num);
		int ret2 = obj.strong2(num,sum);
		System.out.println(ret2);
		if(num == ret1 && num == ret2)
			System.out.println(num+" is Strong Number");
		else
			System.out.println(num+" is not Strong Number");
	}
}

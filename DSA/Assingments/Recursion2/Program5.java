 
// check whether is palindrome or not

import java.util.*;
class Demo {
	
	boolean palindrome(int num) {
	
		int rev = 0;
		int temp = num;
		while(temp != 0){
		
			int rem = temp%10;
			rev = rev*10 + rem;
			temp = temp/10;
		}

		if(num == rev)
			return true;
		else 
			return false;
	}

	int palindrome1(int num,int rev) {
	
		if(num == 0)
			return rev;

		rev = rev*10 + num%10;

		return palindrome1(num/10,rev);
	}
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number");
		int num = sc.nextInt();
		
		int rev = 0;
		Demo obj = new Demo();
		boolean ret1 = obj.palindrome(num);
		int ret2 = obj.palindrome1(num,rev);

		if(ret2 == num && ret1 == true)
			System.out.println(num+" is Palindrome Number");
		else
			System.out.println(num+" is not Palindrome Number");
	}
}


// print the products of digits of number
//
import java.util.*;
class Demo{


	int pro1(int num){
	
		int pro = 1;
		while(num!=0){
		
			pro = pro*num%10;
			num = num/10;
		}
		return pro;
	}

	int pro2(int num,int pro){

		if(num == 0)
			return pro;
		pro = pro * num%10; 
		return pro2(num/10,pro);

	}
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		Demo obj = new Demo();
		
		int pro = 1;
		int ret1 = obj.pro1(num);
		int ret2 = obj.pro2(num,pro);
		System.out.println(ret1);
		System.out.println(ret2);
	}
}

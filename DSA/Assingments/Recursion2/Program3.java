
// print the maximum digit in given number
//
import java.util.*;
class Demo{


	int max1(int num){
	
		int max = Integer.MIN_VALUE;
		while(num!=0){
		
			if(num%10 > max)
				max = num%10;
			
			num = num/10;
		}
		return max;
	}

	int max2(int num,int max){

		if(num == 0)
			return max;
		if(num%10 > max)
			max = num%10;

		return max2(num/10,max);

	}
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		Demo obj = new Demo();
		int max = Integer.MIN_VALUE;

		int ret1 = obj.max1(num);
		int ret2 = obj.max2(num,max);
		System.out.println(ret1);
		System.out.println(ret2);
	}
}

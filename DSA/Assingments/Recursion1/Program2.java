
//  first 10 natural numbers in reverse order


class Demo {

	static void printRev(int num) {
	
		if(num == 0)
			return;

		System.out.println(num);
		printRev(--num);
	}
	public static void main(String[] args) {
	
		int num = 10;

		printRev(num);
	}
}

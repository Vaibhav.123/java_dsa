
//
//sum of digits of given posituve number
import java.util.*;
class Demo {
	
	// using loop
/*	int sumDigit(int num) {
	
		int sum = 0;
		while(num!=0){
		
			sum = sum+num%10;
			num = num/10;
		}
		return sum;
	}*/
	// using recursion
	int sum = 0;
	int sumDigit(int num) {
	
		if(num == 0)
			return sum;
		sum = sum + num%10;
		return sumDigit(num/10);
	}
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		Demo obj = new Demo();
		int sum = obj.sumDigit(num);

		System.out.println(sum);
	}
}

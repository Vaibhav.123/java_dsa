

// length pf digits in number


class Demo {
	// using while loop
/*	int len(int num) {
		
		int count = 0;
		while(num != 0){
		
			count++;
			num = num/10;
		}
		return count;
	}
*/
	// using recursion
	int count =0;
	int len(int num) {
	
		if(num == 0)
			return 0;
		count++;
		len(num/10);
		return count;
	}
	public static void main(String[] args){
	
		int num = 12123;
		Demo obj = new Demo();
		int len = obj.len(num);
		System.out.println(len);
	}

}

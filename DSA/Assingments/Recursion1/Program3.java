
// sum of nautral numbers 


class Demo{

	int sum(int num){

		if(num == 1)
			return 1;
		
		return num+sum(--num);

	}
	public static void main(String[] args) {
	
		int num = 10;
		Demo obj = new Demo();

		int sum = obj.sum(num);
		System.out.println(sum);
	}
}


// number is prime or not

import java.util.*;
class Demo {

	// using for loop
/*	int checkPrime(int num){
		
		int count = 2;
		
		for(int i=2; i<=num/2; i++){
		
			if(num%i == 0)
				count++;

			if(count > 2)
				return 1;
		}
		return 0;
	}*/

	// using recursion
	int checkPrime(int num, int i) {

		if(num <= 2)
			return 0;

		if(num%i == 0)
			return 1;

		if(i*i > num)
			return 0;

		return checkPrime(num,i+1);
	}

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		
		Demo obj = new Demo();
		int ret = obj.checkPrime(num,2);

		if(ret == 0)
			System.out.println(num +" is Prime Number");
		else	
			System.out.println(num +" is Not Prime Number");
	}
}



// occurence of specific digit in given number

import java.util.*;
class Demo {
	
	int count=0;
	int occurCount(int num,int occ) {

		if(num == 0)
			return count;
		if(num%10 == occ)
			count++;

		return occurCount(num/10,occ);
	}
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number");
		int num = sc.nextInt();
		System.out.println("Enter number for check occurence");
		int temp = sc.nextInt();

		Demo obj = new Demo();
		int count = obj.occurCount(num,temp);

		System.out.println(count);
	}
}


// print string in reverse order


class Demo {
	void revStr(String str) {
		char arr[] = str.toCharArray();
		int start = 0;
		int end = arr.length-1;

		while(start < end){
		
			char temp = arr[start];
			arr[start] = arr[end];
			arr[end] = temp;
			start++;
			end--;
		}
		System.out.println(arr);
	}
	String revStr1(String str) {
	
		if(str == null || str.length() <= 1)
			return str;
		
		return revStr1(str.substring(1)) + str.charAt(0);
	}
	public static void main(String[] args) {
	
		String str = "Vaibhav";
		System.out.println(str);
		Demo obj = new Demo();
		obj.revStr(str);
		String str2 = obj.revStr1(str);
		System.out.println(str2);
	}
}

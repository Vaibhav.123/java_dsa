// print 1 to 10

class Demo {
	// tail recursion
/*	static void printNum(int num){
	
		if(num == 0){
			return;
		}
		printNum(--num);
		System.out.println(++num);
	}
*/
	// non tail recursion
	static void printNum(int num) {
	
		if(num == 0){
		
			return ;
		}
		System.out.println(num);
		printNum(--num);
	}

	public static void main(String[] args) {
	
		int num = 10;
		printNum(num);
	}
}

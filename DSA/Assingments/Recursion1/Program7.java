

// factorial of given number
import java.util.*;
class Demo {
	
/*	// using loop
	int fact(int num) {
		int fact = 1;
		for(int i=1; i<=num; i++){
			fact = fact*i;
		}
		return fact;
	}
*/
	// using recursion
	
	int fact(int num) {
		
		if(num == 1)
			return 1;

		return num * fact(num-1);
	}
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		Demo obj = new Demo();
		int fact = obj.fact(num);

		System.out.println(fact);
	}
}

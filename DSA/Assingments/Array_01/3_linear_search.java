
import java.util.*;

class Demo {
	
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Elements");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		int ps[] = new int[arr.length];
		ps[0] = arr[0];
		for(int i=1; i<ps.length; i++){
			
			ps[i] = ps[i-1] + arr[i];
		}
		int sum = 0;
		System.out.println("Enter no of queries");
		int Q = sc.nextInt();

		for(int i=1; i<=Q; i++){
			System.out.println("Enter start and end");
			int start = sc.nextInt();
			int end = sc.nextInt();

			if(start == 0)
				sum = ps[end];
			else 
				sum = ps[end] - ps[start-1];
		
			System.out.println("Sum "+i +" = "+ sum);

		}
	}
}



import java.util.*;

class Queue {

	int queueArr[];
	int front;
	int rear;
	int maxSize;

	Queue(int size){
	
		this.queueArr = new int[size];
		this.front = -1;
		this.rear = -1;
		this.maxSize = size;
	}

	void enqueue(int data) {
	
		if(rear == maxSize-1){
		
			System.out.println("Queue Is Full");
			return;
		}

		if(front == -1) 
			front = rear = 0;
		else
			rear++;

		queueArr[rear] = data;
	}
	int dequeue(){
	
		if(front == -1){
		
			System.out.println("Queue is Empty");
			return -1;

		}else{
			int val = queueArr[front];

			if(front == rear){
				front = rear = -1;
				return val;
			}else{
				front++;
				return val;
			}
		}
	}

	boolean empty(){
	
		if(front == -1)
			return true;
		else 
			return false;
	}
	int front(){
	
		if(front == -1){
			System.out.println("Queue Is Empty");
			return -1;
		}else{
			int ret = queueArr[front];

			return ret;
		}
	}
	int rear(){
	
		if(front == -1){
			System.out.println("Queue Is Empty");
			return -1;
		}else{
			int ret = queueArr[rear];

			return ret;
		}
		

	}

	boolean reverseQueue(){
	
		if(front == -1)
			return true;
		else{
			int temp = rear;
			for(int i=front; i<=(rear-1)/2; i++){
			
				int rev = queueArr[i];
				queueArr[i] = queueArr[temp];
				queueArr[temp] = rev;
				temp--;
			}
			return false;
		}

	}

	int reverseK(int ele, int k){
	
		if(front == -1)
			return -1;
		else {
			int flag = 0;
			int index = -1;
			for(int i=front; i<=rear;i++){
				System.out.println("here");
				if(queueArr[i] == ele){
					flag = 1;
					index = i;
					break;
				}
			}

			if(flag == 0){
				System.out.println("Element is not present in queue");
				return 0;
			}else {
				
				int j = index;
				for(int i=1; i<=k; i++){
				
					if(j == maxSize-1){
						j = front;
					}
					j++;
				}
				int temp = queueArr[index];
				queueArr[index] = queueArr[j];
				queueArr[j] = temp;
				return 0;
			}
		}
		
	}

	void printQueue(){
	
		if(front == -1) {
		
			System.out.println("Queue is Empty");
			return;
		}else{
			
			int temp = front;
			System.out.print("[ ");
			while(temp != rear){
			
				System.out.print(queueArr[temp]+" ");
				temp++;
			}
			System.out.println(queueArr[temp]+" ]");
		}
	}

}

class Client {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size for queue");
		int size = sc.nextInt();
		
		Queue obj = new Queue(size);

		char ch;
		do{
		
			System.out.println("1.Enqueue");
			System.out.println("2.Dequeue");
			System.out.println("3.Empty");
			System.out.println("4.Front");
			System.out.println("5.Rear");
			System.out.println("6.printQueue");
			System.out.println("7.reverseQueue");
			System.out.println("8.reverse first k elements of Queue");

			System.out.println("Enter your choice");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1:
					 {
					 
						 System.out.println("Enter data for enqueue");
						 int data = sc.nextInt();
						 obj.enqueue(data);
					 }
					 break;

				case 2:
					 {
					 
						 int ret = obj.dequeue();
						 if(ret != -1)
							 System.out.println(ret +" is Dequeued");
					 }
					 break;

				case 3:
					 {
					 	boolean ret = obj.empty();
						if(ret)
							System.out.println("Queue Is Empty");
						else
							System.out.println("Queue Is Not Empty");
					 }
					 break;

				case 4:
					 {
						int ret = obj.front();
						if(ret != -1)
							System.out.println("Front Element is "+ ret);

				       	 }
					 break;

				case 5: 
					 {
					 
						 int ret = obj.rear();
						 if(ret != -1)
							 System.out.println("Rear Element is "+ret);
					 }
					 break;

				case 6: 
					 {
					 
						 obj.printQueue();
					 }
					 break;

				case 7: 
					 {
						 boolean ret = obj.reverseQueue();
						 if(ret){
						 	System.out.println("Queue is empty");
						 }
					 }
					 break;

				case 8:
					 {
					 		
						 System.out.println("Enter element to start from that");
						 int ele = sc.nextInt();
						 
						 System.out.println("Enter k value");
						 int k = sc.nextInt();
							
						 int ret = obj.reverseK(ele,k);
						 if(ret == -1)
							 System.out.println("Queue is empty");
					 }
					 break;

				default:
					 {
					 	System.out.println("Wrong Choice");
	
					 }
					 break;
			}

			System.out.println("Continue?");
			ch = sc.next().charAt(0);

		}while(ch == 'y' || ch == 'Y');
	}
}

















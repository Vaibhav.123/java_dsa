import java.util.*;

class Reverse_Queue {

	int front;
	int rear;
	int maxSize;
	int queueArr[];

	Reverse_Queue(int size){
	
		this.rear = this.front = -1;
		this.maxSize = size;
		this.queueArr = new int[size];
	}

	void enqueue(int data) {
		
		if(front == -1)
			front = rear = 0;

		this.queueArr[rear] = data;
		rear++;
	}

	int[] reverseQueue(){
	
		int temp[] = new int[maxSize];
		int j= 0;
		for(int i=rear-1; i>=0;i--){
		
			temp[j++] = queueArr[i];
		}

		return temp;
	}
}

class Client {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size for queue");

		int size = sc.nextInt();

		Reverse_Queue obj = new Reverse_Queue(size);

		System.out.println("Enter data to enter in queue");

		for(int i=0; i<size; i++){
			
			int data = sc.nextInt();
			obj.enqueue(data);
		}

		int queueArr[] = new int[size];

		queueArr = obj.reverseQueue();
		
		System.out.print("[ ");

		for(int i=0; i<size; i++){	
			System.out.print(queueArr[i]+" ");
		}
		System.out.println("]");

			
	}
}


import java.util.*;

class Node {

	int data;
	Node next;

	Node(int data){
	
		this.data = data;
		this.next = null;
	}
}

class CircularQueueLL {

	Node rear;
	Node front;

	void enqueue(int data){
	
		Node newNode = new Node(data);

		if(this.front == null){
		
			this.front = this.rear = newNode;
			this.front.next = this.front;
			
		}else{
		
			this.rear.next = newNode;
			newNode.next = this.front;
			this.rear = newNode;
		}
	}

	int dequeue(){
	
		if(this.front == null){
		
			System.out.println("Queue Is Empty");
			return -1;
		}else{
		
			int ret = this.front.data;
			if(this.front.next == this.front){
			
				this.front = this.rear = null;
			
			}else{
				
				this.front = this.front.next;
				this.rear.next = this.front;
			}

			return ret;
		}
	}

	void printQueue(){
	
		if(this.front == null){
		
			System.out.println("Queue is Empty");
			return;
		}else{
		
			Node temp = this.front;


			while(temp.next != this.front){
			
				System.out.print(temp.data+" => ");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}
}

class Client {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		CircularQueueLL obj = new CircularQueueLL();

		char ch;

		do{
		
			System.out.println("1.Enqueue");
			System.out.println("2.Dequeue");
			System.out.println("1.printQueue");

			System.out.println("Enter your choice");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1 :
					{
					
						System.out.println("Enter data ");
						int data = sc.nextInt();

						obj.enqueue(data);
					}
					break;

				case 2: 
					{
					
						int ret = obj.dequeue();
						if(ret != -1)
							System.out.println(ret+" is Dequeued");
					}
					break;

				case 3:
					{
						obj.printQueue();
					}
					break;

				default:
					System.out.println("Wrong Choice");
					break;

			}

			System.out.println("Continue?");
			ch = sc.next().charAt(0);

		}while(ch == 'y' || ch == 'Y');
	}
}






















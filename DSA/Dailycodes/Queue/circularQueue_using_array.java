
import java.util.*;

class CircularQueue {

	int queueArr[];
	int maxSize;
	int front;
	int rear;

	CircularQueue(int size){
	
		this.queueArr = new int [size];
		this.maxSize = size;
		this.front = -1;
		this.rear = -1;
	}

	void enqueue(int data) {
	
		if((front == 0 && rear == maxSize-1) || (front == (rear+1) % maxSize)){
		
			System.out.println("Queue Is Full");
			return;

		}else if(front == -1){

			front = rear = 0;

		}else if(rear == maxSize-1 && front!=0){
		
			rear = 0;

		}else{
		
			rear++;

		}
		queueArr[rear] = data;
	}

	int dequeue(){
	
		if(front == -1){
		
			System.out.println("Queue IS Empty");
			return -1;
		}else {
		
			int val = queueArr[front];

			if(front == rear){
				
				front = rear = -1;

			}else if(front == maxSize-1){
			
				front = 0;
			}else
				front++;

			return val;
		}
	}
	void printQueue(){
	
		if(front == -1){
		
			System.out.println("Queue Is empty");
		}else {
		
			System.out.print("[ ");
			if(front <= rear){
			
				for(int i = front; i<=rear; i++){
				
					System.out.print(queueArr[i]+" ");
				}
			}else {
			
				for(int i=front; i<maxSize; i++){
				
					System.out.print(queueArr[i]+" ");
				}
				for(int i=0; i<=rear; i++){
				
					System.out.print(queueArr[i] + " ");
				}
			}
			System.out.println("]");
		}
	}
}

class Client {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size for Queue");
		int size = sc.nextInt();

		CircularQueue obj = new CircularQueue(size);

		char ch;

		do{
		
			System.out.println("1.Enqueue");
			System.out.println("2.Dequeue");
			System.out.println("1.printQueue");

			System.out.println("Enter your choice");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1 :
					{
					
						System.out.println("Enter data ");
						int data = sc.nextInt();

						obj.enqueue(data);
					}
					break;

				case 2: 
					{
					
						int ret = obj.dequeue();
						if(ret != -1)
							System.out.println(ret+" is Dequeued");
					}
					break;

				case 3:
					{
						obj.printQueue();
					}
					break;

				default:
					System.out.println("Wrong Choice");
					break;

			}

			System.out.println("Continue?");
			ch = sc.next().charAt(0);

		}while(ch == 'y' || ch == 'Y');
	}
}






















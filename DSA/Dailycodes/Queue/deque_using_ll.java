import java.util.*;

class DequeLL {

	class Node {
	
		int data;
		Node next;

		Node(int data) {
		
			this.data = data;
			this.next = null;
		}
	}

	Node rear;
	Node front;

	void pushBack(int data){
		
		Node newNode = new Node(data);

		if(this.rear == null){
			
			this.rear = this.front = newNode;
		}else{
		
			this.rear.next = newNode;
			this.rear = newNode;
		}
	}

	void pushFront(int data) {
	
		Node newNode = new Node(data);

		if(this.rear == null){
		
			this.rear = this.front = newNode;
		}else {
		
			newNode.next = this.front;
			this.front = newNode;
		}
	}

	int popFront(){
	
		if(this.rear == null){
		
			System.out.println("Queue Is Empty");
			return -1;

		}else {
			int ret = this.front.data;

			if(this.front.next == null){
				
				this.rear = this.front = null;	

			}else {
			
				this.front = this.front.next;
			}
			return ret;
		}
	}

	int popBack(){
	
		if(this.rear == null){
		
			System.out.println("Queue Is Empty");
			return -1;
		}else {
		
			int ret = this.rear.data;

			if(this.front.next == null){
				this.front = this.rear = null;
				System.out.println("here1");
			
			}else if(this.front.next.next == null){
			
				System.out.println("here2");
				this.rear = this.front;
				this.rear.next = null;
			}else{
				System.out.println("here3");
			
				Node temp = this.front;
				while(temp.next.next != null){
				
					temp = temp.next;
				}
				this.rear = temp;
				this.rear.next = null;
			}
			return ret;
		}
	}
	void printQueue(){
	
		if(this.front == null){
			System.out.println("Queue Is Empty");
			return;
		}else{
			
			Node temp = this.front;
			while(temp.next != null){
			
				System.out.print(temp.data+" => ");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}
}

class Client {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		char ch;

		DequeLL obj = new DequeLL();
		
		do{
			System.out.println("1. pushBack");
			System.out.println("2. pushFront");
			System.out.println("3. popBack");
			System.out.println("4. popFront");
			System.out.println("5. printQueue");

			System.out.println("Enter Your Choice");
			int choice = sc.nextInt();

			switch(choice) {
			
				case 1:
					{
					
						System.out.println("Enter data");
						int data = sc.nextInt();
						obj.pushBack(data);
					}
					break;

				case 2:
					{
					
						System.out.println("Enter data");
						int data = sc.nextInt();
						obj.pushFront(data);
					}
					break;

				case 3:
					{
						int ret = obj.popBack();
						if(ret != -1){
						
							System.out.println(ret+ " IS popped");
						}

					}
					break;

				case 4:
					{
					
						int ret = obj.popFront();
						if(ret != -1){
						
							System.out.println(ret+ " IS popped");
						}
					}
					break;

				case 5:
					{
					
						obj.printQueue();
					}
					break;

				default:
					System.out.println("Wrong Choice");
					break;
			}

			System.out.println("Continue?");
			ch = sc.next().charAt(0);

		}while(ch == 'y' || ch == 'Y');
	}
}

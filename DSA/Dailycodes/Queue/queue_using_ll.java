import java.util.*;

class QueueLL {

	class Node {
	
		int data;
		Node next;

		Node(int data) {
		
			this.data = data;
			this.next = null;
		}
	}

	Node rear;
	Node front;

	void enqueue(int data){
		
		Node newNode = new Node(data);

		if(this.rear == null){
			
			this.rear = this.front = newNode;
		}else{
		
			this.rear.next = newNode;
			this.rear = newNode;
		}
	}

	int dequeue(){
	
		if(this.rear == null){
		
			System.out.println("Queue Is Empty");
			return -1;

		}else {
			int ret = this.front.data;

			if(this.front.next == null){
				
				this.rear = this.front = null;	

			}else {
			
				this.front = this.front.next;
			}
			return ret;
		}
	}

	int front(){
	
		if(this.front == null){
		
			System.out.println("Queue is Empty");
			return -1;
		}else {
		
			return this.front.data;
		}
	}

	int rear(){
	
		if(this.front == null){
		
			System.out.println("Queue is Empty");
			return -1;
		}else {
		
			return this.rear.data;
		}
	}
	

	void printQueue(){
	
		if(this.front == null){
			System.out.println("Queue Is Empty");
			return;
		}else{
			
			Node temp = this.front;
			while(temp.next != null){
			
				System.out.print(temp.data+" => ");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}
}

class Client {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		char ch;

		QueueLL obj = new QueueLL();
		
		do{
			System.out.println("1. enqueue");
			System.out.println("2. dequeue");
			System.out.println("3. front");
			System.out.println("4. rear");
			System.out.println("5. printQueue");

			System.out.println("Enter Your Choice");
			int choice = sc.nextInt();

			switch(choice) {
			
				case 1:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();
						obj.enqueue(data);
					}
					break;

				case 2:
					{
					
						int ret = obj.dequeue();
						if(ret != -1){
						
							System.out.println(ret+ " IS popped");
						}
					}
					break;

				case 3:
					{
					
						int ret = obj.front();
						if(ret != -1){
							System.out.println("front = "+ret);
						}
					}
					break;

				case 4:
					{
					
						int ret = obj.rear();
						if(ret != -1){
							System.out.println("rear = "+ret);
						}
					}
					break;

				case 5:
					{
					
						obj.printQueue();
					}
					break;

				default:
					
					System.out.println("Wrong Choice");
					break;

			}

			System.out.println("Continue?");
			ch = sc.next().charAt(0);

		}while(ch == 'y' || ch == 'Y');
	}
}

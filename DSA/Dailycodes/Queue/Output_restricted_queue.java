
// Output Restricted Queue

import java.util.*;

class Deque {

	int queueArr[];
	int front;
	int rear;
	int maxSize;

	Deque(int size){
	
		this.queueArr = new int[size];
		this.front = -1;
		this.rear = -1;
		this.maxSize = size;
	}

	void pushBack(int data) {
	
		if(rear == maxSize-1){
		
			System.out.println("Queue Is Full");
			return;
		}

		if(front == -1) 
			front = rear = 0;
		else
			rear++;

		queueArr[rear] = data;
	}

	void pushFront(int data) {
	
		if(front == 0){
			
			System.out.println("Queue Is Full Cant insert from front");
			return;
		}

		if(front == -1){
		
			front = rear = 0;

		}else {
		
			front--;
		}

		queueArr[front] = data;
	}

	int popFront(){
	
		if(front == -1){
		
			System.out.println("Queue is Empty");
			return -1;

		}else{
			int val = queueArr[front];

			if(front == rear){
				front = rear = -1;
				return val;
			}else{
				front++;
				return val;
			}
		}
	}
	
	void printQueue(){
	
		if(front == -1) {
		
			System.out.println("Queue is Empty");
			return;
		}else{
			
			int temp = front;
			System.out.print("[ ");
			while(temp != rear){
			
				System.out.print(queueArr[temp]+" ");
				temp++;
			}
			System.out.println(queueArr[temp]+" ]");
		}
	}

	boolean empty(){
	
		if(front == -1)
			return true;
		else
			return false;
	}

}

class Client {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size for queue");
		int size = sc.nextInt();
		
		Deque obj = new Deque(size);

		char ch;
		do{
		
			System.out.println("1.pushBack");
			System.out.println("2.pushFront");
			System.out.println("3.empty");
			System.out.println("4.popFront");
			System.out.println("5.printQueue");

			System.out.println("Enter your choice");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1:
					 {
						 System.out.println("Enter data");
						 int data = sc.nextInt();
						 obj.pushBack(data);
					 }
					 break;

				case 2:
					 {
						 System.out.println("Enter data");
						 int data = sc.nextInt();
						 obj.pushFront(data);
					 }
					 break;

				case 3:
					 {
						 boolean ret = obj.empty();
						 if(ret)
							 System.out.println("Queue is Empty");
					 }
					 break;

				case 4:
					 {
						int ret = obj.popFront();
						if(ret != -1)
							System.out.println(ret +" is Dequeued");
					 }
					 break;

				case 5: 
					 {
					 
						 obj.printQueue();
					 }
					 break;

				default:
					 {
					 	System.out.println("Wrong Choice");
	
					 }
					 break;
			}

			System.out.println("Continue?");
			ch = sc.next().charAt(0);

		}while(ch == 'y' || ch == 'Y');
	}
}

















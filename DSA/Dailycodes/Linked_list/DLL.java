import java.util.*;

class LinkedList {

	class Node {
		
		Node prev = null;
		int data;
		Node next = null;
		Node(int data){
			this.data = data;
		}
	}
	Node head = null;

	void addFirst(int data){
	
		Node newNode = new Node(data);
		if(head == null){
			head = newNode;
		}else{
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
	}

	void addLast(int data){
	
		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
		}else{
			Node temp = head;
			while(temp.next != null){
				temp = temp.next;
			}
			temp.next = newNode;
			newNode.prev = temp;
		}
	}

	void printSLL(){
	
		Node temp = head;

		while(temp != null){
			
			if(temp.next != null)
				System.out.print(temp.data +" => ");
			else
				System.out.print(temp.data);

			temp = temp.next;
		}
	}

	int countNode(){
		
		Node temp = head;
		int count = 0;
		while(temp != null){
			count++;
			temp = temp.next;
		}
		return count;
	}

	void addAtPos(int data, int pos){
	
		if(pos <= 0 || pos > countNode()+1){
			System.out.println("Invalid Position");
		}else if(pos == 1){
			addFirst(data);
		}else if(pos == countNode()+1){
			addLast(data);
		}else{
			
			Node newNode = new Node(data);
			Node temp = head;

			while(pos-2 != 0){
				temp = temp.next;
				pos--;
			}
			newNode.next = temp.next;
			newNode.prev = temp;
			temp.next.prev = newNode;
			temp.next = newNode;
		}
	}

	void delFirst(){
	
		if(head == null){
			System.out.println("Empty Linked List");
			return;
		}
		if(head.next == null){
			head = null;
			System.out.println("Node Deleted");
		}else{
			head = head.next;
			System.out.println("Node Deleted");
		}
	}

	void delLast(){
	
		if(head == null){
			System.out.println("Empty Linked List");
			return;
		}

		if(head.next == null){
			head = null;
			System.out.println("Node Deleted");
		}else{
			Node temp = head;
			while(temp.next.next != null){
				temp = temp.next;
			}
			temp.next = null;
			System.out.println("Node Deleted");
		}
	}

	void delAtPos(int pos){
	
		if(head == null){
			System.out.println("Empty Linked List");
			return;
		}

		if(pos <= 0 || pos > countNode()+1){
			System.out.println("Invalid Position");
		}else if(pos == 1){
			delFirst();
		}else if(pos == countNode()+1){
			delLast();
		}else{
			
			Node temp = head;

			while(pos-2 != 0){
				temp = temp.next;
				pos--;
			}
			temp.next = temp.next.next;
			temp.next.next.prev = temp.next;
			System.out.println("Node Deleted");
		}
	}
}

class Client{

	public static void main(String[] args) {
	
		LinkedList ll = new LinkedList();
		Scanner sc = new Scanner(System.in);
		char ch;

		do{
			System.out.println("\n**********Singly Linked List**********\n");
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtPos");
			System.out.println("4.delFirst");
			System.out.println("5.delLast");
			System.out.println("6.delAtPos");
			System.out.println("7.countNode");
			System.out.println("8.printSLL\n");

			System.out.println("Enter choice");
			int choice = sc.nextInt();

			switch(choice) {
			
				case 1:{
					System.out.println("Enter Data");
					int data = sc.nextInt();
					ll.addFirst(data);
					}
				       break;
				case 2:{
					System.out.println("Enter Data");
					int data = sc.nextInt();
					ll.addLast(data);
					}
				       break;
				case 3:{
					System.out.println("Enter Data");
					int data = sc.nextInt();
					System.out.println("Enter Position");
					int pos = sc.nextInt();
					ll.addAtPos(data,pos);
					}
				       break;
				case 4:{
					ll.delFirst();
					}
				       break;
				case 5:{
					ll.delLast();
					}
				       break;
				case 6:{
					System.out.println("ENter position");
					int pos = sc.nextInt();
					ll.delAtPos(pos);
					}
				       break;
				case 7:{
					System.out.println(ll.countNode());
					}
				       break;
				case 8:{
					ll.printSLL();
					}
				       break;
				default:
				       System.out.println("Wrong Input");
			}

			System.out.println("Continue ? : ");
			ch = sc.next().charAt(0);

		}while(ch == 'Y' || ch == 'y');
	}
}

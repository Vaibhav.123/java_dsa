

import java.util.*;

class Demo {
	
	static int findFactor(int num) {
	
		int count = 0;
		int itr = 0;
		for(int i=1; i*i<=num; i++){
			itr++;
			if(num%i == 0){
				if(i == num/i)
					count++;
				else
					count = count+2;
			}
		}
		System.out.println("itr = "+itr);
		return count;
	}
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter value");
		int num = sc.nextInt();

		int ret = findFactor(num);
		System.out.println("factor count = "+ret);
	}
}

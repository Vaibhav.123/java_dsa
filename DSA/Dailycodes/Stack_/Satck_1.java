
// Stack using predefined fnctions

import java.util.*;
class StackDemo {

	public static void main(String[] ags) {
	
		Stack s = new Stack();
		
		// push
		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);

		System.out.println(s);
		
		// size
		System.out.println(s.size());

		// pop
		System.out.println(s.pop());
		System.out.println(s.pop());

		System.out.println(s);

		// peek
		System.out.println(s.peek());

		// empty
		System.out.println(s.empty());
		s.pop();
		s.pop();
	
		System.out.println(s);
		System.out.println(s.empty());
	}
}

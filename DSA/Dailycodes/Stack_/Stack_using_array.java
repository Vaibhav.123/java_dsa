
import java.util.*;

class Stack {

	int maxSize;
	int stackArr[];
	int top = -1;

	Stack (int size){
	
		this.stackArr = new int[size];
		this.maxSize = size;
	}


	void push(int data){
		
		if(data == -1){
			System.out.println("Please can't enter -1");
			return;
		}

		if(top == maxSize-1){
		
			System.out.println("Stack is Full");
		}else{
		
			top++;
			stackArr[top] = data;
		}
	}

	boolean empty(){
	
		if(top == -1)
			return true;
		else 
			return false;
	}

	int pop(){
	
		if(empty()){
			return -1;
		}else {
		
			int val = stackArr[top];
			top--;

			return val;
		}
	}
	int peek(){
	
		if(empty()){
			return -1;
		}else {
			int val = stackArr[top];
			return val;
		}
	}

	int size(){
	
		return top;
	}

	void printStack(){
	
		System.out.print("[ ");
		for(int val = 0; val<=top; val++){
		
			System.out.print(stackArr[val] + " ");
		}
		System.out.println("]");
	}
}

class Client {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size for Stack");
		int size = sc.nextInt();

		Stack s = new Stack(size);

		char ch;

		do{
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.peek");
			System.out.println("4.size");
			System.out.println("5.empty");
			System.out.println("6.print Stack");

			System.out.println("Enter your choice: ");
			int choice = sc.nextInt();

			switch(choice) {
			
				case 1:{
				
						System.out.println("Enter element for push in stack [except -1]");
						int data = sc.nextInt();
						s.push(data);
					}
					break;
				
				case 2: {
					
						int flag = s.pop();	
						if(flag == -1)
							System.out.println("Stack is Empty");
						else 
							System.out.println(flag + " is popped");
					}
					break;

				case 3: {
						
						int flag = s.peek();	
						if(flag == -1)
							System.out.println("Stack is Empty");
						else 
							System.out.println(flag);
					}
					break;

				case 4: {
						int sz = 1 + s.size();
						System.out.println("size = "+ sz);
					}
					break;

				case 5: {
						boolean ret = s.empty();
						if(ret)
							System.out.println("Stack is Empty / true");
						else 
							System.out.println("Stack Is Not Empty / false");
					}
					break;

				case 6: {
					
						s.printStack();
					}
					break;
					
				default:
					System.out.println("Wrong choice");
					break;
			}

			System.out.println("Do you want to continue?");
			ch = sc.next().charAt(0);

		}while(ch == 'y' || ch == 'Y');
	}
}

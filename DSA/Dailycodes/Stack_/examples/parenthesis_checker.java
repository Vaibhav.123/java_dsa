// valid parenthesis

import java.util.*;

class Parenthesis {
	

	boolean check_parenthesis(String str) {
		
		Stack<Character> s = new Stack<Character> ();

		for(int i=0; i<str.length(); i++){
		
			char ch = str.charAt(i);

			if(ch == '{' || ch == '(' || ch == '[' ){
			
				s.push(ch);
			}else{
			
				if(!s.empty()){
				
					char x = s.peek();
			
					if((x == '(' && ch == ')') || (x == '{' && ch == '}') || (x == '[' && ch == ']')){
						
						s.pop();

					}else {
				
						return false;
					}
				}else{
					
					return false;
				}

			}
		}

		if(s.empty())
			return true;
		else
			return false;
	}

}

class Client {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Expression");
		String str = sc.next();

		Parenthesis obj = new Parenthesis();

		boolean ret = obj.check_parenthesis(str);

		if(ret)
			System.out.println("Balanced");
		else
			System.out.println("Not Balanced");
	}
}

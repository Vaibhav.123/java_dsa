// T.C => O(N^3)
// S.C => O(1)

class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{2,4,1,3};

		for(int i=0; i<arr.length; i++){
		
			for(int j=i; j<arr.length; j++){
			
				for(int k=i; k<=j; k++){
					System.out.print(arr[k]);
				}
			
			System.out.println();
			}
		}
	}
}
/* 
 * OP: 

	2
	24
	241
	2413
	4
	41
	413
	1
	13
	3
*/

// Total no of subarrays possible in array
// T.C => O(N^2)
// S.C => O(1)
//
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{1,2,3,4};

		int count = 0;
		
		for(int i=0; i<arr.length; i++){
		
			for(int j=i; j<arr.length; j++){
			
				count++;
			}
		}
		System.out.println(count);
	}
}

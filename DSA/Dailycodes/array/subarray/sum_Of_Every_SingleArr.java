// T.C => O(N^3)
// S.C => O(1)
// Using brute force approach
/*class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{2,4,1,3};

		for(int i=0; i<arr.length; i++){
		
			for(int j=i; j<arr.length; j++){
				int sum = 0;
				for(int k=i; k<=j; k++){
					sum = sum+arr[k];
				}
				System.out.println(sum);

			}
		}
	}
}
*/ 
 

// T.C => O(N^2)
// S.C => O(1)
// Using prefiz sum approach
/*
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{2,4,1,3};
		int ps[] = new int[arr.length];
		
		ps[0] = arr[0];
		for(int i=1; i<arr.length; i++){
		
			ps[i] = ps[i-1] + arr[i];
		}
		for(int i=0; i<arr.length; i++){
			for(int j=i; j<arr.length;j++){
				if(i != 0)
					System.out.println(ps[j] - ps[i-1]);
				else 
					System.out.println(ps[j]);
			}
		}
	}
}
*/
// T.C => O(N^2)
// S.C => O(1)
// using carry forward approach
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{2,4,1,3};

		for(int i=0; i<arr.length; i++){
			int sum = 0;
			for(int j=i; j<arr.length; j++){			
				sum = sum+arr[j];
				System.out.println(sum);
			}


		}
	}
}
/* 
	2
	6
	7
	10
	4
	5
	8
	1
	4
	3
*/

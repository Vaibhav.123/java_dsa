
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{6,1,7,5,3,8,4,9};

		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int minlength = Integer.MAX_VALUE;
		for(int i=0; i<arr.length; i++){
			if(arr[i] > max)
				max = arr[i];
			if(arr[i] < min)
				min = arr[i];
		}
		int len = 0;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i] == min){
			
				for(int j=i+1; j<arr.length; j++){
				
					if(arr[j] == max){
						len = j-i+1;
						if(len < minlength){
							minlength = len;
						}
					}
				}
			}else if(arr[i] == max){
				
					for(int j=i+1; j<arr.length; j++){
					
						if(arr[j] == min){
							len = j-i+1;
							if(minlength > len){
								minlength = len;
							}
						}
					}
				}
			}
		}
		System.out.println(minlength);
	}
}

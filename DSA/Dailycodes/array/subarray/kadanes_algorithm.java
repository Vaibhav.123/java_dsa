

// Kadane's Algorith :
// this is used for finding the maximum subarray sum
// this is best approach dor it
// its 
//
// T.C => O(N)
// S.C => O(1)


class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};

		int sum = 0;
		int max = Integer.MIN_VALUE;
		int start = -1;
		int end = -1;
		int x = -1;

		for(int i=0; i<arr.length; i++){
		
			sum = sum + arr[i];

			if(sum > max){
				max = sum;
				start = x;
				end = i;
			}
			if(sum < 0){
				x = i;
				sum = 0;
			}
		}
		if(start == -1)
			System.out.println("0");
		else{
			for(int i=start; i<=end; i++){
			
				System.out.print(arr[i]+ "  ");
			}
			System.out.println();
		}
	}
}

// T.C => O(N^2)
// S.C => O(1)
//

class Demo {

	public static void main(String[] ars) {
	
		int arr[] = new int[]{1,2};
		int totalsum = 0;
		int sum = 0;
		for(int i=0; i<arr.length; i++){
		
			for(int j=i; j<arr.length; j++){
			
				sum = sum + arr[j];
			}
			totalsum = totalsum+sum;
		}
		System.out.println(totalsum);
	}
}

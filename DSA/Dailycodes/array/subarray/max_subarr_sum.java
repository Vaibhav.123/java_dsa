
// Find contiguos subarray (containing at least one number) which 
// has largest sum and return its sum

// Using brute force approach
// T.C => O(N^3)
// S.C => O(1)

/*
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};
		
		int max = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
		
			for(int j=i; j<arr.length; j++){
			
				int sum = 0;
				for(int k=i; k<=j; k++){
				
					sum = sum+arr[k];
				}
				if(sum > max)
					max = sum;
			}
		}
		System.out.println("max = "+max);
	}
}
*/
//using carryforward approach
// T.C => O(N^2)
// S.C => O(1)

/*
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};
		
		int max = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
			
			int sum = 0;
			for(int j=i; j<arr.length; j++){
			
				sum = sum+arr[j];
				if(sum > max)
					max = sum;
			}
		}
		System.out.println("max = "+max);
	}
}
*/
// using prefix sum approach
// T.C => O(N^2)
// S.C => O(N)

class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};
		
		int max = Integer.MIN_VALUE;
		int ps[] = new int[arr.length];
		
		ps[0] = arr[0];
		for(int i=1; i<arr.length; i++){
		
			ps[i] = ps[i-1] + arr[i];
		}

		for(int i=0; i<arr.length; i++){
			int sum = 0;
			for(int j=i; j<arr.length; j++){
			
				if(i == 0)
					sum = ps[j];
				else 
					sum = ps[j] - ps[i-1];

				if(sum > max)
					max = sum;
						
			}
		}
		System.out.println("max = "+max);
	}
}


// T.C = O(N)
// S.C = O(N)
/*
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{1,2,3,4,5,6};
		int temp[] = new int[arr.length];
		int j=arr.length-1;
		for(int i=0; i<arr.length; i++){
			temp[i] = arr[j];
			j--;
		}
		for(int i=0; i<temp.length; i++){
			System.out.println(temp[i]);
		}
	}
}
*/
// optimize
// T.C = O(N)
// S.C = O(1)

class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{1,2,3,4,5,6};
		int j=arr.length-1;
		for(int i=0; i<arr.length/2; i++){
			int temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
			j--;

		}
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
	}
}


// sum from start to end index


import java.util.*;

class Demo{

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		int arr[] = new int[]{1,2,3,4,5,6,7,8};
		System.out.println("Enter Start and end index");
		int start = sc.nextInt();
		int end = sc.nextInt();
		int sum = 0;
		if(start < 0 || end > arr.length)
			System.out.println("Enter valid index");
		else{
			for(int i=start; i<=end; i++){
		
				sum = sum+arr[i];
			}
		}
		System.out.println("Sum = "+sum);
	}
}

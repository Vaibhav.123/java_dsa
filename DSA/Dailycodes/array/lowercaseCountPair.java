// return count of pair(i,j) such that
// i<j
// arr[i] = 'a' , arr[j] = 'g'
/*
class Demo {

	public static void main(String[] args) {
	
		char arr[] = new char[]{'a','b','e','g','a','g'};
		int count = 0;
		int itr = 0;
		for(int i=0; i<arr.length; i++){
			
			for(int j=i+1; j<arr.length; j++){
				itr++;
				if(arr[i] == 'a' && arr[j] == 'g')
					count++;
			}
		}
		System.out.println(count);
		System.out.println(itr);

	}	
}*/
/*
class Demo {

	public static void main(String[] args) {
	
		char arr[] = new char[]{'a','b','e','g','a','g'};
		int count = 0;
		int itr = 0;
		for(int i=0; i<arr.length; i++){
			
			if(arr[i] == 'a'){
				for(int j=i+1; j<arr.length; j++){
					itr++;
					if(arr[j] == 'g')
						count++;
				}
			}
		}
		System.out.println(count);
		System.out.println(itr);

	}	
}*/
// this code only for when the 'g' is not cpme first
class Demo {

	public static void main(String[] args) {
	
		char arr[] = new char[]{'a','b','e','g','a','g'};
		int count = 0;
		int pair = 0;

		for(int i=0; i<arr.length; i++){
			
			if(arr[i] == 'a'){
				count++;
			}else if(arr[i] == 'g')
				pair = pair + count;
		}
		System.out.println(count);

	}	
}

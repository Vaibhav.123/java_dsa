m

import java.util.*;

class BinarySearch {
	
	int flag = 0;
	int start;
	int end;

	int searchEle(int arr[],int search){
		
		if(flag == 0){
			start = 0;
			end = arr.length-1;
			flag=1;
		}

		if(start > end)
			return -1;
		else{
			int mid = (start+end)/2;
			if(arr[mid] == search){
				return mid;
			}
			if(arr[mid] > search){
				end = mid-1;
				return searchEle(arr,search);
			}else{
				start = mid+1;
				return searchEle(arr,search);
			}
		}
	}
	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size for array");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements in sorted order : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter Element for search : ");
		int search = sc.nextInt();

		BinarySearch obj = new BinarySearch();
		int index = obj.searchEle(arr,search);

		if(index == -1)
			System.out.println("Element Not Found");
		else
			System.out.println("Element Found at index : "+index);
	}
}



class Demo {
	
	static int flag = 0;
	static int fibo(int num){
			
		if(flag == 0){
			num = num-1;
			flag =1;
		}

		if(num <= 1)
			return num;

		return fibo(num-1) + fibo(num-2);
	}
	public static void main(String[]  args) {
	
		int num = 8;
		int ret = Demo.fibo(num);

		System.out.println(ret);
	}
}

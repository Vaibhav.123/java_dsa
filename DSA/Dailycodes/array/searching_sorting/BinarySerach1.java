

import java.util.*;

class BinarySearch {
	
	int searchEle(int arr[], int search){
	
		int start = 0 ;
		int end = arr.length-1;

		while(start <= end) {
		
			int mid = (start+end)/2;

			if(arr[mid] == search)
				return mid;

			if(arr[mid] > search)
				end = mid-1;

			if(arr[mid] < search)
				start = mid+1;
		}
		return -1;
	}
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size for array");
		int size = sc.nextInt();
		
		int arr[] = new int[size];
		System.out.println("Enter elements in sorted order");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		
		System.out.println("Enter element for search");
		int search = sc.nextInt();

		BinarySearch obj = new BinarySearch();
		int index = obj.searchEle(arr,search);
		
		if(index != -1)
			System.out.println("Element Found at index : "+index);
		else
			System.out.println("Element Not Found");

	}
}

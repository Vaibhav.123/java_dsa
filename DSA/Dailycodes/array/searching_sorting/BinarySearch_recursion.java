

import java.util.*;

class BinarySearch {
	/*
	int searchEle(int arr[], int start,int end,int search){
	
		if(start > end)
			return -1;
		int mid = (start+end)/2;
		if(arr[mid] == search){
			return mid;
		}
		
		if(arr[mid] > search){
			return searchEle(arr,start,mid-1,search);
		}
		if(arr[mid] < search){
			return searchEle(arr,mid+1,end,search);
		}

		return -1;
	}*/
	int searchEle(int arr[], int start,int end,int search){
	
		if(start > end)
			return -1;
		else{
			int mid = (start+end)/2;
			if(arr[mid] == search){
				return mid;
			}
		
			if(arr[mid] > search){
				return searchEle(arr,start,mid-1,search);
			}else{
				return searchEle(arr,mid+1,end,search);
			}
		}
	}
	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size for array");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements in sorted order : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter Element for search : ");
		int search = sc.nextInt();

		BinarySearch obj = new BinarySearch();
		int start = 0;
		int end = arr.length-1;
		int index = obj.searchEle(arr,start,end,search);

		if(index == -1)
			System.out.println("Element Not Found");
		else
			System.out.println("Element Found at index : "+index);
	}
}

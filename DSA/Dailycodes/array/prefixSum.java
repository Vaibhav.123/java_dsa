

import java.util.*;

class PrefixSum {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size for Array");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		int ps[] = new int[arr.length];
		ps[0] = arr[0];
		for(int i=1; i<arr.length; i++){
		
			ps[i] = ps[i-1] + arr[i];
		}

		System.out.println("Enter no og queries");
		int Q = sc.nextInt();
		int sum = 0;
		for(int i=1; i<=Q; i++){
		
			System.out.println("Enter start and end index");
			int start = sc.nextInt();
			int end = sc.nextInt();

			if(start < 0 || end > arr.length){
				System.out.println("Enter valid Index");
				break;
			}
			if(start == 0)
				sum = ps[end];
			else
				sum = ps[end] - ps[start-1];
			System.out.println("Sum = "+sum);
		}
	}
}

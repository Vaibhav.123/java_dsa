// T.C => O(N)
// S.C => O(1)
// but in the below code previous array is not remains same
/*
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int max = Integer.MIN_VALUE;

		for(int i=0; i<arr.length; i++){
		
			if(arr[i] > max){
			
				max = arr[i];
			}
			arr[i] = max;
		}
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] + "  ");
		}
		System.out.println();

	}
}*/

// now in this code we have need space complexity or need one more array
// T.C => O(N)
// S.C => O(N)
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int left[] = new int[arr.length];
		int max = Integer.MIN_VALUE;

		left[0] = arr[0];
		for(int i=1; i<arr.length; i++){
		
			if(left[i-1] < arr[i]){
			
				left[i] = arr[i];
			}else
				left[i] = left[i-1];
		}
		for(int i=0; i<left.length; i++){
		
			System.out.print(left[i] + "  ");
		}
		System.out.println();

	}
}

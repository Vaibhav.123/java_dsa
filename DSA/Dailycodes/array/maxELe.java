
// find max element from the array
// T.C => O(N)
// S.C => O(1)
//
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{3,4,5,1,2,7,9,8};

		int max = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i] > max)
				max = arr[i];
		}
		System.out.println("Max = "+max);
	}
}


// given an array of size N.
// Count the number elements having at least 1 element greater than itself.

// Basic approach
// T.C = O(N^2);
// S.C = O(N)
/*class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{2,5,1,4,8,0,8,1,3,8};

		int count = 0;

		for(int i=0; i<arr.length; i++){
		
			for(int j=0; j<arr.length; j++){
			
				if(arr[i] < arr[j]){
					count++;
					break;
				}
			}
		}
		System.out.println(count);
	}
}*/

// optimize code
// T.C = O(N)
// S.C = O(1)
class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{2,5,1,4,8,0,8,1,3,8};
		int count=0;
		// max
		int max = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
			if(arr[i] > max)
				max = arr[i];
		}
		for(int i=0; i<arr.length; i++){
			if(arr[i] == max)
				count++;
		}
		System.out.println(arr.length-count);

	}
}

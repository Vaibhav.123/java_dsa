
import java.util.*;

class PrefixSum {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size for Array");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		int ps[] = new int[arr.length];
		ps[0] = arr[0];
		for(int i=1; i<arr.length; i++){
		
			ps[i] = ps[i-1] + arr[i];
		}
			
		int x = 16;
		int flag = 0;
		for(int i=0; i<ps.length; i++){
			if(ps[i] == x)	{
				flag = 1;
				break;
			}
		}
		if(flag == 1)
			System.out.println("Yes");
		else 
			System.out.println("No");
	}
}

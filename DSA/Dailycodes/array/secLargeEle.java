
// T.C = O(N^2)
// S.C = O(N)
//
/*class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{1,6,3,5,7,4,8};
		int secL = Integer.MIN_VALUE-1;
		int max = Integer.MIN_VALUE;

		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr.length; j++){
				if(arr[i] > max){
					secL = max;
					max = arr[i];
				}
			}
		}
		System.out.println(secL);
	}
}
*/
// optimize code 
// T.C = O(N)
// S.C = O(1)

class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{1,6,3,5,7,4,8};
		int secL = Integer.MIN_VALUE-1;
		int max = Integer.MIN_VALUE;

		for(int i=0; i<arr.length; i++){
			if(arr[i] > max){
				secL = max;
				max = arr[i];
			}
		}
		System.out.println(secL);
	}
}

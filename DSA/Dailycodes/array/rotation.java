

import java.util.*;

class PrefixSum {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size for Array");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter no of times to rotate(B)");
		int B = sc.nextInt();

		for(int i=0; i<B; i++){
		
			int ele = arr[0];
			for(int j=0; j<arr.length-1; j++){
				
				arr[j] = arr[j+1];
			}
			arr[arr.length-1] = ele;
		}
		for(int i=0; i<arr.length; i++) {
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}

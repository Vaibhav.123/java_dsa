
// Given an array of size N and Q are no. of Queries.
// Query contains two parameters (s,e).
// for all queries print sum of all elements from index s to index e.
//

// T.C = O(Q*N)
// S.C = O(1)

import java.util.*;

class Demo{

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		int arr[] = new int[]{1,2,3,4,5,6,7,8};

		System.out.println("Enter no of queries");
		int Q = sc.nextInt();
		int flag = 0;
		for(int i=1; i<=Q; i++){
			System.out.println("Enter Start and end index");
			int start = sc.nextInt();
			int end = sc.nextInt();
			int sum = 0;
			if(start < 0 || end > arr.length){
				System.out.println("Enter valid Index");
				break;
			}
			for(int j=start; j<=end; j++){
		
				sum = sum+arr[j];
			}
			System.out.println("Sum = "+sum);
		}
	}
}

// T.C => O(N)
// S.C => O(1)
// but in the below code previous array is not remains same

class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int max = Integer.MIN_VALUE;

		for(int i=arr.length-1; i>=0; i--){
		
			if(arr[i] > max){
			
				max = arr[i];
			}
			arr[i] = max;
		}
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] + "  ");
		}
		System.out.println();

	}
}



class Demo {

	public static void main(String[] args) {

		StringBuffer sb = new StringBuffer();

		for(int i=0; i<18; i++){
		
			sb.append(i);
		}

		System.out.println(sb.length());	// 26	here some numbers are 2 digit so it consider it ass 2 characters
		System.out.println(sb.capacity());	// 34 [(initial capacity + 1)*2 = 34]
	}
}

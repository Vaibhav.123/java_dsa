

class Demo {

	public static void main(String[] args) {
	
		StringBuffer sb = new StringBuffer();

		System.out.println(sb.capacity());	// 16 initial capacity of stringBuffer
		System.out.println(sb.length());	// 0
	}
}



class Demo {

	public static void main(String[] ar) {
	
		StringBuffer str1 = new StringBuffer("Shashi");
		StringBuffer str2 = new StringBuffer("Shashi");

		System.out.println(str1.equals(str2)); 	// it compares actual objects

		str1.setLength(3);	// it sets the length for string
		System.out.println(str1);

	}
}
// output :  flase
// 	     Sha

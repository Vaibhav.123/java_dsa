
class Demo {

	public static void main(String[] args) {
	
		StringBuffer str1 = new StringBuffer("Shashi");
		String str2 = new String("Shashi");
		
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		if(str2.equals(str1))
			System.out.println("Both are equal");
		else

			System.out.println("Both are not equal");
	}
}
// output : Both are not equal  [ equals method compares the hashcodes of strings]
//    it is method of stringBuffer class

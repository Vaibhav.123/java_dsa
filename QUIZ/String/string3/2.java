

class Demo {

	public static void main(String[] as) {
	
		StringBuffer str1 = new StringBuffer("Demo");	// capacity = 16+4 = 20
		StringBuffer str2 = new StringBuffer();		// capacity = 16
		
		str2 = str2.append("Demo");			// capacity = 16 after append aslo

		System.out.println(str1.capacity() == str2.capacity());  // 20 == 16

	}
}

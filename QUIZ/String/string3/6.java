

class Demo {

	public static void main(String[] ars) {
	
		StringBuffer str = new StringBuffer();

		str.ensureCapacity(10);		// ensureCapacity method checks the capacity is less than given integer parameter.
						// if yes then it will change capacity otherwise not

		System.out.println(str.capacity());
	}
}

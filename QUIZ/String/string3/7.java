

class Demo {

	public static void main(String[] as) {
	
		StringBuffer str = new StringBuffer(400);

		str.append("Java_Development_Kit");

		str.trimToSize();  // it trims size upto length of string
		str.setLength(10);

		System.out.println(str.capacity());	// 20
		System.out.println(str.length());	// 10
	}
}

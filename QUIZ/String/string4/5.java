

class Demo {

	public static void main(String[] args) {
		
		StringBuilder str = new StringBuilder();

		str.append("Java_Virtual_Machine");

		str.setCharAt(str.charAt(14),'z');

		System.out.println(str);
	}
}
// OUTPUT : 
// runtime exception because 
// charAt method replaces its place with returned character whic is "a". So the method call changes to str.setCharAt('a','z').
// But paramter required to method setCharAt is int,char. So 'a' is replaced with its ASCII value i.e. 97. but 97 is out of bound



class Demo  {

	public static void main(String[] args)  {
	
		String s1 = "This is a String";
		
		String s2 = "a String";

		String s3 = "This is "+s2;

		if(System.identityHashCode(s1) == System.identityHashCode(s3))
			System.out.println("Equal");
		else
			System.out.println("Not equal");
	}
}

//   Not equal


class Demo  {

	public static void main(String[] args)  {
	
		String str1 = new String("core2web");
		String str2 = new String("core2web");

		if(str1 == str2)
			System.out.print("True1  ");    // here condition false
		if(str1.equals(str2))
			System.out.print("True2  ");	// true2,  direct data of string will be compared
		if(str1.hashCode() == str2.hashCode())
			System.out.println("True3  ");	// true3,  hash value compare
	}
}

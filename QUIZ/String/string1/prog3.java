

class Demo  {

	public static void main(String[] args)  {
	
		String s1 = "This is a string";
		String s2 = "This is a string";

		if(System.identityHashCode(s1) == System.identityHashCode(s2))
			System.out.println("Equal");

		else
			System.out.println("Not Equal");
	}
}

//  O/P :  Equal

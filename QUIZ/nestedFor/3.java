/*   4  3  2  1
/*   4  3  2  1
/*   4  3  2  1
/*   4  3  2  1
 */

class Demo  {
	
	public static void main(String[] args){
		
		for(int i=1; i<=4; i++){
			
			int x=4;

			for(int j=1; j<=4; j++){
				
				System.out.print(x-- +"  ");
			}
			System.out.println();
		}
	}
}



class Core2web {
	
	public static void main(String[] args){
		
		int var=5;
		System.out.println("Inside main");	// Inside main

		while(var-- > 3);{	// here while is an empty statement, but the condition given with it is true which eventually becomes false.
					//  So it does not go into an infinite loop
			
			System.out.println("Inside while");	// Inside while
		}
	}
}

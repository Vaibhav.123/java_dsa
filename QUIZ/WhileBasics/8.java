

class Core2web {
	
	public static void main(String[] args){
		
		int var=5;
		System.out.println("Inside Main");

		while(var>3);{
		
			System.out.println("HEllo,Core2web!");
	
		}

	}
}

// 	here while is an empty statement, but the condition given with it is true
// 	so this unexpected behaviour happens
//
//O/P: 
//	Inside Main
//	(Code goes into infinite loop with no output)
//


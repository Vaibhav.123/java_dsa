  

class Demo {
	
	public static void main(String[] as){
		
		System.out.println("Before for loop");

		for(int i,j; i<3; i++,j++){		// variable i and j are not have been initialized
			
			System.out.println("Insiden for loop");
		}

		System.out.println("After for loop");
	}
}

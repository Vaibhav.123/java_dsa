  

class Demo {
	
	public static void main(String[] as){
		
		System.out.println("Before for loop");

		for(int i,j; i<3; i++{		// variable i is not have been initialized
						// here error only for i because only i gave been used in the condition and incremented
			
			System.out.println("Insiden for loop");
		}

		System.out.println("After for loop");
	}
}

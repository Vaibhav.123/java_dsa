
class Core2web{
	
	public static void main(String[] args){
		
		System.out.println("Before for loop");

		for(int i=0,j=0; i<1; j++){		// here we cannot change the value of i so condition never false for that output is infinte loop
			
			System.out.println("Inside for");
		}

		System.out.println("Outside for");
	}
}

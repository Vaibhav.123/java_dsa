  
class Demo{
	
	public static void main(String[] aargs){
		
		for(int var1=10, float var2=40.25f; var1<15; var1++,var2++){	// in loop we cannot initialize variables ofdifferent data types 
										// it gives multiple errors for syntax
			
			if(var2%2==0){
				
				System.out.println(var1);
			}
		}
	}
}

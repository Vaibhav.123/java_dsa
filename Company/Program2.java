

class Demo {

	Demo(){
	
		System.out.println("Demo Constructor");
	}
}
class DemoChild extends Demo {

	DemoChild(){
	
		System.out.println("DemoChild Constructor");
	}
}

class Parent {

	Parent (){
	
		System.out.println("Parent Constrcutor");
	}

	Demo m1(){
	
		System.out.println("In Parent-m1");
		return new Demo();
	}
}
class Child extends Parent {

	Child (){
	
		System.out.println("Child Constrcutor");
	}
	DemoChild m1(){
	
		System.out.println("In Child-m1");
		return new DemoChild();
	}
}

class Client {

	public static void main(String[] args) {
	
		Parent obj1 = new Child();
		obj1.m1();
		System.out.println("*******************************************");
		Parent obj2 = new Parent();
		obj2.m1();
	}
}

//  OP:
//  		Parent Constructor
//		Child Constructor
//		In Child-m1
//		Demo Constructor
//		DemoChild Constructor
//		***************************************************
//		Parent Constructor
//		In Parent-m1
//		Demo Constructor

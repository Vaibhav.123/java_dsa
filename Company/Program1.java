

class Parent {

	int x = 10;

	void m1(){
	
		System.out.println("In Parent-m1");
	}

	Parent (){
	
		System.out.println("1");
	}
}

class Child extends Parent {

	int a = 20;
	 void m1(){
	 
		 System.out.println("In Child-m1");
	 }
}

class Demo {

	Demo(Parent P) {
	
		System.out.println("Parent Constructor");
		P.m1();
	}
	Demo(Child C) {
	
		System.out.println("Child Constructor");
		C.m1();
	}

	public static void main(String[] args) {
	
		Demo obj1 = new Demo(new Parent());
		Demo obj2 = new Demo(new Child());
	}

}

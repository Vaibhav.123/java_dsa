//  WAP take an input from user for array size and also integer elements from user and
//  print only the number which is divisible by 5 


import java.io.*;

class Demo  {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size for array");
		int size=Integer.parseInt(br.readLine());
		
		int arr[]=new int [size];

		System.out.println("Enter elements");
		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Number divisible by 5");
		
		for(int i=0; i<arr.length; i++){
			if(arr[i]%5==0){
				System.out.println(arr[i]);
			}
		}
	}
}

//  WAP take an input from user for array size and also elements from user and
//  print the product of even number from the array


import java.io.*;

class Demo  {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size for array");
		int size=Integer.parseInt(br.readLine());
		
		int arr[]=new int [size];
		int pro=1;

		System.out.println("Enter elements");
		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]%2==0){
				pro=pro*arr[i];
			}
		}
		System.out.println("Product of even number = "+pro);
	}
}

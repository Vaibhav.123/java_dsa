//  WAP take an input from user for array size and also character elements from user and
//  print the only vowels from the array


import java.io.*;

class Demo  {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size for array");
		int size=Integer.parseInt(br.readLine());
		
		char arr[]=new char[size];

		System.out.println("Enter characters");
		for(int i=0; i<arr.length; i++){
			arr[i]=(char)(br.read());
			br.skip(1);
		}

		System.out.println("Vowels are");

		for(int i=0; i<arr.length; i++){
		
			
			if(arr[i]=='a')
				System.out.print(arr[i]+"  ");
			else if(arr[i]=='e')
				System.out.print(arr[i]+"  ");
			else if(arr[i]=='i')
				System.out.print(arr[i]+"  ");
			else if(arr[i]=='o')
				System.out.print(arr[i]+"  ");
			else if(arr[i]=='u')
				System.out.print(arr[i]+"  ");
				

		}
		System.out.println();
	}
}

/*  WAP program to create an array of 'n' integer elements.
 *  take values from user and Find the common element from two arrays
 */

import java.io.*;
class Demo  {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size for array1 and array2");
		int size1=Integer.parseInt(br.readLine());
		int size2=Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];
		int arr2[] = new int[size2];

		System.out.println("Enter integer elements in array1");
		
		for(int i=0; i<size1; i++){
			
			arr1[i]=Integer.parseInt(br.readLine());
				
		}

		System.out.println("Enter integer elements in array2");

		for(int i=0; i<size2; i++){

			arr2[i]=Integer.parseInt(br.readLine());

		}

		int flag=0;

		for(int j=0; j<size1; j++){
			for(int i=0; i<size2; i++){
			
				if(arr1[j]==arr2[i]){
					System.out.println("common element = "+arr1[j]);
					flag=1;
				}

			}
		}
		if(flag==0)
			System.out.println("No Common Elements present in arrays");



	}
}

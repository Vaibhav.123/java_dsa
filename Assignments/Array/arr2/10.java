/*  WAP program to create an array of 'n' integer elements.
 *  take values from user and print the elements whose addition of digits is even 
 *  wx. 26=2+6=8  (8 is even ) print 26
 */

import java.io.*;
class Demo  {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size for array1 and array2");
		int size1=Integer.parseInt(br.readLine());

		int arr[] = new int[size1];
		
		System.out.println("Enter integer elements in array1");
		
		for(int i=0; i<size1; i++){
			
			arr[i]=Integer.parseInt(br.readLine());
				
		}
	
		

		System.out.println("OUPUT ");
		int x;

		for(int i=0; i<arr.length; i++){
			int sum=0;
			
			x=arr[i];

			while(arr[i] != 0){
				
				int rem=arr[i]%10;
				sum=sum+rem;
				arr[i]=arr[i]/10;
			}

			if(sum%2==0){
				
				System.out.println(x+"   ");
			}

		}

	}
}

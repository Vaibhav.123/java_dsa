/*  WAP program to create an array of 'n' integer elements.
 *  take values from user and merge two arrays
 */

import java.io.*;
class Demo  {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size for array1 and array2");
		int size1=Integer.parseInt(br.readLine());
		int size2=Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];
		int arr2[] = new int[size2];
		int arr3[] = new int[size1+size2];


		System.out.println("Enter integer elements in array1");
		
		for(int i=0; i<size1; i++){
			
			arr1[i]=Integer.parseInt(br.readLine());
			arr3[i]=arr1[i];
		
				
		}

		System.out.println("Enter integer elements in array2");
	

		for(int i=0; i<size2; i++){

			arr2[i]=Integer.parseInt(br.readLine());
			arr3[size1+i]=arr2[i];


		}
		System.out.println("Merged arrays: ");
		System.out.print("  [ ");

		for(int i=0; i<arr3.length; i++){
			
			System.out.print(arr3[i]+"  ");
			
		}
		System.out.print("]");
		System.out.println();



	}
}

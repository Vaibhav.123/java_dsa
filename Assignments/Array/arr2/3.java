/*  WAP program to create an array of 'n' integer elements.
 *  take values from user and print the sum of even and odd numbers from the array
 */

import java.io.*;
class Demo  {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size for array");
		int size=Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int sum1=0;
		int sum2=0;

		System.out.println("Enter integer elements");

		for(int i=0; i<size; i++){
			
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0)
				sum1=sum1+arr[i];
			else
				sum2=sum2+arr[i];
				
		}

		System.out.println("Sum of Even Elements array = "+sum1);
		System.out.println("Sum of Odd Elements array = "+sum2);
	}
}

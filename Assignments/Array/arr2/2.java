/*  WAP program to create an array of 'n' integer elements.
 *  take values from user and print the number of even and odd numbers from the array
 */

import java.io.*;
class Demo  {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size for array");
		int size=Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int count1=0;
		int count2=0;

		System.out.println("Enter integer elements");

		for(int i=0; i<size; i++){
			
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0)
				count1++;
			else
				count2++;
		}

		System.out.println("Number of Even Elements array = "+count1);
		System.out.println("Number of Odd Elements array = "+count2);
	}
}

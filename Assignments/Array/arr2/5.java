/*  WAP program to create an array of 'n' integer elements.
 *  take values from user and Find the minimum element from array
 */

import java.io.*;
class Demo  {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size for array");
		int size=Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int min=0;

		System.out.println("Enter integer elements");
		
		for(int i=0; i<size; i++){
			
			arr[i]=Integer.parseInt(br.readLine());
				
		}
		min=arr[0];
		for(int i=0; i<size; i++){
			if(arr[i]<min)
				min=arr[i];
			
				
		}

		System.out.println("Minimum element from array "+min);
	}
}

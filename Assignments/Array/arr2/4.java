/*  WAP program to create an array of 'n' integer elements.
 *  take values from user and search specific element from the array
 */

import java.io.*;
class Demo  {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size for array");
		int size=Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		

		System.out.println("Enter integer elements");
		
		for(int i=0; i<size; i++){
			
			arr[i]=Integer.parseInt(br.readLine());
				
		}

		System.out.println("Enter element to search");
		int key=Integer.parseInt(br.readLine());

		for(int i=0; i<size; i++){
			
			if(arr[i]==key)
				System.out.println("Key found at = "+i);
		}
	}
}

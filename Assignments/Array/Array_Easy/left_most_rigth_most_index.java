
import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size");
		int size = sc.nextInt();

		System.out.println("Enter Values");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter element");
		int x = sc.nextInt();
		int first = -1;
		int last = -1;
		int flag = 0;
		for(int i=0; i<arr.length; i++){
			if(flag == 0){
				if(arr[i] == x){
					first = i;
					flag = 1;
				}
			}
			if(flag == 1){
				if(arr[i] == x){
					last = i;
				}
			}
		}
		System.out.println("First = "+first + " Last = "+last);
	}
}

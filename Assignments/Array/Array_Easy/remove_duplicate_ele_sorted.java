
import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size");
		int size = sc.nextInt();

		System.out.println("Enter Values");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		int temp[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			for(int j=i; j<arr.length; j++){

				if( i!=j && arr[i] == arr[j]){
					arr[i]=arr[i+1];
					arr[i+1]=-1;
				}
			}
		}
		for(int i=0; i<arr.length; i++){	
			if(arr[i] != -1){
				System.out.print(arr[i]+"  ");
			}
		}
		System.out.println();

	}
}


import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size");
		int size = sc.nextInt();

		System.out.println("Enter Values");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		int first_max = Integer.MIN_VALUE;
		int second_max = Integer.MIN_VALUE;
		int third_max = Integer.MIN_VALUE;

		for(int i=0; i<arr.length; i++){
			int temp = arr[i];
			if(first_max < temp){
				third_max = second_max;
				second_max = first_max;
				first_max = temp;
			}else if(second_max < temp){
				third_max = second_max;
				second_max = temp;
			}else if(third_max < temp){
			
				third_max = temp;
			}
		}
		System.out.println("Pro = "+first_max*second_max*third_max);
	}
}

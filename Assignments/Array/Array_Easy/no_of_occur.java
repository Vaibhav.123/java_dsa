

import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter values");
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter number for calculating");
		int x = sc.nextInt();
		int count=0;
		for(int i=0; i<arr.length; i++){
		
			if(x == arr[i])
				count++;
		}
		System.out.println("Count = "+count);
	}
}

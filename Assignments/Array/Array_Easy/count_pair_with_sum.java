

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter elements");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter sum");
		int sum = Integer.parseInt(br.readLine());

		int count = 0;
		for(int i=0; i<arr.length; i++){
			//int count = 0;
			for(int j=i+1; j<arr.length; j++){
				if(arr[i]+arr[j] == sum){
					count++;
				}
			}
			
		}
		System.out.println("Count = "+count);
	}
}

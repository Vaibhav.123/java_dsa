

import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter values");
		for(int i=0; i<arr.length; i++){
		
			arr[i]=sc.nextInt();
		}
		int flag=0;
		int j = 1;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i] != arr[j]){
				flag=1;
				System.out.println(i);
				break;
			}
		}
		if(flag == 0)
			System.out.println("-1");
	}
}

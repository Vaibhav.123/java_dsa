

import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter element upto this");
		int p = sc.nextInt();
		System.out.println("Enter values");
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
		int temp[] = new int[size];
		int k=0;
		for(int i=1; i<=p; i++){
			int count = 0;
			for(int j=0; j<arr.length; j++){
				if(i == arr[j]){
					count++;
				}
			}
			if(count == 1)
				temp[k] = 1;
			else if(count == 0)
				temp[k] = 0;
			else
				temp[k] = count;
			k++;
		}
		for(int i=0; i<temp.length; i++){
			System.out.print(temp[i]+"  ");
		}
		System.out.println();
	}
}



import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter elements only 0,1 and 2");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++) {
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int flag =0;
		for(int i=0; i<arr.length; i++){
			int count = 0;
			for(int j=0; j<arr.length; j++){
			
				if(arr[i] == arr[j]){
					count++;
				}
			}
			if(count >= 2){
				System.out.print(arr[i]+"  ");
				flag = 1;
			}
		}
		if(flag == 0)
			System.out.print("-1");
		System.out.println();
	}
}



import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter elements");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int flag = 0;
		int larg = Integer.MIN_VALUE;
		int sec_larg = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
			
			if(arr[i] > larg){
				sec_larg = larg;
				larg = arr[i];
			}else if(arr[i] != larg && arr[i] > sec_larg)
				sec_larg = arr[i];
		}
		System.out.println(sec_larg);
	}
}



import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter values");
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter first ele");
		int x = sc.nextInt();
		System.out.println("Enter second ele");
		int y = sc.nextInt();

		for(int i=0; i<arr.length; i++){
		
			if(arr[i] > x && arr[i] < y){
				System.out.println(i);
				break;
			}
		}

	}
}



import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter elements");
		int arr[] = new int[size-1];
		for(int i=0; i<arr.length; i++) {
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=1; i<=size; i++){
			int flag = 0;
			for(int j=0; j<arr.length; j++){
			
				if(arr[j] == i){
					flag = 1;
					break;
				}else
					continue;
			}
			if(flag == 0){
				System.out.println("Missing ele = "+i);
				break;
			}
		}	
	}
}


import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size");
		int size = sc.nextInt();

		System.out.println("Enter Values");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		int count = 0;
		for(int i=0;i<arr.length; i++){
		
			for(int j=i+1;j<arr.length; j++){
				if(arr[i] < arr[j]){
					count = i;
					break;
				}
			}
		}
		System.out.println(count);
	}
}

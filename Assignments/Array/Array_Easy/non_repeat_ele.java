
import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size");
		int size = sc.nextInt();

		System.out.println("Enter Values");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		int max = Integer.MIN_VALUE;
		int flag = 0;
		for(int i=0; i<arr.length; i++){
			int count=0;
			for(int j=0; j<arr.length; j++){
			
				if(arr[i] == arr[j]){
					count++;
				}
			}
			if(count <= 1){
				flag = 1;
				System.out.println("ele = "+arr[i]);
			}
		}
		if(flag == 0)
			System.out.println("ele = 0");
	}
}

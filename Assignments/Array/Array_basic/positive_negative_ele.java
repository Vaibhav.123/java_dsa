
import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter size");
		int size = sc.nextInt();

		System.out.println("Enter positive and negative array elements");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		
		for(int i=0; i<arr.length; i++){
		
			for(int j=i+1; j<arr.length; j++){
				
				if(arr[i] < arr[j]){
					int temp = arr[i];
					arr[i]= arr[j];
					arr[j]= temp;
					break;
				}else	
					break;
			}
		}
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] +", ");
		}
		System.out.println();

	}
}

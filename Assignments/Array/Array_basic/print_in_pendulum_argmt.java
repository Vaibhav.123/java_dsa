
import java.util.*;

class Demo {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		for(int i=0; i<arr.length;i++){
		
			for(int j=i+1; j<arr.length; j++){
			
				if(arr[i] > arr[j]){
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
				
			}
		}
		int mid = size/2;
		int temp[] = new int[size];
		temp[mid] = arr[0];
		int j=1;
		int k=1;
		for(int i=1; i<arr.length; i++){
		
			if(i%2 != 0){
				temp[mid+j] = arr[i];
				j++;
			}else{
			
				temp[mid-k] = arr[i];
				k++;
			}
		}
		for(int i=0; i<temp.length; i++){
		
			System.out.print(temp[i]+"  ");
		}
		System.out.println();
	}
}



import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENter Size for Array");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter Array Ele");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=0; i<arr.length; i++){
			int temp = arr[i];
			int count=0;
			for(int j=0; j<arr.length; j++){
			
				if(temp == arr[j])
					count++;
			}
			if(count%2 == 0){
				System.out.print(temp+ "  ");
			
			}else{
				System.out.print("-1");
			
			}
		}
		System.out.println();
	}
}

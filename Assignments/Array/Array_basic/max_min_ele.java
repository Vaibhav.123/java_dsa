
import java.io.*;
class Demo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter size for array");
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];
		
		System.out.println("Enter elements in array");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int max = arr[0];
		int min = arr[0];
		for(int i=0; i<arr.length; i++){

			if(max <= arr[i])
				max = arr[i];
		}
		for(int i=0; i<arr.length; i++){

			if(min >= arr[i])
				min = arr[i];
		}
		System.out.println("Maximum element : "+ max);
		System.out.println("Minimum element : "+ min);
	}
}


import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter size");
		int size = sc.nextInt();

		System.out.println("Enter array elements");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		int pro = 1;
		int one = Integer.MIN_VALUE;
		int two = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
			
			if(arr[i] > two){
				two = one;
				one = arr[i];
			}else if(arr[i] != two && arr[i]>one){
			
				one = arr[i];
			}
		}
		System.out.println("Max pro of two num "+one+" & "+two+" = "+ one*two);
	}
}

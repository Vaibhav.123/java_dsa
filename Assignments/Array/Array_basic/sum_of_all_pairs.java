
import java.io.*;
class Demo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter size for array");
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];
		
		System.out.println("Enter elements in array");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		int sum =0;
		for(int i=0; i<arr.length; i++){
			
			for(int j=i+1; j<arr.length; j++){
			
				sum = arr[i] - arr[j];
			}
		
		}
		System.out.println("Sum = "+ sum);
	}
}

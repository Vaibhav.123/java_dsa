
import java.util.*;

class Demo {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		int sum1 = 0;
		int sum2 = 0;

		for(int i=0; i<(arr.length) / 2; i++){
			
			sum1 = sum1 + arr[i];
		}
		for(int i=(arr.length/2); i<arr.length; i++){
			
			sum2 = sum2 + arr[i];
		}
		System.out.println("Pro = "+sum1*sum2);

	}
}

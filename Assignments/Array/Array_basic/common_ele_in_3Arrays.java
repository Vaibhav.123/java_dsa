
import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size for 1st array");
		int size1 = sc.nextInt();
		System.out.println("Enter first array Elements");
		int arr1[] = new int[size1];
		for(int i=0; i<arr1.length; i++){
			arr1[i] = sc.nextInt();
		}
		System.out.println("Enter Size for 2nd array");
		int size2 = sc.nextInt();
		System.out.println("Enter 2nd array Elements");
		int arr2[] = new int[size2];
		for(int i=0; i<arr2.length; i++){
			arr2[i] = sc.nextInt();
		}
		System.out.println("Enter Size for 3rd array");
		int size3 = sc.nextInt();
		System.out.println("Enter 3rd array Elements");
		int arr3[] = new int[size3];
		for(int i=0; i<arr3.length; i++){
			arr3[i] = sc.nextInt();
		}
		int flag = 0;
		for(int i=0; i<arr1.length; i++){
			flag = 0;
			for(int j=0; j<arr2.length; j++){
				if(arr1[i] == arr2[j]){
					int temp = arr1[i];
					for(int k=0; k<arr3.length; k++){
				
						if(temp == arr3[k]){
							flag = 1;
							break;
						}
					}
				}
			}if(flag == 1)
				System.out.print(arr1[i]+", ");
		}
		System.out.println();
	}
}

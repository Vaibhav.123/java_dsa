
import java.io.*;
class Demo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter size for array");
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];
		
		System.out.println("Enter elements in array");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter element for search");
		int ele = Integer.parseInt(br.readLine());
		int flag = 0;
		int index = -1;
		for(int i=0; i<arr.length; i++){
		
			if(ele == arr[i]){
				flag = 1;
				System.out.println("Found at index : "+i);
				break;
			}
		}
		if(flag == 0)
			System.out.println("Not found");
	}
}

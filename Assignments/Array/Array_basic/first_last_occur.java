
import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENter Size for Array");
		int size1 = Integer.parseInt(br.readLine());
		System.out.println("Enter Array Ele");
		int arr[] = new int[size1];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}	

		int first = -1;
		int last = -1;
		System.out.println("Enter element for check");
		int ele = Integer.parseInt(br.readLine());
		for(int i=0; i<arr.length; i++){
			if(first == -1){
				if(arr[i] == ele)
					first = i;
			}
			if(arr[i] == ele)
				last = i;
		}
		if(first == -1 && last == -1){
			System.out.println("-1");
		}else{
			System.out.println("First = "+ first);
			System.out.println("Last = "+ last);
		}

	}
}

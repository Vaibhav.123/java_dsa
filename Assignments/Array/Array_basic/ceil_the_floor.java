
import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter size");
		int size = sc.nextInt();

		System.out.println("Enter array elements");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter element for find");
		int x = sc.nextInt();
		int ceil = -1;
		int floor = -1;
		for(int i=0; i<arr.length; i++) {
		
			if(arr[i] == x+1){
			
				ceil = arr[i];
				break;
			}
		}
		for(int i=0; i<arr.length; i++){
		
			if(arr[i] == x-1){
			
				floor = arr[i];
				break;
			}
		}
		if(ceil != -1 || floor != -1){
			
			System.out.println("Floor of "+x+" is "+floor);
			System.out.println("Ceil of "+x+" is "+ceil);
		}
	}
}

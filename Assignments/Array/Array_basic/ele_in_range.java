
import java.io.*;

class ArrayDemo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size for array");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter elements");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter range");
		int A = Integer.parseInt(br.readLine());
		int B = Integer.parseInt(br.readLine());
		int flag=0;
		for(int i=A; i<=B; i++){
			flag = 0;
			for(int j=0; j<arr.length; j++){
			
				if(i == arr[j]){
					flag=1;
					break;
				}
			}
			if(flag==1)
				continue;
			else
				flag=0;
				break;
		}
		if(flag==1)
			System.out.println("Yes");
		else
			System.out.println("No");

	}
}

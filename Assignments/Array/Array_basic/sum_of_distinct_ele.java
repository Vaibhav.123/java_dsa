
import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size");
		int size = sc.nextInt();

		System.out.println("Enter elements");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		int sum = arr[0];
		for(int i=0; i<arr.length; i++) {
			
			for(int j=i; j<arr.length; j++){
				if(arr[i] == arr[j])
					continue;
				if(arr[i] != arr[j]){
					sum = sum+arr[j];
					break;
				}
			}
		}
		System.out.println("Sum = "+ sum);
	}
}


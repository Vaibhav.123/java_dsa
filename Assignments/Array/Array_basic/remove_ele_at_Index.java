

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENter Size for Array");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter Array Ele");
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter Index");
		int index = Integer.parseInt(br.readLine());
		int arr1[] = new int[size-1];
		for(int i=0,j=0; i<arr.length; i++){
			if(arr == null || index < 0 || index >= arr.length)
				break;

			if(index == i)
				continue;

			arr1[j++] = arr[i];
		}
			
		for(int i=0; i<arr1.length; i++){
		
			System.out.print(arr1[i]+ "  ");
		}
		System.out.println();
	}
}

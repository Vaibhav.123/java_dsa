

import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter String");
		String str = sc.nextLine();

		char ch[] = new char[str.length()];

		int last = -1;
		for(int i=0; i<str.length(); i++){
		
			ch[i] = str.charAt(i);

			if(ch[i] == '1')
				last = i;
		}
		System.out.println("Last occur of 1 = "+last);

	}
}

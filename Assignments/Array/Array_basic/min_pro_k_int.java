
import java.util.*;

class Demo {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter no of integers");
		int k = sc.nextInt();

		for(int i=0; i<arr.length; i++){
		
			for(int j=i+1; j<arr.length; j++){
			
				if(arr[i] > arr[j]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		int pro=1;
		for(int i=0; i<k; i++){
			pro = pro*arr[i];
		}
		System.out.println("Pro = "+pro);

	}
}


import java.io.*;

class Demo {
	
	static int min(int arr[]) {
		int temp = 0;	
		for(int i=0; i<arr.length; i++){
		
			for(int j=i+1; j<arr.length; j++) {
			
				if(arr[i] < arr[j]){
					temp = arr[i];
					arr[i] = arr[j];
					arr[j]= temp;
				}
			}
		}
		return temp;
	}
	static int fun(int arr[], int k){
		
		int max = 0;
		int ret = 0;
		for(int i=0; i<arr.length; i++){
		
			int count = 0;
			for(int j=0; j<arr.length; j++) {
				
				if(arr[j] == arr[i]){
				
					count++;
				}
			}

			if(count > max){
				ret = arr[i];
				max = count;
			}
		}
	
		if(max == k)
			return ret;
		else
			return min(arr);
	}
	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size for Array");
		int size1 = Integer.parseInt(br.readLine());
		System.out.println("Enter Array Ele");
		int arr[] = new int[size1];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter range");
		int k = Integer.parseInt(br.readLine());
		int max = fun(arr,k);

		System.out.println("Max = "+max);
	}
}

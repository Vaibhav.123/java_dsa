

import java.io.*;

class Demo  {
	
	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size for array");
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int [size];

		System.out.println("Enter array elements");
		
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		if(arr.length>2){
			for(int i=0; i<arr.length; i++){

				for(int j=i+1; j<arr.length; j++){
				
					if(arr[i]>arr[j]){
					
						int temp=arr[i];
						arr[i]=arr[j];
						arr[j]=temp;
					}
				}
			}
		}else
			System.out.println("Wrong size");

		System.out.println("Second max number is : "+arr[arr.length-2]);
		
	}
}

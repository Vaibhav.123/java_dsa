

import java.io.*;

class Demo  {
	
	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size for array");
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int [size];

		System.out.println("Enter array elements");
		
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0; i<arr.length; i++){
			int rev=0;
			int x=arr[i];
			while(x!=0){
				int rem=x%10;
				rev=rev*10+rem;
				x=x/10;
			}	
			if(rev==arr[i]){
				
				System.out.println("Palindrome number "+arr[i]+" found at index : "+i);
			}

		}
	}
}


import java.io.*;

class Demo  {
	
	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size for array");
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int [size];

		System.out.println("Enter array elements");
		
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0; i<arr.length; i++){
			int x=arr[i];
			int count=0;

			while(x!=0){
				count++;
				x=x/10;
			}

			System.out.println(arr[i]+ " = "+ count);

		}


	}
}

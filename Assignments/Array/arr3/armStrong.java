

import java.io.*;

class Demo  {
	
	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size for array");
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int [size];

		System.out.println("Enter array elements");
		
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0; i<arr.length; i++){
			int sum=0;
			int x=arr[i];
			int count=0;

			while(x!=0){
				count++;
				x=x/10;
			}
			x=arr[i];

			while(x!=0){
				int rem=x%10;
				int y=1;

				for(int j=1; j<=count;j++){
				
					y=rem*y;
				}
				sum=sum+y;
				x=x/10;
			}

			if(sum==arr[i]){
				
				System.out.println("armStrong number "+arr[i]+" found at index : "+i);
			}

		}
	}
}

//
//  if number is even then print that number in reverse order and
//  if number is odd the print that number in reverse order by diff 2

import java.util.*;
class Demo {
	
	public static void main(String[] atrhs){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number");
		int num = sc.nextInt();

		if(num%2==0){
		
			while(num!=0){
			
				System.out.print(num+"  ");
				num--;
			}
		}else{
		
			while(num!=-1){
			
				System.out.print(num+"  ");
				num=num-2;
			}
		}
		System.out.println();
	
	
	}


}


import java.util.*;
class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number");
		long num = sc.nextLong();
	
		
		while(num!=0){
		
			long rem = num%10;
			if(rem%2==0){
			
				System.out.print(rem*rem+"  ");
			}
			num=num/10;
		}
		System.out.println();
	}
}

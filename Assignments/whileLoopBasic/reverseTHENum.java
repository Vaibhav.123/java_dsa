

import java.io.*;
class Demo {

	public static void main (String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number");
		long num = Long.parseLong(br.readLine());
		long rev = 0;
		while(num!=0){
		
			long rem = num%10;
			rev = (rev*10)+rem;
			num=num/10;
		}
		System.out.println("Reverse number = "+rev);
	}
}


import java.util.*;
class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number ");

		long num = sc.nextLong();
		int count=0;
		long x=num;

		while(num!=0){
		
			num=num/10;
			count++;
		}
		System.out.println("Count of digits of "+x+" = "+count);
	}
}

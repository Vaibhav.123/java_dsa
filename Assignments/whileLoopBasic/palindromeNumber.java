
import java.util.*;
class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number");
		long num = sc.nextLong();
		long rev = 0;
		long val = num;
		while(num!=0){
		
			long rem = num%10;
			rev = (rev*10)+rem;
			num=num/10;
		}
		if(rev==val)
			System.out.println(rev+" is Palindrome number");
		else
			System.out.println(rev+" is not Palindrome number");
	}
}

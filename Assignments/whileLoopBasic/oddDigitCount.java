
import java.io.*;
class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number ");
		long num = Long.parseLong(br.readLine());
		long x = num;
		int count=0;

		while(num!=0){
			long rem = num%10;
			if(rem%2==1){
			
				count++;
			}
			num=num/10;
		}
		System.out.println("Odd count of "+x+" = "+count);
	}
}

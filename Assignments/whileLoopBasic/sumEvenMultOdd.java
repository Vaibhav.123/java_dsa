// Print the sum of all even numbers and multiplication of all odd numbers from the given range


import java.util.*;
class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter range");
		int start = sc.nextInt();
		int end = sc.nextInt();
		int mult = 1;
		int sum = 0;


		while(start!=end+1){
			
			if(start%2==0){
			
				sum = sum+start;
			}else{
			
				mult=mult*start;
			}
			start++;
		}
		System.out.println("Sum of even = "+sum);
		System.out.println("Mult of odd = "+mult);

	}
}

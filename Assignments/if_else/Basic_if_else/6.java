//  WAP to find maximum from the three numbers


import java.util.Scanner;

class Demo{

	public static void main(String[] args){
		int x,y,z;

	
		Scanner s=new Scanner(System.in);

		System.out.println("Enter three numbers: ");
		x=s.nextInt();
		y=s.nextInt();
		z=s.nextInt();

		if(x==y && x>z){

			System.out.println(x+ " and " +y+" are equal and both are greater than "+z);

		}else if(x==z && z>y){
			
			System.out.println(x+ " and " +z+" are equal and both are greater than "+y);

		}else if(y==z && y>x){
			
			System.out.println(y+ " and " +z+" are equal and both are greater than "+x);

		}else if(x>y && x>z && z==y){
		
			System.out.println(z+ " and "+y+ " are equal and " +x+ " is greater");
		
		}else if(y>x && y>z && x==z){
		
			System.out.println(z+ " and "+x+ " are equal and " +y+ " is greater");
		
		}else if(z>y && z>x && x==y){
		
			System.out.println(x+ " and "+y+ " are equal and " +z+ " is greater");	

		}else if(x>y && x>z){
			
			System.out.println(x+" is greater than "+y+ "," +z);
		
		}else if(y>x && y>z){
			
			System.out.println(y+" is greater than " +x+"," +z);

		}else if(x == y && y == z){
			
			System.out.println("Both are equal");
		
		}else{
			
			System.out.println(z+" is greater than " +x+ "," +y);
		}
	}
}

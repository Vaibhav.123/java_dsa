//  Calculate profit and loss
//  WAP that takes the cost price and selling price
//  and calculates its profit or loss


class Demo  {
	public static void main(String[] args){
		
		int sell=200;
		int cost=500;

		if(sell>cost){
			
			System.out.println("Profit of "+(sell-cost));

		}else if(sell==cost){
			
			System.out.println("No profit");

		}else {
			
			System.out.println("Loss of "+(cost-sell));
		}
	}
}

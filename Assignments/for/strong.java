// WAP to take an input from user
// and check number is strong or not
//


import java.util.*;

class Demo {
	
	public static void main (String[] args){
		
		Scanner sc=new Scanner (System.in);
		System.out.print("Enter number:  ");
		int num=sc.nextInt();
		
		int sum=0;
		int temp=num;
		while(num!=0){
			
			int rem=num%10;
			int fact=1;

			for(int i=1; i<=rem; i++){
				
				fact=fact*i;
			}
			sum=sum+fact;
			num=num/10;
		}
		
		if(sum==temp){
			
			System.out.println(temp+" is Strong Number");
		}else{
			
			System.out.println(temp+" is Not Strong Number");
		}
	}
}

//  WAP to take an input from user
//  Print Fibonacci series to the given number
//


import java.util.*;

class Demo {
		
	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number:  ");
		int num=sc.nextInt();
		
		int prev=1;
		int curr=0;
		int sum=0;

		for(int i=1; i<=num; i++){
			
			System.out.print(sum+"  ");

			sum=prev+curr;
			prev=curr;
			curr=sum;
		}
		System.out.println("");
	}
}

//  WAP to print the even numbers from 1 to 100

class Demo {
	
	public static void main(String[] as){
		
		int n=100;

		for(int i=1; i<=n; i++){
			
			if(i%2==0)
				System.out.println(i);
		}
	}
}


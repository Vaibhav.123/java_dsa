//  WAP to take an input from user
//  and check the number is prime or not


import java.util.*;

class Demo  {
	
	public static void main(String[] args){
		
		Scanner sc=new Scanner (System.in);
		System.out.print("Enter number:  ");
		int num=sc.nextInt();
	
		int count=0;
		if(num==0){
			
			System.out.println(num+" is Nor composite nor Prime");
		}else{

			for(int i=2; i<=num/2; i++){
	
				if(num%i==0){
					count=1;
					break;
				}
			}

			if(count==1){
			
				System.out.println(num+" is Composite number");
		
			}else {
			
				System.out.println(num+" is Prime number");
			}
		}
	}
}

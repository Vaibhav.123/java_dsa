//  WAP to take an input from user
//  and check the number is automorphic or not
//

//logic 1: 
//

import java.util.*;

class Demo {
	
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number:  ");
		int num1=sc.nextInt();

		int count=0;
		int temp=num1*num1;
		int num2=num1;
		
		while (num1!=0){
			
			count++;
			num1=num1/10;
		}
		int mod=1;

		for(int i=1; i<=count; i++){
			
			mod=mod*10;

		}

		if(temp%mod==num2){
			
			System.out.println(num2+" is Automorphic number");
	
		}else {
		
			System.out.println(num2+" is not Automorphic number");
		}
	}
}

// Logic 2:
//
/*
import java.util.*;

class Demo {
	
	public static void main(String[]as){
		
		Scanner sc=new Scanner (System.in);
		System.out.print("Enter number:  ");
		int num1=sc.nextInt();
		int count=0;
		int temp=num1*num1;
		int temp2=num1;
		int rev1=0;
		int rev2=0;

		while(num1!=0){
			
			count++;
			num1=num1/10;
		}

		for (int i=1; i<=count; i++){
			
			int rem=temp%10;
			rev1=rev1*10+rem;
			temp=temp/10;

		}

		while(rev1!=0){
			
			int rem=rev1%10;
			rev2=rev2*10+rem;
			rev1=rev1/10;

		}

		if(rev2==temp2){
		
			System.out.println(temp2+" is Automorphic number");
		}else{
		
			System.out.println(temp2+" is not Automorphic number");
		}

	}
}
*/
// Logic 3:

/*
import java.util.*;

class Demo {
	
	public static void main(String[] as){
		
		Scanner sc=new Scanner (System.in);
		System.out.print("Enter number:  ");
		int num=sc.nextInt();

		int flag=0;
		int temp=num*num;

		for(int i=num; i!=0; i=i/10){
			
			if(temp%10 == num%10){
				
				flag=1;
			}
	
			temp=temp/10;
			num=num/10;

		}

		if(flag==1){
			
			System.out.println("It is Automorphic number");
		}else{
			System.out.println("It is not Automorphic number");
		}
	}
}
*/

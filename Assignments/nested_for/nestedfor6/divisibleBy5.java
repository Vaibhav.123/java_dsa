//  WAp to print the numbers didvisible by 5 and the number is even also print the count of even numbers


import java.io.*;
class Demo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter lower limit: ");
		int low = Integer.parseInt(br.readLine());
		System.out.println("Enter upper limit: ");
		int upp = Integer.parseInt(br.readLine());
		
		int count=0;
		for(int i=low; i<=upp; i++){
		
			if(i%5==0 && i%2==0){

				System.out.print(i+"   ");
				count++;
			}
		}
		System.out.println();
		System.out.println("Count of even = "+count);
	}
}

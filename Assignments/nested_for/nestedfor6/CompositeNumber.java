// WAp to take range as input from user and print the composite numbers

import java.io.*;
class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter lower limit : ");
		int low = Integer.parseInt(br.readLine());
		System.out.println("Enter upper limit : ");
		int upp = Integer.parseInt(br.readLine());

		for(int i=low; i<=upp; i++){
			int count = 0;
			for(int j=1; j<=i; j++){
			
				if(i%j==0){
					count++;
				}
			}
			if(count>2){
			
				System.out.print(i+"   ");
			}
		}
		System.out.println();
	}
}



/*    A   b   C   d
 *    E   f   G   h
 *    I   j   K   l
 *    M   n   O   p 
 */


class Demo  {
	
	public static void main( String[] as){
		

		int row=4;
		char ch1='A';
		char ch2='a';
		int cnt=0;

		for(int i=1; i<=row; i++){
			
			if(i%2==1){
			
				System.out.print(ch1++ + "    ");
				ch2++;
				
			}else {
				
				System.out.print(ch2++ +"    ");
				ch1++;
			}

			if(i%row==0){
				
				System.out.println();
				i=0;
				cnt++;
			}

			if(cnt==row)
				break;
		}
	}
}

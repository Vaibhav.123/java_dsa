
/*     1	2	 9
 *     4	25	 6
 *     49	8	 81
 */

class Demo {
	
	public static void main(String[] args){
		
		int row=3;
		int cnt=0;
		int x=1;

		for(int i=1; i<=row; i++){
			
			if(x%2==0){
				System.out.print(x++ +"		");
			}else {
				
				System.out.print(x*x +"		");
				x++;
			}

			if(i%row==0){
				
				System.out.println();
				i=0;
				cnt++;

			}
			if(cnt==row)
				break;
		}
	}
}

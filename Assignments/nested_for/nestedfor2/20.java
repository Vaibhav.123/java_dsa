
/*   F   5   D   3   B   1 
/*   F   5   D   3   B   1 
/*   F   5   D   3   B   1 
/*   F   5   D   3   B   1 
/*   F   5   D   3   B   1 
/*   F   5   D   3   B   1 
 */


class Deno {

	public static void main(String[] args){
		
		int row=6;
		char ch='F';
		int x=row;
		int cnt=0;

		for(int i=1; i<=row; i++){
			
			if(i%2==1){
				
				System.out.print(ch-- +"   ");
				x--;
			}else{
				
				System.out.print(x-- +"   ");
				ch--;

			}

			if(i%row==0){
				
				System.out.println();
				ch='F';
				x=row;
				cnt++;
				i=0;
			}
			if(cnt==row){
				
				break;
			}
		}


		
	}
}

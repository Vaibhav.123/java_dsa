
/*   4  4  4  4
 *   5  5  5  5
 *   6  6  6  6
 *   7  7  7  7
 */

class Denmo {
	
	public static void main(String[] as ){
		
		int row=4;
		int x=row;
		int cnt=0;


		for(int i=1; i<=row; i++){
			
			System.out.print(x+ "   ");

			if(i%row==0){
			
				System.out.println();
				x++;
				cnt++;
				i=0;

			}
			if(cnt==row){
				
				break;
			}
		}
	}
}

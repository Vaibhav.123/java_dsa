
/*    1C3   4B2   9A1
 *    16C3  25B2  36A1
 *    49C3  64B2  81A1


*/


class Demo {
	
	public static void main(String[] args){
		
		int row=3;
		char ch='C';
		int cnt=0;
		int x=row;
		int y=1;


		for(int i=1;i<=row; i++){
			
			System.out.print(y*y +""+ch-- +""+x-- +"	");
			y++;

			if(i%row==0){
				
				System.out.println();
				i=0;
				cnt++;
				x=row;
				ch='C';
				
			}
			if(cnt==row)
				break;
		}

	}
}

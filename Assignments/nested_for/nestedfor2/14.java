

/*   1A  2B  3C  4D
/*   1A  2B  3C  4D
/*   1A  2B  3C  4D
/*   1A  2B  3C  4D
 */

class Demo  {
	
	public static void main(String[] args){
	
		int row=4;

		char ch='A';
		int cnt=0;

		for(int i=1; i<=row; i++){
			
			System.out.print(i +""+ ch++ +"   ");

			if(i%row==0){
				System.out.println();
				ch=65;
				cnt++;
				i=0;
			}

			if(cnt==row){
				
				break;
			}
		}
	}
}


/*   26  Z  25  Y
 *   24  X  23  W
 *   22  V  21  U
 *   20  T  19  S
 */


class Demo  {
	
	public static void main(String [] as){
		
		int row=4;
		int x=26;
		char ch=90;
		int cnt=0;


		for (int i=1; i<=row; i++){
		
			if(i%2==1){
				
				System.out.print(x-- +"  ");
			}
			else {
				
				System.out.print(ch-- +"  ");
			}

			if(i%row==0){
			
				System.out.println();
				i=0;
				cnt++;

			}
			if(cnt==row)
				break;
		}
	}
}

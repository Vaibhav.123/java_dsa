
/*     1	4	9
 *     16	25	36
 *     49	64	81
 *
 */


class Demo {
	
	public static void main(String[] asr){
		
		int row=3;
		int x=1;
		int cnt=0;

		for(int i=1; i<=row; i++){
			
			System.out.print(x*x +"	       ");
			x++;

			if(i%row==0){
				
				System.out.println();
				i=0;
				cnt++;
				
			}

			if(cnt==row){
				
				break;
			}
		}
	}
}


/*   14  15  16  17 
/*   15  16  17  18 
/*   16  17  18  19 
/*   17  18  19  20
 */

class Demno  {
	
	public static void main(String[] as){
		
		int row=4;
		int x=14;
		int cnt=0;

		for(int i=1; i<=row; i++){
			
			System.out.print(x++ +"   ");

			if(i%row==0){
				
				System.out.println();
				x=x-3;
				i=0;
				cnt++;
			}

			if(cnt==row){
				
				break;
			}
		}
	}
}

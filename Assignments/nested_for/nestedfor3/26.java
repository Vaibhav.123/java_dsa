
/*   9   
 *   9  8
 *   9  8  7
 *   9  8  7  5
 *   
 */


import java.util.*;

class Java  {
	
	public static void main(String[] s){

	Scanner sc=new Scanner (System.in);
	System.out.print("Enter row:    ");
	int row=sc.nextInt();
	
	int num=(row*(row+1)/2)-1;
	int x=1;

	for(int i=1; i<=x; i++){
		
		System.out.print(num-- +"   ");

		if(i==x){

			System.out.println();
			i=0;
			x++;
			num=num+x-1;
			
		}
		if(x==row+1)
			break;
		
	}

	}
		
}

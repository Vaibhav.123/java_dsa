
/*   1  
 *   2   3
 *   4   5   6
 *   7   8   9   10
 */


import java.util.*;

class Java  {
	
	public static void main(String[] s){

	Scanner sc=new Scanner (System.in);
	System.out.print("Enter row:    ");
	int row=sc.nextInt();

	int x=1;
	int num=1;

	for(int i=1; i<=x; i++){
		
		System.out.print(num++ + "   ");

		if(i==x){

			System.out.println();
			i=0;
			x++;
		}
		if(x==row+1)
			break;
	}

	}
		
}

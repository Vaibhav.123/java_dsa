
/*   3C   3C   3C   3C
 *   3C   3C   3C
 *   3C   3C
 *   3C 
 */


import java.util.*;

class Java  {
	
	public static void main(String[] s){

	Scanner sc=new Scanner (System.in);
	System.out.print("Enter row:    ");
	int row=sc.nextInt();

	for(int i=1; i<=row; i++){
		
		System.out.print("3C  ");

		if(i==row){

			System.out.println();
			i=0;
			row--;
		}
		
	}

	}
		
}

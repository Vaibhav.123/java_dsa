
/*   F
 *   E  F
 *   D  E  F
 *   C  D  E  F
 *   B  C  D  E  F
 *   A  B  C  D  E  F
 *   
 */


import java.util.*;

class Java  {
	
	public static void main(String[] s){

	Scanner sc=new Scanner (System.in);
	System.out.print("Enter row:    ");
	int row=sc.nextInt();
	
	char ch=(char)(64+row);

	int x=1;

	for(int i=1; i<=x; i++){
		
		System.out.print(ch++ +"   ");

		if(i==x){

			System.out.println();
			i=0;
			x++;
			ch=(char)(ch-x);
			
		}
		if(x==row+1)
			break;
		
	}

	}
		
}


/*   10   10   10   10
 *   11   11   11
 *   12   12
 *   13
 */


import java.util.*;

class Java  {
	
	public static void main(String[] s){

	Scanner sc=new Scanner (System.in);
	System.out.print("Enter row:    ");
	int row=sc.nextInt();
	
	int num=row*(row+1)/2;

	for(int i=1; i<=row; i++){
		
		System.out.print(num +"   ");

		if(i==row){

			System.out.println();
			i=0;
			row--;
			num++;
		}
		
	}

	}
		
}

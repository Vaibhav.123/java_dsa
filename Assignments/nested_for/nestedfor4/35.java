
/*      A  B  C  D
 *      B  C  D
 *      C  D
 *      D
 *   
  */


import java.util.*;

class Java  {
	
	public static void main(String[] s){

	Scanner sc=new Scanner (System.in);
	System.out.print("Enter row:   ");
	int row=sc.nextInt();
	
	char ch='A';

	for(int i=1; i<=row; i++){
		
		System.out.print(ch++ +"   ");

		if(i==row){

			System.out.println();
			i=0;
			row--;
			ch=(char)((ch-row));
		
			
		}
		
	}

	}
		
}

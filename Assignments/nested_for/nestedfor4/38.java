
/*      10
 *      I    H
 *      7    6    5
 *      D    C    B    A
 *
  */


import java.util.*;

class Java  {
	
	public static void main(String[] s){

	Scanner sc=new Scanner (System.in);
	System.out.print("Enter row:   ");
	int row=sc.nextInt();
	
	int num=row*(row+1)/2;
	int x=1;
	char ch=(char)(64+num);

	for(int i=1; i<=x; i++){
		
		if(x%2==1){
			System.out.print(num +"   ");
			
		}else {
			System.out.print(ch +"   ");
		}
		num--;
		ch--;

		if(i==x){

			System.out.println();
			i=0;
			x++;
		}
		if(x==row+1)
			break;
		
	}

	}
		
}



class Employee{

	int empId = 10;
	String name = "Vaibhav";
	static int y = 50;

	void EmpInfo () {
	
		System.out.println("ID = "+empId);
		System.out.println("Name = "+name);
		System.out.println("Y = "+y);
	}
}

class MainDemo {

	public static void main(String[] args)  {
	
		Employee emp1 = new Employee();
		Employee emp2 = new Employee();

		emp1.EmpInfo();
		emp2.EmpInfo();

		System.out.println("-----------------------------");

		emp1.empId = 20;
		emp1.name = "Sagar";
		emp1.y = 100;
		
		emp1.EmpInfo();
		emp2.EmpInfo();

	}
}

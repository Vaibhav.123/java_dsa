

class Employee {

	int empId = 10;
	String name = "Vaibhav";

	void EmpInfo() {
	
		System.out.println("ID = "+empId);      // ID = 10
		System.out.println("Name = "+name);     // Name = Vaibhav
	}

}

class MainDemo {

	public static void main(String[] args) {
	
		Employee emp = new Employee();

		emp.EmpInfo();
		
		System.out.println(emp.empId);  // 10
		System.out.println(emp.name);   // Vaibhav

	}


}




class Demo {

	int x = 10;
	private String str = "JAVA";

	void disp(){
	
		System.out.println(x);
		System.out.println(str);
	}
}
class Main{

	public static void main(String[] args) {
	
		Demo obj = new Demo();
		obj.disp();
		
		System.out.println(obj.x);
		System.out.println(obj.str);  // error:  str has an private access in Demo,
					      // private variables are access only within that class not out of the class

	}
}

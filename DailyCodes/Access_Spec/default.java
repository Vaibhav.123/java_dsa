
//  default cant access out of the folder
//  it can access in class and it can access in file also 
//  but not another folder


class Core2Web  {

	int numOfCourse = 3;       // default access specifier
	String courseName = "JAVA";   // deafault access specifier

	void disp(){
	
		System.out.println(numOfCourse);
		System.out.println(courseName);
	}
}

class Student {

	public static void main(String[] args) {
	
		Core2Web obj = new Core2Web();

		obj.disp();

		System.out.println(obj.numOfCourse);
		System.out.println(obj.courseName);
	}
}

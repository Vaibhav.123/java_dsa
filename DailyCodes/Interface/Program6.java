//  in interface we can give body also
//  but those methods are define with keyword static or default 
// we cannot access the methods have body in interface by using the object or ref
// compulsory access using class name only

interface Demo {

	static void fun(){
	
		System.out.println("In Static Fun");
	}
	static void gun(){
	
		System.out.println("In Static Gun");
	}
	default void run(){
	
		System.out.println("In Default run");
	}
}

class Child implements Demo {


}

class Client {

	public static void main(String[] args)  {
		
		Demo obj = new Child();
		obj.run();
		Demo.fun();
		Demo.gun();
	}
}

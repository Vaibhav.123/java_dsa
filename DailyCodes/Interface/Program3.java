// in the below code
// we can implements Demo interface in abstract class 
// and in concrete class we can inherit the abstract class
//

interface Demo {

	void fun();
	void gun();
}

abstract class DemoChild implements Demo {

	public void gun(){
	
		System.out.println("In Gun");
	}
}

class Child extends DemoChild{

	public void fun() {
	
		System.out.println("In Fun");
	}
}

class Client {

	public static void main(String[] args)  {
	
		Demo obj = new Child();
		obj.fun();
		obj.gun();
	}
}

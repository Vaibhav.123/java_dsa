// firstly default 
//  here on the reference of Demo we can call both the methods of Demo and Child class
//

interface Demo {

	default void fun(){
	
		System.out.println("In fun-Demo");
	}	
	void gun();
}

class Child implements Demo {

	public void gun(){
	
		System.out.println("In gun-Child");
	}
}

class Client {

	public static void main(String[] afs) {
	
		Demo obj = new Child();
		obj.fun();
		obj.gun();
	}
}

// op: In fun-Demo
//     In gun-Child

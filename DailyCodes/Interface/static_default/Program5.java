 // solution for this 
 // 
 //
interface Demo1 {

	default void fun(){
	
		System.out.println("In fun-Demo1");
	}
}

interface Demo2 {

	 default void fun(){
	 
		 System.out.println("In fun-Demo2");
	 }
}

class DemoChild implements Demo1,Demo2 {

	public void fun(){
	
		System.out.println("In fun-DemoChild");
	}
}

class Client {

	public static void main(String[] args)  {
		
		Demo1 obj1 = new DemoChild();
		obj1.fun(); 
	
		Demo2 obj2 = new DemoChild();
		obj2.fun();
		
		Demo2 obj3 = new DemoChild();
		obj3.fun();
	}
}
// OP:  
// In fun-DemoChild
// In fun-DemoChild
// In fun-DemoChild

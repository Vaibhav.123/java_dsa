//  if we write default then 
//  we can override the method in class also
//  if we want to write common methods in child class then 
//  for reduce code redundancy 
//  we have write those methods in interface using default 
//  if we write using default then we can override the methods in child class also 
//  if we want to write one method for few classes common and for few classes different then 
//  for those classes does not want same method then they have chance to override it
//  for this purpose default is been introduced
//
//  but java says if you have to write common method and it will be in interface 
//  then why ypu are again override it in child class 
//  but in some cases this will happened so default keyword is used 
//
//
interface Demo {

	default void fun(){
	
		System.out.println("In fun-Demo");
	}
}

class Child implements Demo {

	public void fun(){
	
		System.out.println("In fun-Child");
	}
}

class Client {

	public static void main(String[] args){
	
		Demo obj = new Child();
		obj.fun();
	}
}

// we cannot call static methods using refernce or object

interface Demo {

	static void fun(){
	
		System.out.println("In fun-Demo");
	}
}
class Child implements Demo {

	public void fun(){
	
		System.out.println("In fun-Child");
	}
}

class Client {

	public static void main(String[] atrgs ) {
	
		Demo obj = new Child();
		obj.fun();  // error : illegal static interface method call 
	}
}

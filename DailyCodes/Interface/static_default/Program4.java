 // solution for this 
 // we compulsory override method in child class
interface Demo1 {

	default void fun(){
	
		System.out.println("In fun-Demo1");
	}
}

interface Demo2 {

	 default void fun(){
	 
		 System.out.println("In fun-Demo2");
	 }
}

class DemoChild implements Demo1,Demo2 {


}

class Client {

	public static void main(String[] args)  {
	
		Demo1 obj =new DemoChild();
		obj1.fun();    // error : ambiguity for this
	}
}

// here in this code
// static and default are used for giving body to the method in interface
// both methods are public notice it

interface Demo {

	static void fun(){     // public static void fun()
	
		System.out.println("In fun-Demo");
	}
	default void gun(){    // public default void gun()
	
		System.out.println("In gun-Demo");
	}
}

class Child implements Demo {


}
class Client {

	public static void main(String[] args)  {
	
		Demo obj = new Child();
		Demo.fun();
		obj.gun();
	}
}

// suppose we make refernce of Demo interface and 
// call the static function of Demo interface then error 
// because static in interface will access only from the name of the interface
// there is no other way to access it

interface Demo {

	static void fun(){

		System.out.println("In fun-Demo");
	}
}
class Child implements Demo {}

class Client {

	public static void main(String[] args)  {
	
		Demo obj = new Child();// here reference is Demo interface and object of child
		obj.fun();    // error: illegal start expression

	}
}

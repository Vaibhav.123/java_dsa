

interface Demo {

	int x = 10;
	static int y = 20;
	static void fun(){
		
		System.out.println("X = "+x);      // we print by this also
		
		System.out.println("Y = "+y);
		
		System.out.println("Demo.X = "+Demo.x);    // we can access by this also
		
		System.out.println("Demo.Y = "+Demo.y);
	}
}

class Client {

	public static void main(String[] args) {
	
		Demo.fun();
	}
}

// we can write same name variable in interface 
// but at the time of access it will access only using the interface name only 
// if there is only one onterface at that we can access using variable name also but 
// if that type name of variable is present in that class also then 
// it will consider that class variable not interface variable

interface A {

	int x = 10;

	int y = 20;

	int z = 30;
}
interface B {

	int x = 10;

	int y = 20;

	int z = 30;
}

class Child implements A,B {
	
	int z = 40;
	
	void fun(){
	//	System.out.println(x);   // error: x is ambiguos here 
		
		System.out.println(A.y);   // 20
		System.out.println(B.y);   // 20
		
		System.out.println(z);    // 40
	}
}


class Client {

	public static void main(String[] args) {
	
		Child obj = new Child();
		obj.fun();;
	}
}

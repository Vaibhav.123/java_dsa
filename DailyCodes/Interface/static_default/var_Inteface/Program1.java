
//  in this code 
//  x variable is in interface and it is internally called as given below 
//  it is static and final and also public
//  why static :
//  		it is accessible only for that interface and it is common for all classes given below
//  		access only using inteface name 
//
interface Demo {

	int x = 10;       // internally: public static final int x

	void fun();      // public abstract void fun()
}

class DemoChild implements  Demo {

	public void fun(){
	
		System.out.println("In fun-DemoChild");
		System.out.println(x);      
	}
}

class Client {

	public static void main(String[] args) {

		Demo obj = new DemoChild();

		obj.fun();
	}
}



interface Demo1 {

	static void fun(){
	
		System.out.println("In fun-Demo1");
	}
}

interface Demo2 {

	static void fun(){
	
		System.out.println("In fun-Demo2");
	}
}

class Child implements Demo1,Demo2 {

	public void fun(){
	
		System.out.println("In fun-Child");
		Demo1.fun();
		Demo2.fun();
	}
}
class Client {

	public static void main(String[] args)  {
	
		Child obj = new Child();
		obj.fun();
	}
}

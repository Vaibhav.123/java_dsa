// static =>
// for interface if we write static method in interface and then we implements interface in child class 
// and access that methopd at that error will occured
// because in interface static cannot be inherits in child class
// that is only for interface 
// also it cannot override 
// for interface only

interface Demo {

	static void fun(){
	
		System.out.println("In fun-Demo");
	}
}
class Child implements Demo{
}

class Client {

	public static void main(String[] args) {
	
		Child obj = new Child();
		obj.fun();   // error: cannot find symbol 
	}
}

//  static for class
// static for class different from static in interface
// here in class static can be inherited by child class also
class Demo {

	static void fun(){
	
		System.out.println("In fun-Demo");
	}
}
class Child extends Demo{
}

class Client {

	public static void main(String[] args) {
	
		Child obj = new Child();
		obj.fun(); 
	}
}

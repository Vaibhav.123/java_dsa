// static methods in interface will be access inly usiong interface name only

interface Demo {

	static void fun() {
	
		System.out.println("In fun-Demo");
	}
}

class Child implements Demo {
	
	Demo.fun();
}


class Client {

	public static void main(String[] args) {
	
		Demo.fun();
	}

}

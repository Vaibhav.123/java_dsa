
interface Demo1 {

	void fun();
}

interface Demo2 {

	void gun();
}

class Child implements Demo1,Demo2 {

	public void fun(){
	
		System.out.println("In Fun");
	}
	public void gun(){
	
		System.out.println("In Gun");
	}
}

class Client extends Child{

	public static void main(String[] args) {
	
		Client obj = new Client();
		obj.fun();
		obj.gun();
	}
}

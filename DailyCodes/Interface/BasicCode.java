// this is the basic code of Interface 
// here interface has reference but we cant make object of it
// those methods are defined in interface that are public abstract methods 
// and we have to compulsory that to implements that methods in child class but that are compulsory public 
// in interface we cant give body to the method
// in interface implements keyword is used for implementation of method in child class
//

interface Demo {
	int x = 10;
	void fun();
	void gun();
}

class DemoChild implements Demo {

	public void fun(){   // public is compulsory
	
		System.out.println("In Fun");
	}
	public void gun(){     // public is compulsory
	
		System.out.println("In Gun");
	}
}

class Client {

	public static void main(String[] args)  {
	
		Demo obj = new DemoChild();   // ref of interface is allowed here not object
		obj.fun();
		obj.gun();

	}
}

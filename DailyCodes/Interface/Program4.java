// multiple inheritance in java using interface
// there is multiple interfaces and only one child class implements both the interfaces
//  as like that java supports multiple inheritance
//

interface Demo1 {

	void fun();
}
interface Demo2 {

	void fun();
}

class DemoChild implements Demo1,Demo2 {
	
	public void fun(){
	
		System.out.println("In fun");
	}
}

class Client {

	public static void main(String[] rgs) {
	
		Demo1 obj1 = new DemoChild();
		Demo2 obj2 = new DemoChild();
		obj1.fun();
		obj2.fun();
	}
}


// error: Demo is abstract, cannot be instantiated 
// it means we cannot create object od interface
interface Demo {

	void fun();
	void gun();
}
class Client {

	public static void main(String[] args) {
	
		Demo obj = new Demo();
	}
}



class Logical {

	public static void main(String[] args){
		
		int x=5;
		int y=7;

		boolean ans1= x<y && y<x;	//false
		boolean ans2= x<y || y<x;	//true
	//	boolean ans3= x && y;		// Error 
		System.out.println(ans1);
		System.out.println(ans2);
	}
}

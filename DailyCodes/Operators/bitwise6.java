

class Bitwise  {
	
	public static void main(String[] args){
	
		int x= -7;

		System.out.println(x >>> 31);		// 1

		System.out.println(x);			// -7
		System.out.println(x >>> 30);		// 3
		System.out.println(x >>> 29);		// 7 
		System.out.println(x >>> 28);		// 15
	}
}

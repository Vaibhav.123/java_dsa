

class Bitwise {
	
	public static void main(String[] args){
		
		int x=8;
		int y=10;

		System.out.println(x << 2);	// 32
		System.out.println(y >> 2);	// 2
		
		int a=132;
		int b=75;

		System.out.println(a >> 5);	//4
		System.out.println(b << 2);	//300

	}
}

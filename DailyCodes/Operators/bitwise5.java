

class Bitwise {
	
	public static void main(String[] args){
		
		int x=5;

		System.out.println(x);		// 5
		System.out.println(~x);		// -6
		
		int y=132;

		System.out.println(y);		// 132
		System.out.println(~y);		// -133

	}
}

class Movie {
	static float price = 150.50f;
	static String name = "Kerala Story";
	int seatNo = 5;
	int ticketNo = 15;

	void Info(){
	
		System.out.println("Name = "+ name);           
		System.out.println("Price = "+ price);         
		System.out.println("Seat Number = "+ seatNo);  
		System.out.println("Ticket Number = "+ticketNo);
	}
}
class MainDemo {

	public static void main(String[] args) {
	
		Movie mov1 = new Movie();
		Movie mov2 = new Movie();

		mov1.Info();   
		mov2.Info();

		mov2.price = 200.00f;
		mov2.name = "Shiddat";
		mov2.seatNo = 10;
		mov2.ticketNo = 20;
		
		System.out.println("\n**********************\n");

		mov1.Info();
		mov2.Info();
	}
}

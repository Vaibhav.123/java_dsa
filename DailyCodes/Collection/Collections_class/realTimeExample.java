
import java.util.*;
       
class Application {
	
	String aName = null;	
	float rating = 0.0f ;
	double users = 0;

	Application(String aName, float rating, double users){
	
		this.aName = aName;
		this.rating = rating;
		this.users = users;
	}

	public String toString(){
	
		return "{"+aName+" : "+rating+" | "+users+"}";
	}
}

class SortByName implements Comparator {
	
	public int compare(Object obj1, Object obj2){
	
		return (((Application)(obj1)).aName).compareTo(((Application)(obj2)).aName);
	}
}

class SortByRating implements Comparator {
	
	public int compare(Object obj1, Object obj2){
	
		return (int)(((Application)(obj1)).rating - (((Application)(obj2)).rating));
	}
}

class SortByUsers implements Comparator {
	
	public int compare(Object obj1, Object obj2){
	
		return (int)(((Application)(obj1)).users - ((Application)(obj2)).users);
	}
}
class Client {

	public static void main(String[] args) {
	
		LinkedList ll = new LinkedList();

		ll.add(new Application("Snapdeal",4.50f,120.5));
		ll.add(new Application("Myntra",2.5f,15.1));
		ll.add(new Application("Amazon",5.5f,300.5));
		ll.add(new Application("Flipkart",4.3f,167.0));
		
		System.out.println("Before Comparison : ");
		System.out.println(ll);
		System.out.println();
		
		Collections.sort(ll, new SortByName());
		System.out.println("Using Application Name Comparison : ");
		System.out.println(ll);
		System.out.println();

		Collections.sort(ll,new SortByRating());
		System.out.println("Using Application rating Comparison : ");
		System.out.println(ll);
		System.out.println();

		Collections.sort(ll,new SortByUsers());
		System.out.println("Using Application users Comparison : ");
		System.out.println(ll);
		System.out.println();

	}
}

// insertion order not preserved

import java.util.*;

class Demo {
	
	String str = null;
	Demo(String str) {
	
		this.str = str;
	}
	public String toString(){
	
		return str;
	}
}
class HashSetDemo {

	public static void main(String[] args)  {

		HashSet hs = new HashSet();

		hs.add(new Demo("Vaibhav"));
		hs.add(new Demo("Sagar"));
		hs.add(new Demo("Tejas"));
		hs.add(new Demo("Raj"));

		for(Object obj : hs){
		
			System.out.println(obj);
		}
		System.out.println(hs);
	}
}

import java.util.*;

class NavigableSetDemo {

	public static void main(String[] args) {
	
		NavigableSet ns = new TreeSet();

		ns.add("Vaibhav");
		ns.add("Sagar");
		ns.add("Pranav");
		ns.add("Abhi");
		ns.add("Sanket");
		ns.add("Tejas");

		System.out.println(ns);  // [Abhi Pranav, Sagar, Sanket, Tejas, Vaibhav]

		System.out.println(ns.lower("Sanket"));  // Sagar
		
		System.out.println(ns.lower("Abhi"));  // null
		
		System.out.println(ns.floor("Vaibhav"));  // Vaibhav
		
		System.out.println(ns.ceiling("Sagar"));  // Sagar
		
		System.out.println(ns.higher("Abhi"));  // Pranav

		System.out.println(ns.pollFirst());   // Abhi
		
		System.out.println(ns.pollLast());    // Vaibhav
		
		//descendingSet method remove first and last elements from the set and print between them in descending order

		System.out.println(ns.descendingSet());   // [Tejas, Sanket, Sagar, Pranav]

		System.out.println(ns);

		System.out.println(ns.ceiling("Vaibhav"));   // null
	}
}

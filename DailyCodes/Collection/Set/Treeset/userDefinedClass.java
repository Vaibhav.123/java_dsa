import java.util.*;

class MyClass implements Comparable {
	
	String str = null;
	MyClass(String str) {
	
		this.str = str;
	}
	
	public String toString(){
	
		return str;
	}

	public int compareTo(Object obj){
	
		return str.compareTo(((MyClass)(obj)).str);
	}
}

class TreeSetDemo {

	public static void main(String[] arhs)  {
	
		TreeSet ts = new TreeSet();

		ts.add(new MyClass("Vaibhav"));
		ts.add(new MyClass("Sagar"));
		ts.add(new MyClass("Abhii"));
		ts.add(new MyClass("Pranav"));

		System.out.println(ts);
	}
}


import java.util.*;

class ProgLang implements Comparable {
	
	String lName = null;
	String fName = null;
	int year = 0;

	ProgLang(String lName, String fName, int year) {
	
		this.lName = lName;
		this.fName = fName;
		this.year = year;
	}

	public String toString(){
	
		return "{"+lName+" : "+fName+","+year+"}";
	}

	public int compareTo(Object obj){
	
		return year - (((ProgLang)(obj)).year);
	}

}
class Client {

	public static void main(String[] args) {
	
		TreeSet ts = new TreeSet();

		ts.add(new ProgLang("C","Denis Ritchie",1972));
		ts.add(new ProgLang("Java","James Gosling",1995));
		ts.add(new ProgLang("Python","Guido van Rossum",1991));
		ts.add(new ProgLang("Cpp","Bjarne Stroustrup",1979));

		System.out.println(ts);
	}
}

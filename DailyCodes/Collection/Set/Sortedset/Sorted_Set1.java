// sorted Set [interface]
//  here all the methods of SortedSet interface will be written
import java.util.*;
class SortedSetDemo {

	public static void main(String[] args)  {
	
		SortedSet ss = new TreeSet();

		ss.add("Kanha");
		ss.add("Rajesh");
		ss.add("Rahul");
		ss.add("Ashish");
		ss.add("Badhe");

		System.out.println(ss);  // [Ashish, Badhe, Kanha, Rahul, Rajesh]

		System.out.println(ss.subSet("Badhe","Rahul"));  // [Badhe, Kanha]
		
		System.out.println(ss.headSet("Rahul"));  // [Ashish, Badhe, Kanha]
		
		System.out.println(ss.tailSet("Kanha"));   // [Kanha, Rahul, Rajesh]
		
		System.out.println(ss.first());   // Ashish
		
		System.out.println(ss.last());   // Rajesh
	}
}

/*  public E push(E);
  public synchronized E pop();
  public synchronized E peek();
  public boolean empty();
  public synchronized int search(java.lang.Object);
}
*/
import java.util.*;

class StackDemo {

	public static void main(String[] args)  {
	
		Stack s = new Stack();

		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);

		System.out.println(s);    // [10, 20, 30, 40]
			
		System.out.println(s.search(30));    // nothing is print or no error will be there
					             // where there is element present or not at that time
		System.out.println(s.peek());	 // 40

		System.out.println(s.pop());     // 40

		System.out.println(s);        //[10, 20, 30]
	}
}

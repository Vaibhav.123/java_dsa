/* public synchronized int capacity();
  public synchronized int size();
  public synchronized boolean isEmpty();
  public java.util.Enumeration<E> elements();
  public boolean contains(java.lang.Object);
  public int indexOf(java.lang.Object);
  public synchronized int indexOf(java.lang.Object, int);
  public synchronized int lastIndexOf(java.lang.Object);
  public synchronized int lastIndexOf(java.lang.Object, int);
  public synchronized E elementAt(int);
  public synchronized E firstElement();
  public synchronized E lastElement();
  public synchronized void setElementAt(E, int);
  public synchronized void removeElementAt(int);
  public synchronized void insertElementAt(E, int);
  public synchronized void addElement(E);
  public synchronized boolean removeElement(java.lang.Object);*/

import java.util.*;
class VectorDemo {

	public static void main(String[] args)  {
	
		Vector v = new Vector();
		
		v.addElement(10);
		v.addElement(20.5);
		v.addElement("Vaibhav");
		v.addElement(30.5f);

		System.out.println(v);

		System.out.println(v.capacity());  // default capacity = 10, increases by into 2
						   // o/p = 10
		System.out.println(v.size());    // 4

		System.out.println(v.firstElement());   // 10

		System.out.println(v.lastElement());    // 30.5
 
  		System.out.println(v.elementAt(2));       // Vaibhav


	}
}

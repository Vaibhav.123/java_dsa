
import java.util.*;


class Demo {
	
	String str = null;
	Demo(String str) {
	
		this.str = str;
	}

	public String toString(){
	
		return str;
	}

}

class ArrayListDemo {

	public static void main(String[] args) {
	
		ArrayList al = new ArrayList();

		al.add(10);
		al.add("vaibhav");
		al.add(new Demo("Raj"));

		for(Object obj : al){
		
			System.out.println(obj);
		}
		for(var obj : al){
		
			System.out.println(obj);
		}
	}
}

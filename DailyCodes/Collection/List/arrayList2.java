
import java.util.*;

class CricPlayer {

	int jerNo = 0;
	String str = null;
	
	CricPlayer(String str, int jerNo) {
	
		this.str = str;
		this.jerNo = jerNo;
	}

	public String toString(){
		
		return str+ ":"+jerNo;

	}
}

class ArrayListDemo {

	public static void main(String[] args)  {
	
		ArrayList obj = new ArrayList();

		obj.add(new CricPlayer("Virat",18));
		obj.add(new CricPlayer("Rohit",45));
		obj.add(new CricPlayer("Hardik",33));
		
		obj.add(1,new CricPlayer("Dhoni",07));
		
	//	System.out.println(obj.get(1));
		System.out.println(obj);
	}
}

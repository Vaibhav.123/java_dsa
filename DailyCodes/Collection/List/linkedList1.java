

import java.util.*;

class Employee {
	
	int empId = 0;
	String empName = null;

	Employee(String name, int id){
	
		this.empId = id;
		this.empName = name;
	}

	public String toString(){
	
		return empName +" = "+empId;
	}
}

class LinkedListDemo {

	public static void main(String[] args) {
	
		LinkedList ll = new LinkedList();

		ll.add(new Employee("Vaibhav",101));
		ll.addLast(new Employee("Abhishek",102));
		ll.addFirst(new Employee("Sagar",104));
		ll.add(new Employee("Raj",105));
		
		Iterator itr = ll.iterator();
		while(itr.hasNext()){
		
			System.out.print(itr.next()+" => ");
		}
		System.out.println();
	}
}



import java.util.*;

class QDemo {

	public static void main(String[] args) {
	
		Queue q = new LinkedList();

		q.offer(10);
		q.offer(20);
		q.offer(50);
		q.offer(30);
		q.offer(40);

		System.out.println(q);

		// poll()
		q.poll();
		System.out.println(q);   // this poll method delete first element
		
		q.remove();    // this method throws NoSuchElementException when queue is empty
		
		System.out.println(q);   // this remove method delete first element
		
		System.out.println(q.peek());       // this method return first element
		System.out.println(q.element());    // this method throws NoSuchElementException when queue is empty

		System.out.println(q);
	}
}

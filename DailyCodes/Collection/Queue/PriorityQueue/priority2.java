

import java.util.*;

class PQueueDemo {

	public static void main(String[] aegs) {
	
		PriorityQueue pq = new PriorityQueue();

		pq.offer("Vaibhav");
		pq.offer("Sagar");
		pq.offer("Pranav");
		pq.offer("Abhii");
		pq.offer("Sanket");

		System.out.println(pq); // [Abhii, Pranav, Sagar, Vaibhav, Sanket]
	}
}

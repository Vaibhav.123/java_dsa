

import java.util.*;

class DequeDemo {

	public static void main(String[] args) {
	
		Deque d = new ArrayDeque();

		d.offer(10);
		d.offer(40);
		d.offer(20);
		d.offer(30);

		System.out.println(d);     // [10, 40, 20, 30]
		
		// offerFirst() and offerLast()

		d.offerFirst(5);
		d.offerLast(50);

		System.out.println(d);   // [5, 10, 40, 20, 30, 50]


		// pollFirst() and pollLast()

		d.pollFirst();
		d.pollLast();

		System.out.println(d); // [10, 40, 20, 30]
		
		// peekFirst()  and peekLast()

		System.out.println(d.peekFirst());    // 10
		System.out.println(d.peekLast());     // 30

		// iterator()
		System.out.println("Iterator");
		Iterator itr = d.iterator();
		while(itr.hasNext()){
		
			System.out.println(itr.next());	
		}
/*  
Iterator
10
40
20
30
*/

		// descendingIterator()
		System.out.println("Descending Iterator");
		Iterator itr1 = d.iterator();
		while(itr1.hasNext()){
		
			System.out.println(itr1.next());	
		}
 /* 
Descending Iterator
10
40
20
30
*/
	}
}

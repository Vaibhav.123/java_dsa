

import java.util.concurrent.*;
import java.util.*;
class Employee {

	String str;
	Employee(String str){
	
		this.str = str;
	}

	public String toString(){
	
		return str;
	}
}

class SortByName implements Comparator {

	public int compare(Object obj1, Object obj2){
	
		return ((Employee)obj1).str .compareTo(((Employee)obj2).str);
	}
}
class UserDefinedDemo {

	public static void main(String[] args) throws InterruptedException {
	
		BlockingQueue bq = new PriorityBlockingQueue(2,new SortByName());

		bq.put(new Employee("Vaibhav"));
		bq.put(new Employee("Sagar"));
		bq.put(new Employee("Pranav"));

		System.out.println(bq); 
	}
}

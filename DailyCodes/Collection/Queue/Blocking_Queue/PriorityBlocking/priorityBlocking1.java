

import java.util.concurrent.*;

class Demo {

	public static void main(String[] args)throws InterruptedException {
	
		BlockingQueue pq = new PriorityBlockingQueue(3);

		pq.put("Kanha");
		pq.put("Ashish");
		pq.put("Rahul");

		System.out.println(pq);   // [Ashish, Kanha, Rahul]
	
		pq.put("Badhe");
		pq.put("Shashi");
		pq.put("P");
		pq.put("P");
		pq.put("P");
		pq.put("P");
		pq.put("P");
		pq.put("P");
		pq.put("P");
		pq.put("P");

		pq.put("c");
		pq.put("e");
		
		System.out.println(pq);   // [Ashish, Kanha, Rahul]
	}
}

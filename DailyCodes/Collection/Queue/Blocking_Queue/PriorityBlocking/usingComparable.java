

import java.util.concurrent.*;

class MyClass implements Comparable {

	String name;
	MyClass(String name){
	
		this.name = name;
	}
	
	public String toString(){
	
		return name;
	}
	public int compareTo(Object obj){
	
		return name.compareTo(((MyClass)obj).name);
	}
}
class UserDefinedDemo {

	public static void main(String[] args) throws InterruptedException{
	
		BlockingQueue pq = new PriorityBlockingQueue(3);

		pq.put(new MyClass("A"));
		pq.put(new MyClass("S"));
		pq.put(new MyClass("P"));
		pq.put(new MyClass("D"));
		pq.put(new MyClass("C"));

		System.out.println(pq);
	}
}

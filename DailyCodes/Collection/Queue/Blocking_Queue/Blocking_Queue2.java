// methods od BlockingQueue
// put(E); =>>> this methohds adds the data in the Queue
// offer(ihnt,long,TimeUnit)   =>>> this method for a while of time is stay and then if the the Queue is not gets empty then this will abort the insertion in queue

// take()   =>>>  this methods deletes the E in the from front
// drainTo(collection)  =>>> using this we can place the queue in the other data structure

import java.util.concurrent.*;
import java.util.*;
class BlockingQueueDemo {

	public static void main(String[] args)throws InterruptedException {
		
		BlockingQueue bQueue = new ArrayBlockingQueue(3);

		bQueue.put("Kanha");
		bQueue.put("Ashish");
		bQueue.put("Rahul");

		System.out.println(bQueue);  // [Kanha, Ashish, Rahul]

		bQueue.offer("Badhe",1,TimeUnit.SECONDS);

		System.out.println(bQueue);  // [Kanha, Ashish, Rahul]
		
		bQueue.take();
		
		System.out.println(bQueue);  // [Ashish, Rahul]
	
		ArrayList al = new ArrayList();
		
		System.out.println(al);  // []
		
		bQueue.drainTo(al);  
		
		System.out.println(al);  // [Ashish, Rahul]
		
		System.out.println(bQueue);  // []
	}
}

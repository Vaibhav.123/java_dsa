
// here 
import java.util.concurrent.*;

class BlockingQueueDemo {

	public static void main(String[] args) throws InterruptedException {
	
		BlockingQueue bQueue = new ArrayBlockingQueue(3);

		bQueue.put(10);
		bQueue.put(20);
		bQueue.put(30);

		System.out.println(bQueue); // [10, 20, 30]
	
		bQueue.put(40);   // blocking state 
		
		System.out.println(bQueue); // no output only cursor is blinking
	}
}

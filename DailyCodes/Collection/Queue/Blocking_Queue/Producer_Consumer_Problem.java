
import java.util.*;
import java.util.concurrent.*;

class Consumer implements Runnable {

	BlockingQueue bQueue = null;

	Consumer(BlockingQueue bQueue) {
	
		this.bQueue = bQueue;
	}

	public void run(){
	
		for(int i = 1; i<=10; i++){
		
			try{
			
				bQueue.take();
				System.out.println("Consume = "+i);

				Thread.sleep(1000);
			}catch(InterruptedException ie){}
		}
	}
}

class Producer implements Runnable {

	BlockingQueue bQueue = null;

	Producer(BlockingQueue bQueue){
	
		this.bQueue = bQueue;
	}

	public void run(){
	
		for(int i = 1; i<=10; i++){
		
			try{
			
				bQueue.put(i);
				System.out.println("Produce = "+i);

				Thread.sleep(7000);
			}catch(InterruptedException ie){}
		}
	}
}

class Consumer_Producer {

	public static void main(String[] args) {
	
		BlockingQueue bQueue = new ArrayBlockingQueue(3);
		
		Producer produce = new Producer(bQueue);

		Consumer consume = new Consumer(bQueue);

		Thread pThread = new Thread(produce);

		Thread cThread = new Thread(consume);

		pThread.start(); 
		cThread.start();
	}
}

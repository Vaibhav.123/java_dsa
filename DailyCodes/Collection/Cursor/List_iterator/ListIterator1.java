

import java.util.*;

class ListIteratorDemo {

	public static void main(String[] args) {
	
		ArrayList al = new ArrayList();

		al.add(10);
		al.add("Vaibhav");
		al.add(20.5);
		al.add(30.5f);

		ListIterator cursor = al.listIterator();

		while(cursor.hasNext()){
		
			System.out.println(cursor.next());
		}

		while(cursor.hasPrevious()){
		
			System.out.println(cursor.previous());
		}
	}
}

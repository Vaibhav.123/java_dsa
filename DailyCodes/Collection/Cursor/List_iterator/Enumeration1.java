
// enumeration is only applicable for vector and stack
import java.util.*;

class CursorDemo {

	public static void main(String[] argts)  {
	
		Vector v = new Vector();
		
		Stack s = new Stack();

		s.add("Vaibhav");
		s.add("Sagar");
		s.add("Pranav");
		s.add("Abhi");

		Enumeration itr = s.elements();
		
		System.out.println(itr.getClass().getName());

		while(itr.hasMoreElements()){
		
			System.out.println(itr.nextElement());
		}

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		Enumeration cursor = v.elements();
		
		System.out.println(cursor.getClass().getName());

		while(cursor.hasMoreElements()){
		
			System.out.println(cursor.nextElement());
		}
	}
}


import java.util.*;

class CursorDemo {

	public static void main(String[] arg) {
	
		ArrayList al = new ArrayList();

		al.add("Vaibhav");
		al.add(10);
		al.add(20.5);
		al.add(30.5f);

		for(var ch : al){
		
			System.out.println(ch);
		}
		// for knowing the which type of data will be there 
		//
		// var will at the runtime it will get converted into any type of data will be there
		for(var ch : al){
		
			System.out.println(ch.getClass().getName());   // this will print the what type of data is there
		}
	}
}

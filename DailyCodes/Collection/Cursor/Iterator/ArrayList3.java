

import java.util.*;

class CursorDemo {

	public static void main(String[] ars)  {
	
		ArrayList al = new ArrayList();

		al.add("Vaibhav");
		al.add("Pranav");
		al.add("Sagar");
		al.add("Abhi");

		System.out.println(al);

		Iterator cursor = al.iterator();

		while(cursor.hasNext()){
			
			if("Sagar".equals(cursor.next()))
				cursor.remove();
		}

		System.out.println(al);
	}
}

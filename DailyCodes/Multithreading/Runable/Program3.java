

// we can extends any class in our thread class when we implements Runnable interface


abstract class Demo {

	abstract void fun();
}

class MyThread extends Demo implements Runnable {

	public void run(){
	
		System.out.println("In run");
		fun();
	}

	void fun(){
	
		System.out.println("In fun");
	}
}

class ThreadDemo {

	public static void main(String[] args){
	
		MyThread obj = new MyThread();
		Thread t = new Thread(obj);

		t.start();
	}
}

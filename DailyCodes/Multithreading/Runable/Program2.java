

class Demo extends Thread {

	public void run(){
	
		System.out.println("In Demo run");
	}
}

class MyThread extends Thread implements Runnable {

	public void run(){
	
		System.out.println("In MyThread run");

		Demo obj = new Demo();
		obj.start();
	}
}

class ThreadDemo {

	public static void main(String[] args) {
	
		MyThread obj = new MyThread();
		Thread t = new Thread(obj);

		t.start();
	}
}


// getPriority()
// setPriority()


class MyThread extends Thread {

	public void run() {
	
		System.out.println("In run");
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());  // 5
	}
}

class ThreadDemo {

	public static void main(String[] args){
		
		Thread t = Thread.currentThread();

		System.out.println(t);   // Thread[main,5,main]
		
		System.out.println(t.getPriority());   // 5  (default priority)

		MyThread obj1 = new MyThread();
		obj1.start();
		
		t.setPriority(7);   // if the value is is 0 < value < 11 will must 
				    // if this condition not satisfies then 
				    // IllegalArgumentException will throw by this method

		System.out.println(t.getPriority());    // 7
	}

}

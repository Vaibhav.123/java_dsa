

class MyThread extends Thread {

	public void run(){
	
		System.out.println("In run");
	}
}

class ThreadDemo {

	public static void main(String[] args){
		
		Thread t = Thread.currentThread();
		System.out.println(t.getName());

		MyThread obj = new MyThread();
		obj.start();

		obj.start();
		/*
		 * Exception in thread "main" java.lang.IllegalThreadStateException
        		at java.base/java.lang.Thread.start(Thread.java:793)
        		at ThreadDemo.main(Program5.java:21)
			*/
	}
}


// here we can create thread using runnable interface 
//  runnable interface has only abstract method that is run 
//  and we have compulsory override that method
//  using runnable we can extends any class in our class but using Thread class we cannot extends any our class 
//  only Thread class will be extends because of java does not support multiple inheritance
//  this is main diff between them
//
class MyThread implements Runnable {

	public void run(){
	
		System.out.println("In run");
	}
}

class ThreadDemo {

	public static void main(String[] args)  {
	
		MyThread obj = new MyThread();
		Thread t = new Thread(obj);

		t.start();
	}
}

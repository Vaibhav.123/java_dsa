// ThreadGroup using Thread class
// methods of ThreadGroup : activeCount()
//           		    activegroupCount()
//  and InterruptedException occurs here
//

class MyThread extends Thread {
	
	MyThread(ThreadGroup tg, String str){
	
		super(tg,str);
	}

	public void run(){
	
		System.out.println(Thread.currentThread());
		try {
		
			Thread.sleep(5000);
		}catch(InterruptedException ie){
		
			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo {

	public static void main(String[] args) throws InterruptedException {
	
		ThreadGroup p1 = new ThreadGroup("India");
		MyThread obj1 = new MyThread(p1,"Maharashtra");
		MyThread obj2 = new MyThread(p1,"Goa");
		obj1.start();
		obj2.start();

		ThreadGroup c1 = new ThreadGroup(p1,"Pakistan");
		MyThread obj3 = new MyThread(c1,"Lahore");
		MyThread obj4 = new MyThread(c1,"Karachi");
		obj3.start();
		obj4.start();
		
		ThreadGroup c2 = new ThreadGroup(p1,"Bangladesh");
		MyThread obj5 = new MyThread(c2,"Dhaka");
		MyThread obj6 = new MyThread(c2,"Mirpur");
		obj5.start();
		obj6.start();

		c1.interrupt();
		
		Thread.sleep(1000);
		System.out.println("Active Count Of ALL Threads : "+ p1.activeCount());
		System.out.println("Active Count Of ThreadGroups Count : "+ p1.activeGroupCount());
	}
}

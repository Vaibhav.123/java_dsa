
// ThreadGroup using Runnable interface 
// 
class MyThread implements Runnable {
	
	public void run(){
	
		System.out.println(Thread.currentThread());

		try {
		
			Thread.sleep(5000);
		}catch(InterruptedException ie){
	
		}
	}
}

class ThreadGroupDemo {

	public static void main(String[] args) {
	
		ThreadGroup p1 = new ThreadGroup("India");
		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread t1 = new Thread(p1,obj1,"Maharahshtra");
		Thread t2 = new Thread(p1,obj2,"Goa");
		t1.start();
		t2.start();

		ThreadGroup c1 = new ThreadGroup(p1,"Pakistan");
		MyThread obj3 = new MyThread();
		MyThread obj4 = new MyThread();

		Thread t3 = new Thread(c1,obj3,"Lahore");
		Thread t4 = new Thread(c1,obj4,"Karachi");
		t3.start();
		t4.start();
		
		ThreadGroup c2 = new ThreadGroup(p1,"Bangladesh");
		MyThread obj5 = new MyThread();
		MyThread obj6 = new MyThread();
		
		Thread t5 = new Thread(c2,obj5,"Dhaka");
		Thread t6 = new Thread(c2,obj6,"Mirpur");
		t5.start();
		t6.start();
	}
}



class MyThread extends Thread {
	
	MyThread(ThreadGroup tg, String str){
	
		super(tg,str);
	}
	public void run(){
	
		System.out.println(Thread.currentThread());
	}
}

class Client {

	public static void main(String[] args)  {
	
		ThreadGroup pthreadGP = new ThreadGroup("Mall");
		
		MyThread obj1 = new MyThread(pthreadGP, "Theatre");
		MyThread obj2 = new MyThread(pthreadGP, "FoodCourt");
		MyThread obj3 = new MyThread(pthreadGP, "Shooping");

		obj1.start();
		obj2.start();
		obj3.start();
		
		ThreadGroup cthreadGP = new ThreadGroup("FoodCourt");
		
		MyThread obj4 = new MyThread(pthreadGP, "Sandwich");
		MyThread obj5 = new MyThread(pthreadGP, "Burger");
		MyThread obj6 = new MyThread(pthreadGP, "Pizza");

		obj4.start();
		obj5.start();
		obj6.start();

		System.out.println("Thread Count = "+ pthreadGroup.activeCount());
	}
}


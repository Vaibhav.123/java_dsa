//
// we can set Thread name by using two ways: 
// 1. by using setName()
// 2. by directly passing name at the time of creating object 
//

class MyThread extends Thread  {
	
	MyThread(){
	
	}	
	MyThread(String str){
		
		super(str);
	}
	public void run(){
	
		System.out.println(getName());    // Core2Web
		System.out.println(Thread.currentThread().getThreadGroup());  // java.lang.ThreadGroup[name=main,maxprio=10]
	}
}

class ThreadDemo {

	public static void main(String[] args) throws InterruptedException{
	
		MyThread obj = new MyThread("Core2Web");
		obj.start();
		
		obj.join(2000);
		MyThread obj1 = new MyThread();
		obj1.start();
		obj1.setName("Vaibhav");

		System.out.println(obj1.getName());
	}
}



//  by using this Executors.newFixedThreadPool(int) method 
//  we can whatever the parameter will be given then the threads will be created in threadpool 
//  means we can create specific count of threads and only that threads will be sassigned to the task 
//  if there are more than threads count then that tasks will be schedule using the ExecutorService interface
//
import java.util.concurrent.*;
class MyThread implements Runnable {

	int num;
	MyThread(int num) {
	
		this.num=num;
	}

	public void run(){
	
		System.out.println(Thread.currentThread()+"Start Thread :"+num);
		
		dailyTask();
		
		System.out.println(Thread.currentThread()+"End Thread :"+num);
	}

	void dailyTask(){
	
		try{
		
			Thread.sleep(5000);
		}catch(InterruptedException ie){}
	}

}

class ThreadPoolDemo {

	public static void main(String[] args)  {
		// this method use when we give the fixed threads to create in thread pool
		//
		ExecutorService ser = Executors.newFixedThreadPool(2);

		for(int i=1; i<=4; i++){
		
			MyThread obj = new MyThread(i);
			ser.execute(obj);
		}

		ser.shutdown();
	}
}

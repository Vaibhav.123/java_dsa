
import java.util.concurrent.*;

class MyThread implements Runnable  {
	
	int num1;
	MyThread(int num1){
	
		this.num1 = num1;
	}
	public void run(){
	
		System.out.println("Start :"+num1+Thread.currentThread());
		dailyTasks();
		System.out.println("End :"+num1+Thread.currentThread());
	}

	void dailyTasks(){
	
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie) {}
	}
}

class ThreadPoolDemo {

	public static void main(String[] args)  {
	
		ThreadPoolExecutor tpe1 = (ThreadPoolExecutor)Executors.newFixedThreadPool(2);
		ThreadPoolExecutor tpe2 = (ThreadPoolExecutor)Executors.newFixedThreadPool(2);

		for(int i=1; i<=10; i++){
		
			MyThread obj = new MyThread(i);
			tpe1.execute(obj);
		}
		for(int i=1; i<=10; i++){
		
			MyThread obj = new MyThread(i);
			tpe2.execute(obj);
		}

		tpe1.shutdown();
		tpe2.shutdown();
		
		
	}
}

// Executors.newSingleThreadPool()
// this method is create threads one by one
//

import java.util.concurrent.*;
class Demo {

	static void dailyTasks(){
	
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){}
	}
}
class MyThread implements Runnable {
	int num;
	MyThread (int num) {
	
		this.num=num;
	}
	public void run(){
	
		System.out.println(Thread.currentThread()+" Start Thread :"+num);
		Demo.dailyTasks();
		System.out.println(Thread.currentThread()+" End Thread :"+num);
	}
}

class ThreadPoolDemo {

	public static void main(String[] args)  {
	
		ExecutorService ser = Executors.newSingleThreadExecutor();

		for(int i=1; i<=6; i++){
		
			MyThread obj = new MyThread(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}

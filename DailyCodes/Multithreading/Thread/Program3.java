
// now this is the actual code
class MyThread extends Thread {  // we cannot write here throws InterruptedException 
				 // it can be handle by only try catch block
	public void run(){

		for(int i=0; i<10; i++){

			System.out.println("In run");
			try{
				Thread.sleep(1000);

			}catch(InterruptedException obj){

				System.out.println("Caught");	
			}
		}
	}
}

class ThreadDemo {

	public static void main(String[] args) throws InterruptedException{
	
		MyThread obj = new MyThread();
		obj.start();

		for(int i=0; i<10; i++){
			System.out.println("In main");
			Thread.sleep(1000);
		}
	}
}

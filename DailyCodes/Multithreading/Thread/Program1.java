
// by this way we can create thread using Thread class
/*
 * RULES :
 * 1. we must make Thread class to parent of our class
 * 2. Thread class has its run we have to override it into our class 
 */
class MyThread extends Thread {

	public void run(){
	
		for(int i=0; i<10; i++){
		
			System.out.println("In run");
		}
	}
}

class ThreadDemo {

	public static void main(String[] args) {
		
		MyThread obj = new MyThread();   
		obj.start();

		for(int i=0; i<10; i++){
		
			System.out.println("In main");
		}
	}
}

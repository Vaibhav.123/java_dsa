

class MyThread extends Thread {

	public void run(){
	
		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());     // main
		// the output is main because the Thread is only created not run so
		
	}
	// we cant have to override this start method,
	// because start method is used for starting the thread function 
	// so suppose we can override then it will not gives error but that method cannot have power to run 
	// any thread so at any cost cant override that method
	public void start(){
	
		System.out.println("In MyThread Start");    // In MyThread start
		run();
	}
}

class ThreadDemo {

	public static void main(String[] args)  {
	
		MyThread obj = new MyThread();
		obj.start();

		System.out.println(Thread.currentThread().getName());    //main
	}
}

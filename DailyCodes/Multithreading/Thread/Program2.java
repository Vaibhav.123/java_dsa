

class MyThread extends Thread {  // In thread class there is run method it cannot throws the InterruptedException
				 // so if the InterruptedException is created in our class then 
				 // we cannot throw that exception
				 // only handle using try catch block

	public void run(){   
		for(int i=0; i<10; i++){
			System.out.println("In run");
			Thread.sleep(1000);
		}
	}
}
// if we cannot handle this InterruptedException occured by the sleep method then error:
// unreported exception InterruptedException must be caught or declared to be thrown
class ThreadDemo  {

	public static void main(String[] args)  {
	
		MyThread obj = new MyThread();
		obj.start();

		for(int i=0; i<10; i++){
		
			System.out.println("In main");
			Thread.sleep(1000);   // this method throws InterruptedException,
					      // so we have to handle it
		}
	}
}
// so in the above there is error

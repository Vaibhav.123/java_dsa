// for print the name of the thread

class MyThread extends Thread{

	public void run(){
	
		System.out.println("Child Thread = "+ Thread.currentThread().getName());
	}
}

class ThreadDemo {

	public static void main(String[] afgs)  {
	
		System.out.println("Main Thread = "+ Thread.currentThread().getName());
		MyThread obj = new MyThread();
		obj.start();
		
		MyThread obj1 = new MyThread();
		obj1.start();
	}
}


// we can create thread in run method also

class Demo extends Thread{

	public void run(){
	
		System.out.println("In Demo run");
		System.out.println("Demo = "+ Thread.currentThread().getName());   // Thread-1
	}
}

class MyThread extends Thread {

	public void run(){
	
		System.out.println("In MyThread run");
		System.out.println("MyThread = "+ Thread.currentThread().getName());    // Thread-0

		Demo obj = new Demo();
		obj.start();
	}
}

class ThreadDemo {

	public static void main(String[] args) {
		
		System.out.println("ThreadDemo = "+ Thread.currentThread().getName());   // main
		MyThread obj = new MyThread();
		obj.start();
	}
}

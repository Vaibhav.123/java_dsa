// sleep() method:
//

class MyThread extends Thread {

	public void run(){
	
		System.out.println(Thread.currentThread());    // Thread[Thread-0,5,main]
	}
}

class ThreadDemo {

	public static void main(String[] args) throws InterruptedException {
		
		System.out.println(Thread.currentThread());   // Thread[main,5,main]

		MyThread obj = new MyThread();
		obj.start();

		Thread.sleep(100);
		
		System.out.println(Thread.currentThread());    // Thread[main,5,main]
	}
}

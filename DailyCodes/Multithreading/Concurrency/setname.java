// setName()
// getName()

class MyThread extends Thread {

	public void run() {
	
		System.out.println(Thread.currentThread());
		
		System.out.println("Prev : "+Thread.currentThread().getName());
		Thread.currentThread().setName("Core2Web");
		System.out.println("Curr : "+Thread.currentThread().getName());
	}
}

class ThreadDemo {

	public static void main(String[] args)  {
	
		MyThread obj = new MyThread();

		obj.start();
	}
}

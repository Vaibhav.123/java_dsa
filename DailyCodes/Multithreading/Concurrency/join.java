

class MyThread extends Thread {

	public void run(){
		
		/*try {
			ThreadDemo.nmName.join();
		}catch(InterruptedException obj ) {
		
		}*/

		for(int i=0; i<10; i++){
			
			System.out.println("In Thread-0");
		}

	}
}

class ThreadDemo {
	
	static Thread nmName = null;

	public static void main(String[] args) throws InterruptedException {
		
		ThreadDemo.nmName = Thread.currentThread();
		MyThread obj = new MyThread();
		obj.start();
		
		obj.join();	  // join executes first Thread-0 then man Thread after completion
		for(int i=0; i<10; i++){
		
			System.out.println("In main");
		}
	}
}

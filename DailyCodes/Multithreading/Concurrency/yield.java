

class MyThread extends Thread  {

	public void run(){
	
		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadYieldDemo {

	public static void main(String[] args)  {
	
		MyThread obj1 = new MyThread();
		obj1.start();

		obj1.yield();    // Is Similar to parameterized join method

		System.out.println(Thread.currentThread().getName());
	}
}

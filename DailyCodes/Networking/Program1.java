// InetAddress class 
// this used for finding the IP addresss of site

import java.io.*;
import java.net.*;
class IPAddress {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String site = br.readLine();

		InetAddress ip = InetAddress.getByName(site);   // this is static method of that class 

		System.out.println(ip);
	}
}

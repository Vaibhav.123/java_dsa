
import java.io.*;
import java.net.*;

class Client {

	public static void main(String[] args) throws IOException{
	
		Socket s = new Socket("localhost",1300);

		InputStream is = s.getInputStream();

		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		String str;

		while((str = br.readLine()) != null){
		
			System.out.println(str);
		}

		OutputStream os = s.getOutputStream();
		PrintStream ps = new PrintStream(os);
		char choice = 'a';
		do{
		
			String str1 = br.readLine();
			ps.println(str1);

			System.out.println("Send msg yes or no: ");
			choice = (char)br.read();
		}while(choice == 'y');

		s.close();
		br.close();
	}
}

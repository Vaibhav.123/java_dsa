// URL connectio class 
// this is used for getting information about the website
// means

import java.io.*;
import java.net.*;
import java.util.*;

class URLConnectionDemo {

	public static void main(String[] args)throws IOException {
	
		URL obj = new URL("https://www.core2web.in");

		URLConnection conn = obj.openConnection();

		System.out.println("Last Modified : "+new Date(conn.getLastModified()));  // Last Modified : Mon Sep 11 18:52:24 IST 2023
	}
}

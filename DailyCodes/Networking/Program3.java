// URI class 
// this class is used for opening URL 
//
import java.net.*;
import java.io.*;
import java.awt.*;

class URIDemo {

	public static void main(String[] args)throws IOException, URISyntaxException {
	
		String url = "www.core2web.in";

		URI obj = new URI(url);

		Desktop desk = Desktop.getDesktop();
		desk.browse(obj);
	}
}

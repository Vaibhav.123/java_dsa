// URL class 
// this is used for getting the information of URL
// getProtocol:  gives the protocol of URl
// getPort: this gives the port no, if there in no port no then it gives -1 
// getFile: this will gives filename
//
import java.io.*;
import java.net.*;

class URLDemo {

	public static void main(String[] args) throws MalformedURLException{
	
		URL obj1 = new URL("https://www.core2web.in");
		URL obj2 = new URL("https://www.core2web.in:80/home.html");
		
		System.out.println(obj1.getProtocol());  // https
		System.out.println(obj1.getPort());      // -1
		System.out.println(obj1.getFile());      // blank

		System.out.println(obj2.getProtocol());  // https
		System.out.println(obj2.getPort());	 // 80
		System.out.println(obj2.getFile());	 // /home.html
	}
}

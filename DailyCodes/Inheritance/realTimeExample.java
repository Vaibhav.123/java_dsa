class Defence {

	Defence (){
	
		System.out.println("In Defence Constructor");
		Weapons();
	}
	void Weapons(){
	
		System.out.println("Weapons : AK47,M416,AWM");
		System.out.println("\n******************************************\n");
	}
}

class Army extends Defence {

	Army (){
	
		System.out.println("In Army");
		MilitaryVehicles();
	}
	void MilitaryVehicles (){
	
		System.out.println("Army Vehicles = Willys MB, Humvee, M88 Recovery Vehicle");
		System.out.println("\n******************************************\n");
	}
}

class Navy extends Army {

	Navy(){
	
		System.out.println("In Navy");
		SubMarines();
	}
	void SubMarines(){
	
		System.out.println("Navy SubMarines = INS Vikrant, INS Kamorta, INS Shivalik");
		System.out.println("\n******************************************\n");
	}

}

class AirForce extends Navy {

	AirForce(){
	
		System.out.println("In AirForce");
		Aircraft();
	}

	void Aircraft (){
	
		System.out.println("AirForce Aircrafts = Aviation Corps, Naval AirCraft, Indian Coastguard");
	}
}


class Client {

	public static void main(String[] args) {
		
		AirForce obj1 = new AirForce();


	}
}

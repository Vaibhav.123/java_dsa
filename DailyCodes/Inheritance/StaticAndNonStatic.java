

class Parent {

	int x = 10;

	static int y = 20;

	static {
	
		System.out.println("In Parent Static Block");
	}
	Parent (){
	
		System.out.println("In Parent Constructor");
	}

	static void Method1() {
	
		System.out.println(y);
	}
	void Method2() {
	
		System.out.println(x);
		System.out.println(y);
	}
}

class Child extends Parent {

	static {
	
		System.out.println("In Child Static Block");
		System.out.println(y);
	}
	Child (){
	
		System.out.println("In Child Constructor");
	}
}

class Client {

	public static void main(String[] args) {
	
		System.out.println("In Main");
		Child obj = new Child();
		obj.Method1();
		obj.Method2();
	}
}

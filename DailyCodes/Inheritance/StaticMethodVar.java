

class Parent {

	static int x = 10;
	Parent() {
	
		System.out.println("In Parent Constructor");
	}

	static void access(){
	
		System.out.println(x);
	}
}

class Child extends Parent {

	Child(){
	
		System.out.println("In Child Constructor");
	}

}

class Client {

	public static void main(String[] args) {
	
		Child obj = new Child();
		obj.access();
	}
}



class Parent {

	Parent (){
	
		System.out.println("In Parent Constructor");
	}
}

class Child extends Parent {
	
	Child (){
		super();
		System.out.println("In Child Constructor");
	}
}

class Client {

	public static void main(String[] ags) {
	
		Child obj = new Child();
	}
}

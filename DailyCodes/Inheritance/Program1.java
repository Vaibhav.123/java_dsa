

class ICC extends Object{

	ICC(){
	
		System.out.println("In ICC Constructor");
	}
}
class BCCI extends ICC{

	BCCI(){
	
		System.out.println("In BCCI Constructor");
	}
}
class Client extends BCCI {

	public static void main(String[] args) {
	
		BCCI obj = new BCCI();
		ICC obj1 = new ICC();
	}
}



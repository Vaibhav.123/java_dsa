//  using super we can access parents static non static variables and methods 
//  in child class 

class Parent {

	static int x = 10;
	int y = 20;

	Parent(){
	
		System.out.println("In Parent Constructor");
	}
}

class Child extends Parent {
	
	static int x = 100;
	int y = 200;

	Child(){
		
		System.out.println("In Child Constructor");
	}
	
	void access(){
	
		System.out.println(x);    // 100
		System.out.println(y);    // 200

		System.out.println(super.x);   // 10
		System.out.println(super.y);   // 20
	
	}
}

class Client {

	public static void main(String[] args) {
	
		Child obj = new Child();
		obj.access();
	}
}

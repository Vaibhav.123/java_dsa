
//  here in this below code 
//  child class Access all properties od parent class 
//  including methods or others special method also i.e.constructor
//  int the code parentproperty is accessible for child class 
//  here in this we can create only one object and access both the classes

class Parent {

	Parent(){
	
		System.out.println("In Parent Constructor");
	}

	void ParentProperty(){
	
		System.out.println("Car,Gold,Flat");
	}
}
class Child extends Parent {

	Child(){
	
		System.out.println("In Child Constructor");
	}
}

class Client {

	public static void main(String[] args ){
	
		Child obj1 = new Child();
		obj1.ParentProperty();
	}
	
	
}

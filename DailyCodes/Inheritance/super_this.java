

class Parent {
	
	static int x = 10;
	int y = 20;

}

class Child extends Parent {

	static int x = 100;
	int y = 200;

	void access(){
	
		//System.out.println(super);
		System.out.println(this);
	}
}

class Client {

	public static void main(String[] args) {
	
		Child obj = new Child();
		obj.access();
	}
}

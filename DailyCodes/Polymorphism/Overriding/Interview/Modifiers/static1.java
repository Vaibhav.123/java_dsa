// error : overriden method is static 
// this is not allowed here static will not take part in the overriding
// because static menas it is the special case of that class 
// and static method od class will be special method so it cannot be overriden
// static method will be class method
// one of method will be static then it will be then it will be bind only for that class 
// and the overriding will be in the two classes so static cannot take part in static case

class Parent {

	static void fun(){
	
		System.out.println("Parent fun");
	}
}

class Child extends Parent {

	void fun(){

		System.out.println("Child fun");
	}
}

class Client {

	public static void main(String[] aths) {
	
		Parent obj = new Child();
		obj.fun();

	}
}

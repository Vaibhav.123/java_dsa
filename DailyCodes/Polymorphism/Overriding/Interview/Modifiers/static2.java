// error : overriding method is static
// here will also not allowed static in child class 
// because fun() in parent class wil be default and we make it in child class as static 
// so it will bind with that class 
// so overriding not take part in this scenario also

class Parent {

	void fun(){
	
		System.out.println("Parent fun");
	}
}

class Child extends Parent {

	static void fun(){
	
		System.out.println("Child fun");
	}
}

class Client {

	public static void main(String[] args) {
	
		Parent obj = new Child();
		obj.fun();
	}
}

// here in below code fun() in Parent it will be its own method 
// and child class has is own method 
// so it will cal its own method 
// this concept is called as Method Hiding 
// here overriding is not possible

class Parent {

	static void fun(){
	
		System.out.println("Parent static fun");
	}
}

class Child extends Parent {

	static void fun(){
	
		System.out.println("Child static fun");
	}
}

class Client {

	public static void main(String[] args) {
	
		Parent obj1 = new Parent();
		obj1.fun();       // Parent static fun

		Child obj2 = new Child();
		obj2.fun();       // Child static fun

		Parent obj3 = new Child();
		obj3.fun();       // Parent child fun
	}
}

// here in below code 
//  if we make in child class method fun() final then 
//  for child of child class has not access that method 
//  so here is no error in this code
// final is modifier
// which is used for 
// if parent does not want to change or override its own method at that time, the method has been make final
// final means it cannot be changable 

class Parent {

	void fun(){
	
		System.out.println("Parent fun");
	}
}

class Child extends Parent {

	final void fun(){
	
		System.out.println("Child fun");
	}
}

class Client {

	public static void main(String[] args) {
	
		Parent obj = new Child();
		obj.fun();
	}
}

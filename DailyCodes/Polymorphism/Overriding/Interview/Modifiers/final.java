// here in below code 
// errror :  fun() mehod cannot be overriden in child 
// because overriden method was final
//
// final is keyword
// which is used for 
// if parent does not want to change or override its own method at that time, the method has been make final
// final means it cannot be changable 

class Parent {

	final void fun(){
	
		System.out.println("Parent fun");
	}
}

class Child extends Parent {

	void fun(){
	
		System.out.println("Child fun");
	}
}

class Client {

	public static void main(String[] args) {
	
		Parent obj = new Child();
		obj.fun();
	}
}

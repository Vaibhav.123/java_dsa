// here in below code :
// access specifier will be matter in overriding
// here in parent class we decalre fun as public,
// and in child class we inherits the the Parent fun() method,
// but in parent class method is public so we cant override this method in child class,
// because public scope is anywhere, and we decrease that scope so thats why this is not allowed here 
// so error will be occured: 
// attempting to assign weaker access privilleges, was public 

class Parent {

	public void fun(){
	
		System.out.println("Parent fun");
	}
}
class Child extends Parent {

	void fun(){
	
		System.out.println("Child fun");
	}
}


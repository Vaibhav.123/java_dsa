// here in below code :
// access specifier will be matter in overriding
// in parent fun has access specifier by default is default
// and in child class fun() has access specifier will public 
// but here in this scenario scope will increases so there no error
// java strcitly checks data loss but here no data loss rather than data will upgrade

class Parent {

	void fun(){
	
		System.out.println("Parent fun");
	}
}
class Child extends Parent {

	public void fun(){
	
		System.out.println("Child fun");
	}
}


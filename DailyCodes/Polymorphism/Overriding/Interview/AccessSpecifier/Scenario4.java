// here in the below code 
// error : attempting to assign weaker privilleges, was package 
// because we the change tha scope default from private so error of package will be occured
//  Scope of the parent class methods will be decrese in child class that not allowed here 

class Parent {

	void fun(){
	
		System.out.println("Parent fun");
	}
}
class Child extends Parent {

	private void fun(){
	
		System.out.println("Child fun");
	}
}

class Client {

	public static void main(String[] arhs) {
	
		Parent obj = new Child();

		obj.fun();   // error : attempting to assign weaker privilleges, was package
	}
}

// here in 0the below code 
//  Parent class has its fun() method will be private means it will be accessed only for that class 
//  not outside of that class 
//  for that scope within that class Parent 
//  so in this scenario overriding will be not possible 
//  overriding will not be in the picture
//  so error : private access

class Parent {

	private void fun(){
	
		System.out.println("Parent fun");
	}
}
class Child extends Parent {

	void fun(){
	
		System.out.println("Child fun");
	}
}

class Client {

	public static void main(String[] arhs) {
	
		Parent obj = new Child();

		obj.fun();   // error : fun() has private access in Parent
	}
}

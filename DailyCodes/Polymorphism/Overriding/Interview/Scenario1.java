
// In overriding the maethod signatures are same as well as
// return type is also same 
// but only primitive data types return type will be same
// 
// if all the scenarios are not satified then error :

// error : fun() method int child cannot override in parent
// return type int cant compatible with char

class Parent {

	char fun() {
	
		System.out.println("Parent fun");
		return 'A';
	}
}

class Child extends Parent {

	int fun(){
	
		System.out.println("Child Fun");
		return 10;
	}
}

class Client {

	public static void main(String[] ags) {
	
		Parent obj = new Child();
		obj.fun();
	}
}

class Match {
	void matchType(){
		System.out.println("T20/OneDay/Test");

	}
}

class IPLMatch extends Match {

	void matchType(){
	
		System.out.println("T20");
	}
}

class OneDayMatch extends Match {

	void matchType(){
	
		System.out.println("OneDay");
	}
}

class TestMatch extends Match{

	void matchType(){
	
		System.out.println("Test Match");
	}
}

class Client {

	public static void main(String[] args) {

		Match type1 = new IPLMatch();
		type1.matchType();
		Match type2 = new OneDayMatch();
		type2.matchType();
		Match type3 = new TestMatch();
		type3.matchType();
	}
}

//  Co-varient return type means [ parent child relation ]
//  same here co-variant return type is apply only for classes
//  same scenario as like in overriding but only this is different so return type will be diff. in this scenario
//  so there will no error for diff. returntype of method
//

class Parent {

	Object fun(){
	
		System.out.println("In Parent Fun");
		return new Object();
	}
}

class Child extends Parent {

	String fun(){
	
		System.out.println("In Child fun");
		return "Vaibhav";
	}
}

class Client {
	
	public static void main(String[] args){
		Parent obj = new Child();
		obj.fun();
	}
}
// OP:
// In Child fun

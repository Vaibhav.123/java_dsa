// Scenario 3

class Parent {
	
	void fun(){
	
		System.out.println("Parent fun");
	}

}

class Child extends Parent {

	void fun(int x) {
	
		System.out.println("Child fun");
	}
}

class Client {

	public static void main(String[] as){
	
		Parent obj = new Child();

		obj.fun(10);
	}
}
// OP:
// error : required : no arguments
//         found : int

// here below code 
// both the parent and child classes have same class marry
// and child class override the marry method of parent class
// using method signature of both parent and child classes
//

class Parent {

	Parent(){
	
		System.out.println("Parent Constructor");
	}

	void Property(){
	
		System.out.println("Car,Flat,Gold");
	}

	void marry(){
	
		System.out.println("Deepika Padukone");
	}
}

class Child extends Parent {

	Child(){
	
		System.out.println("Child Constructor");
	}

	void marry(){
	
		System.out.println("Alia Bhatt");
	}
}

class Client{

	public static void main(String[] args) {
	
		Child obj = new Child();

		obj.Property();
		obj.marry();
	}
} 

// OP:
// Parent Constructor
// Child Constructor
// Car,Flat,Gold
// Alia Bhatt

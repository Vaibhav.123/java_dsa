// in the below code error is there because
// method table Parent(obj2)             method table Child (obj1)
//    
//    Parent()                                Child()
//    fun()                                   gun()
//                                            fun();
//  here in thye method table of parent class 
//  there is no gun method so there is error as cannot find symbol
//
//
class Parent {

	Parent(){
	
		System.out.println("Parent Constructor");
	}
	void fun(){
	
		System.out.println("In Fun");
	}
}

class Child extends Parent{

	Child(){
	
		System.out.println("Child Constructor");
	}

	void gun(){
	
		System.out.println("In gun");
	}
}

class Client{

	public static void main(String[] ahs) {
	
		Child obj1 = new Child();
		obj1.fun();
		obj1.gun();

		Parent obj2 = new Parent();
		obj2.fun();
		obj2.gun();   // error: cannot find symbol
	}
}

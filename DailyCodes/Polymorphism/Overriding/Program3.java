//  here we can create as object
//
//  Parent obj = new Child();
//  compiler checks only left side of the equalto sign
//  and jvm creates the object of right side of equalto
//
//  parent cha refernce asel tr child cha object chalto.
//  BUT
//  Child obj = new Parent();  // error : Parent cannot be converted into child
//  child cha reference asel tr Parent cha object chalat nahi
    

class Parent {

	Parent(){
	
		System.out.println("Parent Constructor");
	}

	void fun(){
	
		System.out.println("In Fun");
	}
}

class Child extends Parent {

	Child(){
	
		System.out.println("Child Constructor");
	}

	void fun(){
	
		System.out.println("In gun");
	}
}

class Client {

	public static void main(String[] ahgs) {
	
		Parent obj = new Child();
		obj.fun();
	}
}

//  OP:
//  Parent Constructor
//  Child Constructor
//  In gun

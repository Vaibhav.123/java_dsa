// Scenario 5

class Parent {
	
	void fun(int x){
	
		System.out.println("Parent fun");
	}

}

class Child extends Parent {

	void fun(int x) {
	
		System.out.println("Child fun");
	}
}

class Client {

	public static void main(String[] as){
	
		Parent obj = new Child();

		obj.fun(10);
	}
}
// OP:
// Child fun
// firstly gives priority to the child class

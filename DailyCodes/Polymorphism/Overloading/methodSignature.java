

class Demo {
	
	void fun(int x){   // fun(int)
	
		System.out.println(x);
	}
	void fun(int x){    // fun(int)
	
		System.out.println(x);
	}
}
// error :  method fun(int) is already defined in the class Demo
// because here method signature of both methods is same.

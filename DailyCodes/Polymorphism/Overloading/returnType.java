

class Demo {

	int fun(int x) {   // fun(int)
	
		System.out.println(x);
	}
	float fun(int x) {    // fun(int)
	
		System.out.println(x);
	}
}
// error : method fun(int) is alewady defined in class Demo
// for method signature return type is not required 
// only method name and parameter is required

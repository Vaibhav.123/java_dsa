
class Demo {

	void fun(Object obj) {
	
		System.out.println("Object");
	}
	void fun(String str) {
	
		System.out.println("String");
	}
}
class Client {

	public static void main(String[] ah) {
	
		Demo obj = new Demo();
		
		obj.fun("Core2Web");   // exact match String class
		obj.fun(new StringBuffer("Core2Web"));   // here is call from new so Object class match
		obj.fun(null);     // String class match
	}
}

  //OP: 
//   String
//   Object
//   String

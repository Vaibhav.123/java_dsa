
// Object class is prent of all the classes
// here all the classes are the child class of parent class so there is no error 

class Demo {

	void fun(Object obj) {    // Object obj = "Vaibhav";  here ObjectClass obj = StringClass
	
		System.out.println("Object");
	}
}
class Client{	
	
	public static void main(String[] ahs) {
	
		Demo obj = new Demo();
		obj.fun("Vaibhav");
	}
}

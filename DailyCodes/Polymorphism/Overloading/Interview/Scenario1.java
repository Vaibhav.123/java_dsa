
//  ambiguity between paramters of method
// error : ambiguos function
// here compiler checks the method signature so here method signature is different but
// the compiler also checks the parameters
// in the below code arguments are passes that are matching in both methods 
// so ambigi=uity will be occured

class Demo {

	void fun(int x, float y){
	
		System.out.println("int-float");
	}

	void fun(float x, int y){
	
		System.out.println("float-int");
	}

	public static void main(String[] args) {
	
		Demo obj = new Demo();
		obj.fun(10,10);
	}
}



class Demo {

	void fun(int x){
	
		System.out.println(x);    // 10
	}
	void fun(float x){
	
		System.out.println(x);    // 10.5
	}
	void fun(Demo obj){
	
		System.out.println(obj);   //  Demo@123456
	}

	public static void main(String[] args) {
		
		Demo obj = new Demo();
		obj.fun(10);
		obj.fun(10.5f);
		obj.fun(obj);

	}
}



import java.util.*;

class HashTableDemo {

	public static void main(String[] args) {
	
		Hashtable ht = new Hashtable();

		ht.put(10,"Sachin");
		ht.put(7,"MSD");
		ht.put(18,"Virat");
		ht.put(1,"KLRahul");
		ht.put(45,"Rohit");

		System.out.println(ht); // {10=Sachin, 18=Virat, 7=MSD, 45=Rohit, 1=KLRahul}
	
		Enumeration itr1 = ht.keys();

		// only keys will be print
		while(itr1.hasMoreElements()){
			
			System.out.println(itr1.nextElement());
		}
		// only values[elements] will be print
		Enumeration itr2 = ht.elements();
		while(itr2.hasMoreElements()){
		
			System.out.println(itr2.nextElement());
		}

		System.out.println(ht.get(18));    // Virat

		ht.remove(1);

		System.out.println(ht);  // {10=Sachin, 18=Virat, 7=MSD, 45=Rohit}

	}
}

//  navigableMap methods
//
import java.util.*;
class NavigableDemo {

	public static void main(String[] args) {
	
		NavigableMap nm = new TreeMap();

		nm.put(1,"India");
		nm.put(4,"Srilanka");
		nm.put(6,"Pakistan");
		nm.put(2,"Australia");
		

		System.out.println(nm); //{Aus=Australia, Ind=India, Pak=Pakistan, Sl=Srilanka}
		// lowerKey()  and higherKey()
		System.out.println(nm.lowerKey(4));    // 2
		System.out.println(nm.higherKey(2));   // 4
		
		// floorKey()  and ceilingKey()

		System.out.println(nm.floorKey(3));    // 2
		System.out.println(nm.ceilingKey(3));  // 4
	
		// lowerEntry()  and higherEntry()
		
		System.out.println(nm.lowerEntry(4));    // 2=Australia
	
		System.out.println(nm.higherEntry(1));   // 2=Australia
	
		// floorEntry()  and ceilingEntry()

		System.out.println(nm.floorEntry(3));    // 2=Australia
	
		System.out.println(nm.ceilingEntry(3));   // 4=Srilanka
	
		// firstEntry()  and  lastEntry()
		
		System.out.println(nm.firstEntry());    // 1=India
		
		System.out.println(nm.lastEntry());     // 6=Australia
	
		// descendingMap()
		System.out.println(nm.descendingMap()); // {6=Pakistan, 4=Srilanka, 2=Australia, 1=India}
		
		// subMap()  and headMap()  and tailMap()

		System.out.println(nm.subMap(1,false,4,true));  // {2=Australia, 4=Srilanka}
	
		System.out.println(nm.tailMap(4,true));  // {4=Srilanka, 6=Pakistan}
		
		System.out.println(nm.headMap(4,false));   // {1=India, 2=Australia}
	
		// 
	}

}

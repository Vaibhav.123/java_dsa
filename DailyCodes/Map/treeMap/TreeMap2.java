// userDefined class using tree Map
// using comaparable interface sorting is there

import java.util.*;
class Platform implements Comparable{

	String str;
	int foundYear;

	Platform(String str, int foundYear){
	
		this.str = str;
		this.foundYear = foundYear;
	}
	public String toString(){
	
		return "{"+str+":"+foundYear+"}";
	}
	public int compareTo(Object obj){
	
		return foundYear - (((Platform)obj).foundYear);
	}

}
class TreeMapDemo {

	public static void main(String[] args) {
	
		TreeMap tm = new TreeMap();

		tm.put(new Platform("Youtube",2005),"Google");
		tm.put(new Platform("Instagram",2010),"Meta");
		tm.put(new Platform("Facebook",2004),"Meta");
		tm.put(new Platform("ChatGPT",2022),"OpenAI");

		System.out.println(tm); // {{Facebook:2004}=Meta, {Youtube:2005}=Google, {Instagram:2010}=Meta, {ChatGPT:2022}=OpenAI}
	}
}


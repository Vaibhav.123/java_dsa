// TreeMap is class 
// in TreeMap the entries will be not stored with using HashCode 
// it will print the entries in Sorted manner

import java.util.*;

class TreeMapDemo {

	public static void main(String[] args) {
	
		TreeMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");
		tm.put("Sl","Srilanka");

		System.out.println(tm); // {Aus=Australia, Ban=Bangladesh, Ind=India, Pak=Pakistan, Sl=Srilanka}
	}
}

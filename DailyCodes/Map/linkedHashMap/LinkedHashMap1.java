// in LinkedHashMap insertion order is preserved and print the data by that
//
import java.util.*;

class LinkedHashMapDemo {

	public static void main(String[] args) {
	
		LinkedHashMap hm = new LinkedHashMap();
		
		hm.put("Vaibhav","Shedgaon");
		hm.put("Sagar","Kada");
		hm.put("Pranav","Sangamner");
		hm.put("Tejas","Jejuri");
		
		System.out.println(hm);   // {Vaibhav=Shedgaon, Sagar=Kada, Pranav=Sangamner, Tejas=Jejuri}
					  //

	
	}
}

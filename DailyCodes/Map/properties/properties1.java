

import java.util.*;
import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
		
		Properties obj1 = new Properties();
		
		FileInputStream fobj = new FileInputStream("friends.properties");
		
		obj1.load(fobj);

		System.out.println(obj1.getProperty("Sagar"));   // kada
		
		obj1.setProperty("Krushna","Nashik");

		FileOutputStream outobj = new FileOutputStream("friends.properties");
		
		obj1.store(outobj,"Updated by Vaibhav");
	}
}

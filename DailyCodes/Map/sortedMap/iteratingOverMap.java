
import java.util.*;

class IteratorDemo {

	public static void main(String[] args) {
	
		SortedMap sm = new TreeMap();

		sm.put("Ind","India");
		sm.put("Pak","Pakistan");
		sm.put("SL","Srilanka");
		sm.put("Aus","Australia");
		sm.put("Ban","Bangladesh");

		Set<Map.Entry> data = sm.entrySet();
		Iterator<Map.Entry> itr = data.iterator();

		while(itr.hasNext()){
			
			Map.Entry obj = itr.next();
			System.out.println(obj.getKey()+ " : "+obj.getValue());
		}
	}
}
/* OP: 
Aus : Australia
Ban : Bangladesh
Ind : India
Pak : Pakistan
SL : Srilanka
*/

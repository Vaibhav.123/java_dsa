// Methods in sortedmap
import java.util.*;

class SortedMapDemo {

	public static void main(String[] args) {
	
		SortedMap sm = new TreeMap();

		sm.put("Pak","Pakistan");
		sm.put("Ind","India");
		sm.put("Ban","Bangladesh");
		sm.put("Aus","Australia");
		sm.put("SL","Srilanka");

		System.out.println(sm); // {Aus=Australia, Ban=Bangladesh, Ind=India, Pak=Pakistan, SL=Srilanka}

		// subMap(k,k)
		System.out.println(sm.subMap("Aus","Ind")); // {Aus=Australia, Ban=Bangladesh}

		// headMap(k)
		System.out.println(sm.headMap("Ind"));  // {Aus=Australia, Ban=Bangladesh}

		// tailMap(k)
		System.out.println(sm.tailMap("Ind"));  // {Ind=India, Pak=Pakistan, SL=Srilanka}

		// firstKey()
		System.out.println(sm.firstKey());   // Aus
		
		// lastKey()
		System.out.println(sm.lastKey());    // SL
		
		// keySet()
		System.out.println(sm.keySet());   // [Aus, Ban, Ind, Pak, SL]
	
		// values()
		System.out.println(sm.values());   // [Australia, Bangladesh, India, Pakistan, Srilanka]
		
		// entrySet()
		System.out.println(sm.entrySet());  // [Aus=Australia, Ban=Bangladesh, Ind=India, Pak=Pakistan, SL=Srilanka]
	}
}



import java.util.*;

class Demo{

	public static void main(String[] args) {
	
		HashMap hm = new HashMap();

		hm.put("Ind","India");
		hm.put("Pak","Pakistan");
		hm.put("Ban","Bangladesh");
		hm.put("SL","Srilanka");
		hm.put("Aus","Australia");

		System.out.println(hm);
		
		Set<Map.Entry> data = hm.entrySet();
		Iterator<Map.Entry> itr = data.iterator();

		while(itr.hasNext()){
		
			System.out.println(itr.next());
		}

	}
}

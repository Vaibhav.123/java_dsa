// here in this code the null entries will be free by GC 
// in WeakHashMap 
import java.util.*;

class Demo {

	String str;
	Demo(String str){
	
		this.str = str;
	}
	public String toString(){
	
		return str;
	}

	public void finalize(){
	
		System.out.println("Notify");
	}
}
class HashMapDemo {

	public static void main(String[] args) {
	
		Demo obj1 = new Demo("Vaibhav");	
		Demo obj2 = new Demo("Pranav");	
		Demo obj3 = new Demo("Sagar");	

		WeakHashMap hm = new WeakHashMap();

		hm.put(obj1,20);
		hm.put(obj2,19);
		hm.put(obj3,21);
		
		obj1 = null;
		obj2 = null;
		System.gc();

		System.out.println(hm);
	}
} 
/* OP: 
Notify
Notify
{Sagar=21}

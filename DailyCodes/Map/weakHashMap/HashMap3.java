// here in this code GC cant free the null entries because of HashMap
// this case will be handled in WeakHashMap in the next Code 
import java.util.*;

class Demo {

	String str;
	Demo(String str){
	
		this.str = str;
	}
	public String toString(){
	
		return str;
	}

	public void finalize(){
	
		System.out.println("Notify");
	}
}
class HashMapDemo {

	public static void main(String[] args) {
	
		Demo obj1 = new Demo("Vaibhav");	
		Demo obj2 = new Demo("Pranav");	
		Demo obj3 = new Demo("Sagar");	

		HashMap hm = new HashMap();

		hm.put(obj1,20);
		hm.put(obj2,19);
		hm.put(obj3,21);
		
		obj1 = null;
		obj2 = null;
		System.gc();

		System.out.println(hm);
	}
}

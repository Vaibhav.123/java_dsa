

import java.util.*;

class Demo {
	
	String str;
	Demo(String str){
	
		this.str = str;
	}
	public String toString(){
	
		return str;
	}

	public void finalize(){
	
		System.out.println("Notify");
	}
}
class GCDemo {

	public static void main(String[] args) {
		
		Demo obj1 = new Demo("Vaibhav");		
		Demo obj2 = new Demo("Pranav");		
		Demo obj3 = new Demo("Sagar");	

		System.out.println(obj1);	
		System.out.println(obj2);	
		System.out.println(obj3);

		obj1 = null;
		obj2 = null;
		
		System.gc();  // using this forcefully call to GC

		System.out.println("In main");	
	}
} 
/* OP: Vaibhav
       Pranav
       Sagar
       In main
       Notify
       Notify
*/


// WeakHashMap in this GC can delete the null entrys 
// but in HashMap GC cant delete or free memory of null entries
// for that purpose the WeakHashMap will be in the picture


import java.util.*;


class WeakHashMapDemo {
	
	public static void main(String[] args) {
	
		HashMap hm = new HashMap();
		
		String str1 = "Vaibhav";
		String str2 = "Sagar";
		String str3 = "Pranav";

		hm.put(str1,"Shedgaon");
		hm.put(str2,"Thete");
		hm.put(str3,"Sangamner");

		System.out.println(hm);
	}
}

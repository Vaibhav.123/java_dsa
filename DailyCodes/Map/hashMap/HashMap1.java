// hashSet and HAshMap sotre the element using same calculation
//  
import java.util.*;

class HashMapDemo {

	public static void main(String[] ars) {
	
		HashSet hs = new HashSet();

		hs.add("Vaibhav");
		hs.add("Sagar");
		hs.add("Pranav");
		hs.add("Tejas");

		System.out.println(hs);  // [Pranav, Tejas, Sagar, Vaibhav]


		HashMap hm = new HashMap();

		hm.put("Vaibhav","Gawali");
		hm.put("Sagar","Thete");
		hm.put("Pranav","Pisal");
		hm.put("Tejas","Zagade");

		System.out.println(hm);   // {Pranav=Pisal, Tejas=Zagade, Sagar=Thete, Vaibhav=Gawali}
	}

}

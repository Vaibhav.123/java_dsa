
// it will not store the duplicate keys entry but values will be allow
//  it will consider the latest value in the key
//
// 
import java.util.*;

class HashMapDemo {

	public static void main(String[] args)  {
	
		HashMap hm = new HashMap();

		hm.put(1,"Vaibhav");
		hm.put(2,"Sagar");
		hm.put(1,"Pranav");
		hm.put(3,"Sagar");

		System.out.println(hm);  // {1=Pranav, 2=Sagar, 3=Sagar}
	}
}

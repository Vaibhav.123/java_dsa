
import java.util.*;

class HashMapDemo {

	public static void main(String[] args) {
	
		HashMap hm = new HashMap();

		hm.put("Java",".java");
		hm.put("Python",".py");
		hm.put("Dart",".dart");
		
		System.out.println(hm);  // {Java=.java, Python=.py, Dart=.dart}
		
		// get()
		System.out.println(hm.get("Python"));   	//  .py

		// keySet()
		System.out.println(hm.keySet()); 		//  [Java, Python, Dart]

		// values()
		System.out.println(hm.values());  		// [.java, .py, .dart]

		// entrySet()
		System.out.println(hm.entrySet()); 		//  [Java=.java, Python=.py, Dart=.dart]
	}

}

// runnable interface using Lamda function
//

class Core2web {

	public static void main(String[] ars) {
	
		Runnable obj = () -> {
		
			System.out.println(Thread.currentThread().getName());   // Thread-0
		};

		Thread t1 = new Thread(obj);
		t1.start();
	}
}

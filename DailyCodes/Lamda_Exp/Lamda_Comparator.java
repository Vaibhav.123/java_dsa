import java.util.*;


class Employee{

	String name;
	int id = 0;
	Employee(String name, int id){
	
		this.name = name;
		this.id = id;
	}

	public String toString(){
	
		return name + " : "+id;
	}
}

class Client {

	public static void main(String[] args) {
	
		ArrayList al = new ArrayList();

		al.add(new Employee("Vaibhav",12));
		al.add(new Employee("Sagar",21));
		al.add(new Employee("Pranav",2));

		Collections.sort(al,(obj1,obj2)  -> {
		
			return ((Employee)obj1).name.compareTo(((Employee)obj2).name);
		}
		);
		System.out.println(al);
	}
}

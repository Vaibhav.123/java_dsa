// here in this case 2 .class file will be created 
// lamda function with parameters
interface Core2web {

	String lang(int num);
}

class Year2022 {

	public static void main(String[] args) {
		// we can write by this way also
		Core2web c2w = (int xyz) -> "Bootcamp/Java/CPP/OS : " + xyz;
		// here int parametrs site not must ne write bracket and datatype 
		System.out.println(c2w.lang(10));
		
	}
}

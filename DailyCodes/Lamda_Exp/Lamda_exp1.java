// here in this case 3 .class file will be created 
// but when we use lamda function at that the inner .class file will not be created
// lets see in next code 
// .class files:
// 	Core2web.class
// 	Year2022.class
// 	'Year2022$1.class'
interface Core2web {

	void lang();
}

class Year2022 {

	public static void main(String[] args) {
	
		Core2web c2w = new Core2web() {
		
			public void lang(){
			
				System.out.println("Bootcamp/Java/CPP/OS");
			}
		};
		c2w.lang();

	}
}

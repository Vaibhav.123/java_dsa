// here in this case 2 .class file will be created 
// but when we use lamda function at that the inner .class file will not be created
// lets see in next code 
// .class files:
// 	Core2web.class
// 	Year2022.class
// 	// 'Year2022$1.class'   means this file will not be created

interface Core2web {

	void lang();
	String lang1();
}

class Year2022 {

	public static void main(String[] args) {
		// we can write by this way also
		Core2web c2w = () ->  System.out.println("Bootcamp/Java/CPP/OS");
		c2w.lang();
		
		// we can also write by this way also

		Core2web obj = () ->  "Bootcamp/Java/CPP/OS";

		System.out.println(obj.lang1());

	}
}

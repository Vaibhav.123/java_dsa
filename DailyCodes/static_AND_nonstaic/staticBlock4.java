
//  JAVA supports only global static variables
//  it is called as class variable


class Demo  {

	static int x = 10;

	static {
	
		static int y = 20;	// error: illegal start of expression
	}
	static void fun(){
	
		static int z = 30;	// error: illegal start of expression
	}
	void gun(){
	
		static int p = 40;	// error: illegal start of expression
	}
}

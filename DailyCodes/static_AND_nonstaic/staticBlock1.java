

class Demo {
	
	int x = 10;
	static int y = 20;
	
	static {
	
		System.out.println("Static bllock 1");
	}
	public static void main(String[] args) {
	
		Demo obj = new Demo();
		System.out.println("Main block");
		System.out.println(obj.x);
	}
	static {
	
		System.out.println("Static block 2");
		System.out.println(y);
	}

}

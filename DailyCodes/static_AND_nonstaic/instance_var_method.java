

class Demo {

	int x = 10;	// instance variable [ nonstatic global variable]
	static int y = 20;

	void fun(){		// instance method [ it can access any variable]
	
		System.out.println(x);
		System.out.println(y);
	}
	public static void main(String[] args) {
	
		Demo obj = new Demo();

		obj.fun();
	}

}

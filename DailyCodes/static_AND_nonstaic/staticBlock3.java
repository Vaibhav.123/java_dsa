

class Demo {

	static {
	
		System.out.println("In Static Block1");
	}
	public static void main(String[] args) {
	
		System.out.println("In Main Demo");
	}
}

class Client {

	public static void main(String[] args) {
	
		System.out.println("In Main Client");
	}
	static {
	
		System.out.println("In Static Block2");
	}
}

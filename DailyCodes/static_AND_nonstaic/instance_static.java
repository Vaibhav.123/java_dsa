class Demo {

	static {
	
		System.out.println("In Static Block1");
	}
	{
	
		System.out.println("In Instance Block1");
	}
	Demo(){
	
		System.out.println("Constructor");
	}

	public static void main(String[] args) {
	

		Demo obj = new Demo();

		System.out.println("Main");
	}
	static {
	
		System.out.println("In Static Block2");
	}
	{
	
		System.out.println("In Instance Block2");
	}
}
/* OUTPUT: 
 * In Static Block1
 * In Static Block2
 * In Instance Block1
 * In Instance Block2
 * Constructor
 * Main
 */

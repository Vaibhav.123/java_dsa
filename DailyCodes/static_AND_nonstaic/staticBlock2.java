

class Demo {

	static {
	
		System.out.println("Static block 1");
	}
}
class MainDemo {
	
	public static void main(String[] args){
	
		System.out.println("Main Block");
	}
	static {
	
		System.out.println("Static block main");
	}
}

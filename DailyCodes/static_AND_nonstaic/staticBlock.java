
//  firstly static block will be executed
class Demo {

	static {
	
		System.out.println("In static block");
	}
	public static void main(String[] args) {
	
		System.out.println("In main block");
	}
}
/*  output: 
 *  in Static block 
 *  In main block



class Demo {

	{
	
		System.out.println("Instance Block1");
	}
	public static void main(String[] args) {
	
		Demo obj = new Demo();

		System.out.println("Main");
	}
	
	{
	
		System.out.println("Instance Block2");
	}
}
/* Output: 
 * Instance Block1
 * Instance Block2
 * Main


abstract class Board {

	void Vision(){
	
		System.out.println("Education");
	}

	abstract void Syllabus();
}

class MSBTE extends Board {

	void Syllabus(){
	
		System.out.println("State Board Pattern");
	}
}
class CBSC extends Board {

	void Syllabus(){
	
		System.out.println("Central Board Pattern");
	}
}

class Client {

	public static void main(String[] args) {
	
		Board pattern1 = new MSBTE();
		Board pattern2 = new CBSC();

		pattern1.Vision();
		pattern1.Syllabus();
		
		pattern2.Syllabus();
	}
}

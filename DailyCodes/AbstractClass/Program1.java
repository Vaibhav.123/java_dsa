
// in the below code 
// there is abstract class concept
// ABSTRACT CLASS :
//       1. every abstract method will be define with keyword abstract.
//       2. abstract method does not have body
//       3. if any class have abstract method then that class also define with keyword abstract
//       4. And this concept is called abstract class

class Parent {  // error : Parent is not abstract does not override abstract method marry() in Parent

	void career() {

		System.out.println("Engineer");
	}
	// void marry();   // error : missing method body or declare abstract 
	
}

class Child extends Parent {

	void marry(){
	
		System.out.println("Kiara Advani");
	}
}

class Client {

	public static void main(String[] as) {
	
		Parent obj = new Child();
		obj.marry();
		obj.career();
	}
}

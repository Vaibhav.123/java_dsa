// real time example of abstract class 


abstract class Parent {

	void career(){
	
		System.out.println("Engineer");
	}
	abstract void marry();
}

class Child extends Parent {

	void marry(){
	
		System.out.println("Ashleesha Thakur");
	}
}

class Client {

	public static void main(String[] afs) {
	
		Parent obj1 = new Child();
		obj1.career();
		obj1.marry();
	}
}

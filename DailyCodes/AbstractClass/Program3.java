// abstract class has constructor 
// but we cant make object of it
// constructor i used for constructor chaining

abstract class Parent{
	
	Parent(){
	
		System.out.println("Constructor");
	}
	void career(){
	
		System.out.println("Engineer");
	}
	abstract void marry();
}

class Child extends Parent{

	void marry(){
	
		System.out.println("Anushka Sen");
	}
}

class Client {

	public static void main(String[] args) {
		// Parent obj1 = new Parent();  // parent is abstract, cannot be instantiated
		
		Parent obj = new Child();
		obj.career();
		obj.marry();
	}
}

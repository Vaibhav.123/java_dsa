
//  call to this must be first statement in constructor
//
 
class Demo {

	int x = 10;

	Demo(){
	
		System.out.println(x);
	}

	Demo(int x){
		
		// this();
		System.out.println(this.x);
		this();    // error: this() must be the first statement in the constructor
			   //
	}

	public static void main(String[] args) {
	
		Demo obj = new Demo(120);
	}
}

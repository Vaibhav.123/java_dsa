

class Demo {

	private int x = 10;

	Demo(){
	
		System.out.println("In no args Constructor");
	}

	void fun() {
	
		System.out.println(x);
	}
}

class Client{

	public static void main(String[] args) {
	
		Demo obj = new Demo();

		obj.fun();
		//System.out.println(obj.x);  error : x has private access
	}
}

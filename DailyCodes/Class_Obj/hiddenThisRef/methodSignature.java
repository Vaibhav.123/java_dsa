// 

class Demo {

	int x = 10;

	Demo(){
	
		System.out.println("In no args Constructor");
	}
	//  error: Constructor Demo() is alredy defined 
	//  due to method signature 
	//  we cannnot write same default construtor 
	Demo(){
	
		System.out.println("In no args Constructor");
	}

	Demo(int x) {
	
		System.out.println("In para Constructor");
	}

	public static void main(String[] args) {
	
		Demo obj1 = new Demo();

		Demo obj2 = new Demo(19);
	}
}


// error: recursive constructor invocation
class Demo {

	int x = 10;

	Demo(){
		this(20);
		System.out.println(x);
	}

	Demo(int x){
		this();
		System.out.println(x);
	}

	public static void main(String[] args)  {
	
		Demo obj = new Demo(30);
	}
}

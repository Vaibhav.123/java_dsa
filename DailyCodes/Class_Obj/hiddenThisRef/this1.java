
//  this 
class Demo {
	int x = 10;
	Demo(){ 	//  Demo(Demo this)
	
		System.out.println("In Constructor");
		System.out.println(this.x);
		System.out.println(x);
	}

	void fun(){ 	// fun(Demo this)
	
		System.out.println(this.x);
	}

	public static void main(String[] args)  {
		
		Demo obj = new Demo();		// Demo(obj);

		obj.fun();	// fun(obj);

	}
}

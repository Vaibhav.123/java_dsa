
// 
class Demo {
	int x = 10;

	Demo(){		// Demo(Demo this)
		
		System.out.println(x);		// 10
		System.out.println(this.x);	// 10
		System.out.println(this);	// Demo@1234
	
	}

	void fun(){		// fun(Demo this)
	
		System.out.println(x);		// 10
		System.out.println(this.x);	// 10
		System.out.println(this); 	// Demo@1234
	}

	public static void main(String[] args) {
	
		Demo obj = new Demo(); 		// Demo(obj)
		
		obj.fun();			// fun(obj)

	}
}

//  2nd use of this 
//  ekch objct banvun multiple constructors la access akrta yet
//  eka constructor madhun dusrya constructor la call deta yeto

class Demo {

	int x = 10;
	
	Demo (){
	
		System.out.println("In no args Constructor");
	}

	Demo(int x) {
		
		this();
		System.out.println("In para Constructor");
	}
	public static void main(String[] args) {
	
		Demo obj = new Demo(20);
	}
}
// OUTPUT : 
// In no args Constructor
// In para Constructor
//



class Demo  {
	
	Demo () {
	
		System.out.println("In constructor");
	}
	
	void fun(){
	
		Demo obj1 = new Demo();
		
		System.out.println(obj1);  // Demo@12345f
	}

	public static void main(String[] args)  {
	
		Demo obj = new Demo();

		obj.fun();
		
		System.out.println(obj);   // Demo@32456c

		Demo obj2 = new Demo();

		System.out.println(obj2);   // Demo@87664a
	}
}

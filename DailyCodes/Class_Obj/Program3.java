
class Demo  {

	Demo (){
	
		System.out.println("In Constructor");
	}
	
	static void fun(){
	
		System.out.println("In fun classic");
	}

	public static void main (String[] args) {
	
		System.out.println("In main");

		Demo obj = new Demo();   // here is constructor gets automatically called 
		
		fun();
		

	}
}

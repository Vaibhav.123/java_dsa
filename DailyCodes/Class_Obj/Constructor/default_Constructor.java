

class Demo {

	Demo (){  // but it has hidden parameter =>> Demo(Demo this) 
	
		System.out.println("Default constructor");
	}
	public static void main(String[] args) {
	
		Demo obj = new Demo();
	}
}



class Demo {

	Demo(int x){  // parametarized constructor =>> Demo(Demo this, int x);
	
		System.out.println(x);
	}

	public static void main(String[] ars)  {

		Demo obj = new Demo(10);   // Demo(obj,10)
	}
}



class Demo {

}

class Client {

	public static void main(String[] args)  {
		// this class is calles as Anonymous inner class 
		// this class has no name 
		//
		Demo obj = new Demo (){
		
		};
	}
}

// .class files as :
// 'Client$1.class'
// Client.class
// Demo.class

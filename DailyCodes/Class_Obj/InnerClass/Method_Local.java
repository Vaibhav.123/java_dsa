

class Outer {

	void m1(){
		// this class is only inmpicture when we call the methodand scope of this class only the method call
		// when method gets return then class will be not in picture
		
		class Inner {
		
			void m1(){
			
				System.out.println("In m1-Inner");
			}
		}
		// we to compulsory makes object in the method 
		// out of the method we cannot access that class method

		Inner obj = new Inner();
		obj.m1();
	}

	void m2(){
	
		System.out.println("In Outer-m2");
	}

	public static void main(String[] args){
	
		Outer obj1 = new Outer();
		obj1.m2();

		obj1.m1();
	}
}


// basic structure of inner class 
// basic ode of inner class

class Outer {

	class Inner {
	
		void m1(){
		
			System.out.println("In m1-Inner");
		}
	}
	void m2(){
	
		System.out.println("In m2-Outer");
	}
}

class Client {

	public static void main(String[] args)  {
	
		Outer obj1 = new Outer();
		obj1.m2();    // In m2-Outer
		
		//Inner obj2 = new Inner();   // error : cannot find symbol, 
		
		Outer.Inner obj3 = obj1.new Inner();   //  1st way

		Outer.Inner obj4 = new Outer().new Inner();   // 2nd way

		obj3.m1();
		obj4.m1();
	}
}


//  main in Outer class 
//  here we take Inner class ref only not ref of Outer class
//  if we write main in other class then we have take of both classes and also object of both classe
class Outer {

	class Inner {
	
		void m1(){
			
			System.out.println("In Inner-m1");
		}
	}
	void m2(){
	
		System.out.println("In Outer-m2");
	}

	public static void main(String[] args) {
	
		Inner obj = new Outer().new Inner();    // here we have firstly create object of outer class then inner class 
							// i.e firstly open the lock of outer then inner lock
							// otherwise m1 () cannot be accessed
							//
		obj.m1();
		
		Outer obj1 = new Outer();
		obj1.m2();
	}
}

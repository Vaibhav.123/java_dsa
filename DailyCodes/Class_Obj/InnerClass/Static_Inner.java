

class Outer {

	class Inner {
	
		static void m1(){
		
			System.out.println("In m1-Inner");
		}
	}

	static class Inner1 {
	
		static void m3(){
		
			System.out.println("In m3-static Inner");
		}
	}

	void m2(){
	
		System.out.println("In m2-Outer");
	}

	public static void main(String[] args) {
	
		Inner.m1();
	      	Inner1 obj = new Inner1();
       		obj.m3();		
	}
}
/*
class Client {
	
	public static void main(String[] args){
	
		Outer obj1 = new Outer();
		obj1.m2();
		// we have take ref of Outer and inner 
		// and create object of only inner class not outer class
		// static will be calles using the class name so 
		//
		Outer.Inner obj2 = new Outer.Inner();
		obj2.m1();
	}
}*/

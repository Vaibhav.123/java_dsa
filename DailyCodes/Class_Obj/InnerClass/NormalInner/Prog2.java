
//  Inner class constructor has two parameters are there 
class Outer {

	class Inner {
		
		Inner(){  // internally =>  Outer$Inner(Outer$Inner this, Outer this$0)
		
			System.out.println("Inner-Constructor");
			System.out.println(this);
			System.out.println(this$0);   // cannot print this$ 
		}

		void fun1(){
		
			System.out.println("Fun1-Inner");
		}
	}
	void fun2(){
		
		System.out.println("Fun2-Outer");
	}
}

class Client {


	public static void main(String[] args)  {
	
		Outer.Inner obj = new Outer().new Inner();   // Outer$Inner(obj,new Outer())
	}
}


// this$ it represents only in in which outer class the inner class is written
// it reprents only which outer class is that
//
class Outer {

	class Inner {
	
		void fun1(){
		
			System.out.println("fun1-Inner");
		}
	}
	void fun2(){
		
		System.out.println("fun2-Outer");
	}
}

class Client {

	public static void main(String[] args)  {
	
		Outer obj1 = new Outer();   // Outer(obj1)
		
		Outer.Inner obj2 = obj1.new Inner();   // Outer$Inner(obj2,obj1)
		
		Outer.Inner obj3 = obj1.new Inner();   // Outer$Inner(obj3,obj1)
	}
}

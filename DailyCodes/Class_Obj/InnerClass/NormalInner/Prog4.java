
// inner class access static and non static variables and methods of outer class 
//
class Outer {
	
	static int x = 10;
	int y = 20;

	class Inner {
		
		int x = 30;

		void fun1(){
		
			System.out.println("fun1-Inner");

			System.out.println(x);
			System.out.println(Outer.x);
			System.out.println(y);

			fun2();
		}
	}
	void fun2(){
		
		System.out.println("fun2-Inner");
	}
}

class Client {

	public static void main(String[] args)  {
	
		//Outer obj = new Outer();
		
		Outer.Inner obj = new Outer().new Inner();

		obj.fun1();
	}
}

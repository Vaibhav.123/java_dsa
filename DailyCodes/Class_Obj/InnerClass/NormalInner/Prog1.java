

class Outer {

	class Inner {
	
		void fun1(){
		
			System.out.println("fun1-Inner");
		}
	}
	void fun2(){
	
		System.out.println("fun2-Outer");
	}
}

class Client {


	public static void main(String[] args) {
	
		Outer obj1 = new Outer();
		obj1.fun2();

		Outer.Inner obj2 = new Outer().new Inner();

		Outer.Inner obj3 = obj1.new Inner();

		obj2.fun1();
		obj3.fun1();
	}
}

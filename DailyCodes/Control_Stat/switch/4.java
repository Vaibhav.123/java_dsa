//   ERROR  in this code because the if we written character in case then it will consider ascii value of its character
//   so it gives duplicate case label error

class SwitchDemo {

	public static void main(String[] as){
	
		int x=65;

		switch (x){
			
			case 65:
				System.out.println("One");
				break;

			case 'A':
				System.out.println("Two");
				break;

			case 66:
				System.out.println("Three");
				break;

			case 'B':
				System.out.println("Four");
				break;

			case 5:
				System.out.println("Five");
				break;
			
			default :
				System.out.println("No match");
				break;


		}
		System.out.println("After switch");
	}
}

/* O/P:
 *    Three
 *    After Switch
 *    */

//   ERROR ????   because the cases have constant values but in this we assign the variable to the all cases
//   so there is an  error 
//   ERROR ====>   constant expression required for all cases except default

class SwitchDemo {

	public static void main(String[] as){
	
		int x=3;
		int a=1;
		int b=2;

		switch (x){
			
			case a:
				System.out.println("One");
				break;

			case b:
				System.out.println("Two");
				break;

			case a+b:              
				System.out.println("Three");
				break;

			case a+a+b
				:
				System.out.println("Four");
				break;

			case a+b+b:			
				System.out.println("Five");
				break;
			
			default :
				System.out.println("No match");
				break;


		}
		System.out.println("After switch");
	}
}

/* O/P:
 *    Three
 *    After Switch
 *    */

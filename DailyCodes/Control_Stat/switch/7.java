//   O/P ==>  Monday
//            After switch
//
//  But this string class will be in 1.6 above versions 
//  before the version 1.6 there will error for this string class 
//  but in latest version it will be updated
//  so we can write string also in cases

class SwitchDemo {

	public static void main(String[] as){
	
		String str = "Mon";

		switch (str){
			
			case "Mon":
				System.out.println("Monday");
				break;

			case "Tue":
				System.out.println("Tuesday");
				break;

			case "Wed":              
				System.out.println("Wednesday");
				break;

			case "Thu":
				System.out.println("Thursday");
				break;

			case "Fri":			
				System.out.println("Friday");
				break;
			
			default :
				System.out.println("No match");
				break;


		}
		System.out.println("After switch");
	}
}

/* O/P:
 *  
 */ 

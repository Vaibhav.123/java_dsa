//   this is the perfect code, we can write default case in switch statement anywhere 

class SwitchDemo {

	public static void main(String[] as){
	
		int x=3;

		switch (x){
			
			case 1:
				System.out.println("One");
				break;

			case 2:
				System.out.println("Two");
				break;

			case 3:
				System.out.println("Three");
				break;

			case 4:
				System.out.println("Four");
				break;

			case 5:
				System.out.println("Five");
				break;
			
			default :
				System.out.println("No match");
				break;


		}
		System.out.println("After switch");
	}
}

/* O/P:
 *    Three
 *    After Switch
 *    */

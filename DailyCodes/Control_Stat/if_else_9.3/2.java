//  Take two integers A and B as an input 
//  Print the max of two
//  Assume: x and y are not equal
//  I/P: x=5, y=7
//  O/P: 7 is greater



class Maximum  {
	
	public static void main(String[] args){
		
		int x=5;
		int y=7;

		if(x > y){
			System.out.println(x+ " is greater");
		}else{
			System.out.println(y+ " is greater");
		}
	}
}

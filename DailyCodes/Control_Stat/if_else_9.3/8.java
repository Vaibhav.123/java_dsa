// Electricity Problem 
// Given an integer input A which represents
// units of electricity consumed at your house
// Calculate and print the bill amount 
// units <= 100 : price per unit is 1
// units > 100  : price per unit is 2
//
// I/P: 50
// O/P: 50
// I/P: 200
// O/P: 300



class Problem {
	
	public static void main(String [] args) {
		
		int units = 200;

		if(units <= 100){
			
			System.out.println(units*1);
	
		}else {
		
			System.out.println(100*1 + (units-100)*2);
		}
	}
}

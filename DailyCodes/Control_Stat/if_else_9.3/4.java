//  Given temperature of person in farrenheit
//  Print whether the person has high,normal,low temp
//  > 98.6 			==> high
//  98.0 <= and <= 98.6 	==> Normal
//  < 98.0			==> low



class Temp {
	
	public static void main(String[] args){
		
		float temp = 100f;

		if(temp > 98.6f){

			System.out.println("High temperature");

		}else if(temp < 98.0f){

			System.out.println("Low temperature");
		
		}else{

			System.out.println("Normal temperature");
			
		}
	}
}

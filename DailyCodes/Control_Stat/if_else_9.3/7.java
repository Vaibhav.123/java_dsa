//  Given an integer value is input
//  Print fizz if it is divisible by 3
//  Print buzz if it is divisible by 5
//  Print fizz-buzz if it is divisible by both 
//  If not then print "Not divisible by both"



class Divisible  {
	
	public static void main(String[] args){
		
		int num = 15;

		if(num%3==0 && num%5==0){
			
			System.out.println("Divisible by both 3 and 5");
	
		}else if(num%3==0){
			
			System.out.println("Divisible by 3");

		}else if(num%5==0){
			
			System.out.println("Divisible by 5");
		}else {
		
			System.out.println("Not divisible by both");
			
		}
	}
}

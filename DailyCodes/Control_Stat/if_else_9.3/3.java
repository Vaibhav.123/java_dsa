
//  Take two integers A and B as an input 
//  Print the max of two
//  I/P: x=5, y=7
//  O/P: 7 is greater
//  I/P: x=7, y=7
//  O/P: Both are equal



class Number  {
	
	public static void main(String[] args){
		
		int x=7;
		int y=7;

		if(x>y){
			System.out.println(x+ " is greater");
		}else if(x<y){
			System.out.println(y+ " is greater");
		}else{
			System.out.println("Both are equal");
			
		}

	
	}
}


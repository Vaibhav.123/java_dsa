//  Take an integer as input & print whether it is divisible by 4 or not 
//
//  I/P: 5
//  O/P: Not divisible



class Demo  {
	
	public static void main(String[] args){
		
		int num = 20;

		if(num%4==0){
			
			System.out.println("Divisible by 4");
		}else{
			
			System.out.println("Not divisible");
		}
	}	
}

//  Given an integer check whether it is even or odd

//  I/P: 5
//  O/P: 5 is odd
//  I/P: 4
//  O/P: 4 is even



class Demo  {
	
	public static void main(String[] args){
		
		int num = 5;

		if(num%2==0){
			
			System.out.println(num+ " is even");

		}else{
			
			System.out.println(num+ " is odd");
		
		}
	}
}

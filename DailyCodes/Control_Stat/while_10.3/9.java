//  Take an input N
//  and check the number is palindrome or not
//  I/P: 121
//  O/P: 121 is palindrome number
//  I/P: 123
//  O/P: 123 is not palindrome number


class Demo  {
	
	public static void main(String[] s){
	
		int num=123;
		int x=num;
		int rev=0;

		while(num!=0){
			
			int rem=num%10;

			rev=rev*10+rem;

			num=num/10;

		}
		if(x==rev)
			System.out.println(x+ " it is palindrome number");
		else
			System.out.println(x+" it is not palindrome number" );
	}
}

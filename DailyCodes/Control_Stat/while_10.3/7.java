  // Given an integer N
  // Print the product of its digits
  // Assume: N>=0
  // I/P: 6531
  // O/P: 15



class Demo  {

	public static void main(String [] s){
	
		int num=6531;
		int pro=1;

		while(num!=0){
		
			pro = pro* (num%10);

			num=num/10;
		}
		System.out.println("Product is: "+pro);

	}
}

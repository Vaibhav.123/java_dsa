
//  Given an integer N
//  Print all its digits
//  I/P: 6531
//  O/P:
//  1
//  3
//  5
//  6


class Demo  {
	
	public static void main(String[] args){
	
		int N=6531;


		while(N!=0){
			
			System.out.println(N%10);
			N=N/10;
		}
	}
} 

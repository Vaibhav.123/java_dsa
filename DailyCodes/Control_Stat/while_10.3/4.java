
//  Take an integer N input 
//  Print the multiples of 4 between 1 to N
//  I/P: 22
//


class Demo {

	public static void main(String[] asg){
	
		int N=22;
		int i=1;

		while(i<=N){
			
			if(i%4==0){
			
				System.out.println(i);
			}
			i++;
		}
	}
}

//   Take an integer N as Input
//   Print perfect square till N
//   Perfect square: An integer whose square root is an integer
//   I/P: 30
//   O/P: 1 4 9 16 25



class Demo  {
	
	public static void main(String[] s){
	
		int num=30;
		int i=1;
		if(num>=0){
			while(i*i <=30){
				
				System.out.println(i*i);
				i++;
			}
		}else {
			
			System.out.println("Enter positive number");
		}
	}
} 

  // Given an integer N
  // Print the sum of its digits
  // Assume: N>=0
  // I/P: 6531
  // O/P: 15



class Demo  {
	
	public static void main(String[] s){
	
		int num=6531;
		int sum=0;
		if(num>=0){
			while(num!=0){
			
				sum = sum + num%10;
				num=num/10;
			}
			System.out.println("Sum is: "+sum);

		}else {
			System.out.println("Enter positive number");
		}
	}
} 


//  take an integer N input 
//  and find odd numbers between 1 to N
//  I/P: 10


class Demo{

	public static void main(String[] args){
	
		int N=10;
		int i=1;

		while(i<=10){
		
			if(i%2 != 0){
				
				System.out.println(i);
			}
			i++;
		}
	}
}

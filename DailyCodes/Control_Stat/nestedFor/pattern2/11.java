
/*     1
 *     2  c
 *     4  e  6           
 *     7  h  9  i
 */


class Demo  {
	
	public static void main(String[] args){
		
		int row=4;
		int num=1;
		int x=1;
		char ch='a';

		for(int i=1; i<=x; i++){
			
			if(i%2==1){
				System.out.print(num++ +"   ");
				ch++;
			}else {
				
				System.out.print(ch++ +"   ");
				num++;
			}

			if(i==x){
				
				System.out.println();
				i=0;
				x++;

			}
			if(x==row+1)
				break;
		}
	}
}


/*    A  B  C  D
 *    A  B  C
 *    A  B
 *    A
 */


class Demop  {
	
	public static void main(String[] args){
		
		int row=4;
		char ch='A';

		for(int i=1; i<=row; i++){
			
			System.out.print(ch++ +"  ");

			if(i==row){
				
				System.out.println();
				ch='A';
				i=0;
				row--;
			}
			if(row==0)
				break;
		}
		
	}
}

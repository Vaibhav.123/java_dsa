
/*   * 
 *   *  #
 *   *  #  *
 *   *  #  *  #
 *   *  #  *  #  *
 */


class Demo  {
	
	public static void main(String[] args){
	
		int row=5;
		int x=1;

		for(int i=1; i<=x; i++)	{
				
			if(i%2==1){
				
				System.out.print("*  ");
			}else{
				
				System.out.print("#  ");
			}
			if(i==x){
				
				System.out.println();
				i=0;
				x++;

			}
			if(x==row+1)
				break;
		}
	}
}


/*    *  *  *  * 
 *    *  *  * 
 *    *  * 
 *    *
 */


class Demo  {
	
	public static void main(String[] args){
		
		int row=4;

		for (int i=1; i<=row; i++){
		
			System.out.print("*  ");	

			if(i==row){
				
				System.out.println();
				row--;
				i=0;
			}

			if(row==0)
				break;

		}
	}
}


/*    A  1  B  2
 *    C  3  D
 *    E  4
 *    F
 */

class Demo {

	public static void main(String[] args){
		
		int row=4;
		char ch='A';
		int x=1;

		for(int i=1; i<=row; i++){
			
			if(i%2==1){
				System.out.print(ch++ +"   ");
			}else{
				
				System.out.print(x++ +"   ");
			}

			if(i==row){
				
				System.out.println();
				i=0;
				row--;

			}
			if(row==0)
				break;
		}
	}
}

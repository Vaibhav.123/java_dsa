
/*    	1	
 *     	4	3
 *     	16	5	36 
 *     	49	8	81	10
 */


class Demo {
	
	public static void main(String[] args){
		
		int row=5;
		int num=1;
		int x=1;

		for(int i=1; i<=x; i++){
			
			if(i%2==1){
				System.out.print(num*num +"	 ");	
				num++;
			}else{
				
				System.out.print(num++ +"	");
			}

			if(i==x){
				
				System.out.println();
				i=0;
				x++;
			}

			if(x==row+1)
				break;
		}
	}
}

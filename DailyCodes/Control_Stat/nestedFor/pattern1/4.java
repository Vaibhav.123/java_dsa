/*    A  1  A  1  
/*    A  1  A  1  
/*    A  1  A  1
/*    A  1  A  1
 */


class Demo  {
	
	public static void main(String[] args){
		
		int x=1; 
		char ch='A';
		int row=5; 
		int col=4;

		for(int i=1; i<=row*col; i++ ){
			
			if(col%2==0){
				if(i%2==0){
					System.out.print(x +"  ");
				}
				else{
				
					System.out.print(ch +"  ");
			
				}
				if(i%4==0) {
				
					System.out.println();
				}
			}else {
				
				if(i%2==0){
					System.out.print(x +"  ");
				}
				else{
				
					System.out.print(ch +"  ");
				
				}
				if(i%5==0) {
				
					System.out.println();
				}

			}
		}
	}
}

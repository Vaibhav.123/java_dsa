/*   1   2   3   4
 *   5   6   7   8
 *   9   10  11  12
 */


class Demo {
	
	public static void main(String[] a){
		int x=1;

		for(int i=1; i<=12; i++){
			
			System.out.print(x++ +"    ");

			if(i%4==0){
			
				System.out.println();

			}
		}
	}
}

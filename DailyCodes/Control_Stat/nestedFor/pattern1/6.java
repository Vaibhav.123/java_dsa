
/*    * _ _ _ *
/*    * _ _ _ *
/*    * _ _ _ *
/*    * _ _ _ *
/*    * _ _ _ *
 */


class Demo  {
	
	public static void main(String[] asrg){
		
		int row=5; 
		int cnt=0;

		for(int i=1; i<=row; i++){
			
			if(i%row==1 || i%row==0){
				
				System.out.print("* ");
			}else{
				
				System.out.print("_ ");
			}

			if(i%row==0){
				
				System.out.println();
				cnt++;
				i=0;

			}
			if(cnt==row)
				break;
		}
	}
}

/*   3  4  5
 *   3  4  5
 *   3  4  5
 */


class Demo {
	
	public static void main(String[] args) {
		int x=3;

		for(int i=1; i<=9; i++){
			
			System.out.print(x++ +"  ");

			if(i%3==0){
			
				System.out.println();
				x=3;
			}
		}
	}
}

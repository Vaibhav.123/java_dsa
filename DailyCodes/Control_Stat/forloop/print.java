//  WAP to print start to end

import java.util.*;

class Demo {

	public static void main(String[] s){
		
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Start: ");	
		int x=sc.nextInt();
		System.out.print("Enter End: ");
		int y=sc.nextInt();
		if(x<y){
			for(int i=x; i<=y; i++){
				System.out.println(i);
			}
		}else{
			
			for(int i=x; i>=y; i--){
				System.out.println(i);
			}
		}
	}
}

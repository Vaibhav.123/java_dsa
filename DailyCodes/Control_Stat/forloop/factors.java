//  WAP take an input from user and find the factors of that number 


import java.util.*;

class Demo {
	
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number: ");
		int N= sc.nextInt();
		if(N==0){
			
			System.out.println("0");
		}else if(N>0){
			for(int i=1; i<=N; i++){
			
				if(N%i==0){
			
					System.out.println(i);
				}
			}
		}else{
			
			for(int i=N; i<0; i++){
				
				if(N%i==0){
					
					System.out.println(i);
				}
			}
		}
	}
}

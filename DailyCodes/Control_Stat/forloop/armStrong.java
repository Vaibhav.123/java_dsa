//  WAP  take an input from user 
//  and check that number is armStrong number or not


import java.util.*;

	
class Demo {
	
	public static void main(String[] s){
		
		Scanner sc=new Scanner(System.in);
		System.out.println("enter number: ");
		int num=sc.nextInt();
		int count=0;
		int sum=0;
		int temp=num;

		while(num!=0){
			count++;
			num=num/10;
		}

		num=temp;

		while(num!=0){
			int mult=1;
			int rem=num%10;
			for(int i=1; i<=count; i++){
				
				mult=mult*rem;
			}
			sum=sum+mult;
			num=num/10;
			
		}
		num=temp;

		if(num==sum){
			
			System.out.println(num+ " is armStrong number");
		}else{
			
			System.out.println(num+ " is not armStrong number");
		}



	}
}

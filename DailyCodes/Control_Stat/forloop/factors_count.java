//  WAP take N input and print the count of factors of number 


import java.util.*;

class Demo {
	
	public static void main(String[] as){
	
		Scanner sc= new Scanner(System.in);

		System.out.print("Enter number:  ");
		int N=sc.nextInt();
		int count=0;

		if(N>0){
			
			for(int i=1; i<=N; i++){
				if(N%i==0){
					
					count++;
				}
			}
		}else {
			
			for(int i=N; i<0; i++){
			
				if(N%i==0){
					
					count++;
				}
			}
		}
		System.out.println("Factors count is "+count);
	}
}

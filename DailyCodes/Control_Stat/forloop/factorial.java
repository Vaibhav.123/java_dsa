//  WAP to print the factorial of given number N
//

import java.util.*;

class Demo {

	public static void main(String[] args){
		
		Scanner sc= new Scanner (System.in);
		System.out.print("Enter number: ");
		int N=sc.nextInt();
		int x=N;
		int fact=1;
		if(N>=0){
			
			for(int i=1; i<=N; i++){
				
				fact=fact*i;
			}
		}else{
			
			for(int i=N; i<0; i++){
				fact=fact*i;
			}
		}
		System.out.println("Factorial of "+x+" is "+fact);
	}
}

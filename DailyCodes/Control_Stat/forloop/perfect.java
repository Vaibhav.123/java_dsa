//  WAP to take an input from user
//  and check the number is perfect or not


import java.util.*;

class Demo  {
	
	public static void main(String[] as){
		
		Scanner sc=new Scanner (System.in);
		System.out.print("Enter number: ");
		int num=sc.nextInt();
		int sum=0;

		for(int i=1; i<num; i++){
			
			if(num%i==0){
				
				sum=sum+i;
			}
		}

		if(sum==num){
			
			System.out.println(num+" is perfect number");
		}else {
			
			System.out.println(num+" is not perfect number");
		}
	}
}

//  WAP to print odd numbers from range 1 to N
//


import java.util.*;

class Demo {

	public static void main(String[] sr){
		
		Scanner sc=new Scanner (System.in);
		System.out.print("Enter number: ");
		int N=sc.nextInt();
		if(N>=0){
			for(int i=1; i<=N; i++){
				if(i%2!=0){
				
					System.out.println(i);
				}
			}
		}else{
			
			System.out.println("Enter positive number");
		}
	}


}


//   length of the 2D array and
//   1D array


class Demo  {

	public static void main(String[] args)  {
		
		int arr[][] = new int [4][3];	// 4

		int arr2[][] = new int [3][];	// 3
		
//		int arr[][] = new int [][3];	// error : 

		int arr1[] = new int [2];	// 2

		System.out.println("2D : "+arr.length);
		System.out.println("1D : "+arr1.length);
		System.out.println("2D : "+arr2.length);
	
	}
}

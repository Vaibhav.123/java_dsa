//  taking jagged array as input from user  



import java.io.*;

class Demo  {

	public static void main(String[] args) throws IOException  {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number of rows :");
		int row=Integer.parseInt(br.readLine());
		int arr[][] = new int [row][];
			
		for(int i=0; i<arr.length; i++){
			
			System.out.println("Enter column size for "+i+" row :");
			arr[i]=new int [Integer.parseInt(br.readLine())];
		}

		/*
		System.out.println("Enter size for column");
		int size1=Integer.parseInt(br.readLine());
		int size2=Integer.parseInt(br.readLine());
		int size3=Integer.parseInt(br.readLine());
		
		arr[0]=new int [size1];
		arr[1]=new int [size2];
		arr[2]=new int [size3];
		*/

		System.out.println("Enter array elements");

		for(int i=0; i<arr.length; i++){
		
			for(int j=0; j<arr[i].length; j++){
			
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		System.out.println("Array elements are : ");

		for(int x[] : arr){
		
			for(int y : x){
			
				System.out.print(y+"  ");
			}
			System.out.println();
		}
	}

}

//   pass variable to method
//   and return value from method

class Demo  {
	
	static int fun(int x) {
		
		System.out.println(x);

		return x+10;
	}

	public static void main(String rgs[]){
		
		int x=10;

		System.out.println(fun(x));

	}

}

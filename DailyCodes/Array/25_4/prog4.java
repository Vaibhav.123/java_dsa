//   pass array to a method 
//   changhe the data
class Demo  {
	
	static void fun(int arr[]) {
		
		arr[0]=50;

		for(int x : arr)	
			System.out.println(x);

	}

	public static void main(String rgs[]){
		
		int arr[] = {10,20,30};

		for(int x : arr)
			System.out.println(x);

		fun(arr);

		
	}

}
//O/P:
//    10
//    20
//    30
//    50
//    20
//    30

//   pass array to a method 

class Demo  {
	
	static void fun(int arr[]) {
		
		for(int x : arr)	
			System.out.println(x);

	}

	public static void main(String rgs[]){
		
		int arr[] = {10,20,30};

		for(int x : arr)
			System.out.println(x);

		fun(arr);

		
	}

}
//O/P:
//  10
//  20
//  30
//  10
//  20
//  30

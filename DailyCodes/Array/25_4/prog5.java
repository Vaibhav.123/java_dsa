//   pass array to a method 
//   changhe the data
class Demo  {
	
	static void fun(int arr[]) {

		for(int i=0; i<arr.length; i++)
			arr[i]=arr[i] + 50;
	

		for(int x : arr)	
			System.out.println(x);

	}

	public static void main(String rgs[]){
		
		int arr[] = {10,20,30};

		for(int x : arr)
			System.out.println(x);

		fun(arr);

		for(int x : arr)
			System.out.println(x);


	}

}
//O/P:
//   10
//   20
//   30
//   60
//   70
//   80
//   60
//   70
//   80

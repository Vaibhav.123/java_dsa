

class Demo  {
	
	void fun(int arr[]) {
		
		arr[1]=70;
		arr[2]=80;

	}

	public static void main(String[] args)  {
		
		int arr[] = {10,20,30,40};
		System.out.println("In main");
		for(int x : arr){
			
			System.out.println(x);
		}

		Demo obj = new Demo();

		obj.fun(arr);
		System.out.println("After fun");
		
		for(int x : arr){
			
			System.out.println(x);
		}

	}
}

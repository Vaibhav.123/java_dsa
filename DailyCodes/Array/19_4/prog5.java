

class Demo  {
	
	public static void main(String[] args){
		
		int arr[] =new int[]{10,20,200,127};
		int arr2[] = new int[]{10,20,200,127};

		// data same but address of both arrays are not equal
		System.out.println(arr);
		System.out.println(arr2);

		//but the address of actual data of the both arrays are equal 
		//if the data means elements are between the range from -127 to 127  but data will be equal of both arrays
		//if not in this range then if data will be same then also the adress will be different 

		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr2[0]));

		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr2[1]));

		System.out.println(System.identityHashCode(arr[2]));	// diff
		System.out.println(System.identityHashCode(arr2[2]));	// diff

		System.out.println(System.identityHashCode(arr[3]));
		System.out.println(System.identityHashCode(arr2[3]));

		System.out.println(System.identityHashCode(arr));	// diff 
		System.out.println(System.identityHashCode(arr2));	// diff
	}
}

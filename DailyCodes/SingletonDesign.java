  // this is the singleton design pattern 
  // it is used for only making one object of any class then it will be usef
  // if we make constructor private then also we can create object using this pattern

class Singleton {

	static Singleton obj = new Singleton();
	
	Singleton(){
	
		System.out.println("Constructor");
	}
	static Singleton getObject(){
	
		return obj;
	}
}

class Client {

	public static void main(String[] ags) {
	
		Singleton obj1 = Singleton.getObject();	
		System.out.println(obj1);
		
		Singleton obj2 = Singleton.getObject();	
		System.out.println(obj2);
		
		Singleton obj3 = Singleton.getObject();	
		System.out.println(obj3);

	}
}

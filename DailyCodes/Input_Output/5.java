//  InputStreamReader and BufferedReader in one line
//  runtime error in this code because buffered reader class connection will be closed from the keyboard 
//  Coonection of both can be remove 
//  if br1 is close then br2 will also bw clkose
//  Input pipe will be remove


import java.io.*;

class Demo  {
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br1 = new BufferedReader (new InputStreamReader(System.in));
		BufferedReader br2 = new BufferedReader (new InputStreamReader(System.in));
	
		System.out.println("Enter name: ");
		String name1 = br1.readLine();

		System.out.println(name1);

		br1.close();

		System.out.println("Enter name: ");
		String name2 = br2.readLine();

		System.out.println(name2);
		
	}
}

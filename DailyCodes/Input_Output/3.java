
//  using InputStreamReader
//  this is only for taking character input and output
//


import java.io.*;

class InputStreamReaderDemo  {
	
	public static void main(String[] args) throws IOException{
		
		InputStreamReader obj = new InputStreamReader(System.in);

		System.out.println("Enter character:  ");

		char ch=(char)obj.read();

		System.out.println(ch);

	}
}

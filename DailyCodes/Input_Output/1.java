

//  input output using Scanner class 
//  in this 3 classes are uses Scanner,string and object class is also there,
//  it is compulsory so it it is written by java itself
//

import java.util.Scanner;
class ScannerDemo{  // here is object class
	
	public static void main(String[] args){
		
		Scanner obj= new Scanner (System.in);

		System.out.println("Enter name:  ");

		String name=obj.next();

		System.out.println("My name is "+name);
	}
}

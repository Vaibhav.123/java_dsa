
//  Count Tokens
//  hasmoreTokens


import java.io.*;
import java.util.*;

class Demo  {
	
	public static void main(String[] args)throws IOException {

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter data");

		String str=sc.nextLine();

		StringTokenizer obj=new StringTokenizer(str,",");

		System.out.println("No of tokens: "+obj.countTokens());

		while(obj.hasMoreTokens()){
			
			System.out.println(obj.nextToken());
		}
	}
}

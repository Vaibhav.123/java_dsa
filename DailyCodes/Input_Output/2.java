//  by default return type is string if we want to take integer as input then we have to write not only next but nextInt

import java.util.Scanner;

class ScannerDemo {
	
	public static void main(String[] args){
		
		Scanner obj=new Scanner(System.in);

		System.out.println("Enter name: ");
		String name=obj.next();

		System.out.println("Enter age: ");
		int age=obj.nextInt();

		System.out.println("My name is "+name);
		System.out.println("My age is "+age);

	}
}

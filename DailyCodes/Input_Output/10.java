//   real time example of stringTokenizer


import java.io.*;
import java .util.*;

class Demo {
	
	public static void main(String[] a)throws IOException  {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Movie Name, imdb, no of ticket:");

		String info=br.readLine();

		StringTokenizer obj = new StringTokenizer (info,",");

		String token1=obj.nextToken();
		float token2=Float.parseFloat(obj.nextToken());
		int token3=Integer.parseInt(obj.nextToken());

		System.out.println("Movie : "+token1);
		System.out.println("IMDB : "+token2);
		System.out.println("No of Tickets : "+token3);


	}
}

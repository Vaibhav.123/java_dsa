
//  using InputStreamReader and BufferedReader 
//  this is only for taking string input and output
//


import java.io.*;

class InputDemo  {
	
	public static void main(String[] args) throws IOException{
		
		InputStreamReader obj = new InputStreamReader(System.in);

		BufferedReader br=new BufferedReader (obj);

		System.out.println("Enter Name:  ");

		String name=br.readLine();
		
		System.out.println("Enter Age:  ");

		int ch=Integer.parseInt(br.readLine());

		System.out.println("My name is "+name);

		System.out.println("My age is "+ch);

	}
}


// throw keyword is used for throw the specific exception 
// using throw we can throw the exception before it occured 
// so the exception handle before it occurs
// mostly it used for userdefined exceptions 
// in the above ex. it used for predefined exceptions
//
import java.util.Scanner;

class ThrowDemo {

	public static void main(String[] arfs) {
	
		Scanner sc = new Scanner(System.in);

		int x = sc.nextInt();

		try {
		
			if(x == 0){
			
				throw new ArithmeticException("Divide by zero");
			}
			System.out.println(10/x);
		
		}catch(ArithmeticException obj) {
			// using this we can print exception like the DEH exception
			// 
			System.out.print("Exception in Thread "+ Thread.currentThread().getName()+" ");
			obj.printStackTrace();
		}
	}
}

// userdefined exceptions using array example
//
//
import java.util.*;

class DataOverflowException extends RuntimeException {

	DataOverflowException(String str){
	
		super(str); // if we canno write this super line then it cannot print the our string 
			    
	}
}

class DataUnderflowException extends RuntimeException {

	DataUnderflowException(String str){
	
		super(str);  // because using super line it will call its parent constructor and passes string
	}

}

class ArrayDemo {

	public static void main(String[] args)  {
	
		Scanner sc = new Scanner(System.in);

		int arr[] = new int[5];

		System.out.println("Enter integer values");
		System.out.println("Note : 0<element<100");

		for(int i=0; i<arr.length; i++){
		
			int data = sc.nextInt();
			
			if(data<0){
			
				throw new DataUnderflowException("Are mitra data 0 peksha lahan ahe");
			}
			if(data>100){
		
				throw new DataOverflowException("Are mitra data 100 peksha motha ahe");
			}

			arr[i]=data;
		}

		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}

//  try catch 
//  why the need of try ctach block 
//  because  using try catch block we can do the abnormal termination to the normal termination 


class Demo  {

	public static void main(String[] args)  {
		
		System.out.println("Start main");
		try{
		
			System.out.println(10/0);
		}catch(ArithmeticException obj) {
		
			System.out.println("Exception occured");
		}
		System.out.println("End main");
	}
}
/* 
 * OP:
 *   Start main
 *   Exception occured
 *   End main

// ensureopen checks that the connection is there or not 
//
import java.io.*;
class IOExceptionDemo {

	public static void main(String[] args)throws IOException  {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str1 = br.readLine();
		System.out.println(str1);

		br.close();
		
		String str2 = br.readLine();
		System.out.println(str2);
		
		}

		//System.out.println(str2);
	}
}
/*  OP:
 *
vaibhav
vaibhav
Exception in thread "main" java.io.IOException: Stream closed
        at java.base/java.io.BufferedReader.ensureOpen(BufferedReader.java:123) 
        at java.base/java.io.BufferedReader.readLine(BufferedReader.java:321)
        at java.base/java.io.BufferedReader.readLine(BufferedReader.java:396)
        at IOExceptionDemo.main(Program1.java:14)
	*/

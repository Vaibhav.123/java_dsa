// finally block
// where the exception occured or not but the finally block will be occured
// in this block mostly connection closed operation are performed 
//

class Demo {
	
	void m1(){
	

	}
	void m2(){
	

	}
	public static void main(String[] args)  {
		
		System.out.println("Start main");
		Demo obj = new Demo();

		obj.m1();

		obj = null;

		try{
			obj.m2();
		}catch(NullPointerException obj1) {
		
			System.out.println("NullPointerException occured");
		}
		finally{
		
			System.out.println("Connections closed");
		}
		System.out.println("End main");
	}
}

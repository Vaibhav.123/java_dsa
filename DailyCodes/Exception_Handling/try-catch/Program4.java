
// we can write multiple catch block also 
// but there is need to take care of parent child relation in exception class
// firstly we must have to write child exception then parent exception
// if we write parent exception first and then child then error will occured
// And also we can write nested try catch block
//
class Demo  {

	public static void main(String[] args)  {
	
		try {
	
		}catch (NumberFormatException obj){
		
			try{
			
			}catch (){
			

			}
		}
		catch(IllegalArgumentException obj){
	
		}
		catch(RuntimeException obj){
		
		}

	}
}

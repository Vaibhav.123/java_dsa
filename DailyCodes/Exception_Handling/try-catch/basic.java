
class Demo {

	public static void main(String[] args) {
	
		try {
		
			// Exception code
		}catch(nameOfTheException obj){    // catch is not method 
		
			// handling code
		}
	}
}
// catch is just special type of method 
// but it is not considered as method because 
// it does stored in method table but
// it will stored in Exception table 
// and exception table is stored on method area

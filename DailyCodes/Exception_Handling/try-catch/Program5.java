

class Demo {

	public static void main(String[] args)  {
		
		try{
			System.out.println(10/0);
		}catch(IOException obj){
			// we cannot write checked exception without meaning 
			// means cant write checked exception without any reason
			// so error will be occured
		
			System.out.println("IOException");
		}
		// but unchecked exception we can write any 
		// whatever thy are
		// we can write unchecked exceptions where the exception is there or not but then also we can write
		//
		catch(ArithmeticException obj){
		
			System.out.println("ArithmeticException");
		}
	} 
}

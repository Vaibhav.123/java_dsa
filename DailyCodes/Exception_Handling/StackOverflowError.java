
// StackOverflowError 
// here suppose we can handle this error, but we cannot handle that error
// because error cant be handle 
// 

class Demo {
	
	static void fun(int x){
	
		System.out.println(x);
		fun(++x);
	}
	public static void main(String[] args)  {
		try {
			fun(1);

		}catch(StackOverflowError obj)  {
		
			System.out.println("StackOverflowError");
		}
	}
}


// ExceptionHandler had three methods to handle the exception
// -- toString()
// -- getMessage()
// -- printStackTrace()
//
import java.io.*;
class Demo {

	void getData() throws IOException {   // 
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int data = 0;
		try {
		
			data = Integer.parseInt(br.readLine());
		}
		catch(NumberFormatException obj){
			
			System.out.println("Exception sapadle");
		//      System.out.println(obj.toString());	
		//	System.out.println(obj.getMessage());
		//	obj.printStackTrace();
		}

		System.out.println(data);

	}

	public static void main(String[] rgs) throws IOException  {
	
		Demo obj = new Demo();
		obj.getData();
	}
}

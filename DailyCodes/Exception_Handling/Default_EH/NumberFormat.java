
// NumberFormatException: 
//
import java.io.*;
class Demo {
	
	void getData()throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int data = Integer.parseInt(br.readLine());

		System.out.println(data);
	}
	public static void main(String[] args) throws IOException {
		
		Demo obj = new Demo();

		obj.getData();
	}
}
/*
vaibhav
Exception in thread "main" java.lang.NumberFormatException: For input string: "vaibhav"
        at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:67)
        at java.base/java.lang.Integer.parseInt(Integer.java:668)
        at java.base/java.lang.Integer.parseInt(Integer.java:786)
        at Demo.getData(NumberFormat.java:10)
        at Demo.main(NumberFormat.java:18)
	*/



class StringDemo  {

	public static void main(String[] args)  {
	
		String str1 = "Vaibhav";	// SCP (String Constant Pool)

		String str2 = new String("Vaibhav");	// Heap

		String str3 = "Vaibhav";	// SCP

		String str4 = new String("Vaibhav");	// Heap

		System.out.println(System.identityHashCode(str1));	// 123456
		
		System.out.println(System.identityHashCode(str1));	// 123456
		
		System.out.println(System.identityHashCode(str2));	// 102030
		
		System.out.println(System.identityHashCode(str2));	// 102030
		
		System.out.println(System.identityHashCode(str3));	// 123456
		
		System.out.println(System.identityHashCode(str4));	// 405060
	

		int var1 = 12;

		int var2 = 200;

		System.out.println(System.identityHashCode(var1)); 	// 123456
		
		System.out.println(System.identityHashCode(var1));	// 123456
		
		System.out.println(System.identityHashCode(var2)); 	// 102030
		
		System.out.println(System.identityHashCode(var2));	// 405060
		
		System.out.println(System.identityHashCode(var2));

	}
}

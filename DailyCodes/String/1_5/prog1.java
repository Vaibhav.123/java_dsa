

class Demo  {

	public static void main(String [] args)  {
	
		String str1 = "Vaibhav";

		String str2 = new String("Vaibhav");

		char str3[] = {'V','a','i','b','h','a','v'};

		System.out.println(str1);	// Vaibhav 
		
		System.out.println(str2);	// Vaibhav 
		
		System.out.println(str3);	// Vaibhav
	}
}

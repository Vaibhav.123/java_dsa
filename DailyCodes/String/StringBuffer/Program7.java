

class StringBufferDemo  {

	public static void main(String[] args)  {
	
		String str1 = "Shashi";

		String str2 = new String("Bagal");

		StringBuffer str3 = new StringBuffer("Core2Web");

		// String str4 = str3.append(str1);  // error: incompatible types: StringBuffer cannot be converted into string 
		
		// String str4 = str1.append(str3);  // error: cannot find symbol : because append method is of StringBuffer
		
		StringBuffer str4 = str3.append(str1);  // return type is StringBuffer object so it required StringBuffer variable to store it
		
		System.out.println(str1);  // Shashi 

		System.out.println(str2);  // Bagal
		
		System.out.println(str3);  // Core2WebShashi , in this it will directly change in which string to append
		
		System.out.println(str4);  // Core2WebShashi
	}
}



//  by using this string buffer,
//  str1 cha ek object create hoto ani prt jrr dusri string jrr str1 sobt concate keli tr tyach object madhe ti string concat keli jate tichysathi dusra
//  object nhi create hot 
//  so tyamul string buffer hi concept use keli jaate


import java.util.*;

class StringBufferDemo {

	public static void main(String[] rgs){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter string");

		StringBuffer str1 = new StringBuffer(sc.next());

		System.out.println(System.identityHashCode(str1));  // 1000
		
		System.out.println("Enter string");
		
		str1.append(sc.next());

		System.out.println(str1);

		System.out.println(System.identityHashCode(str1));  // 1000


	}
}

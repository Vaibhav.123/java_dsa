

class ReverseDemo  {

	public static void main(String[] args)  {
	
		StringBuffer str = new StringBuffer("Vaibhav");

		System.out.println(str.reverse());

		String str1 = "Shashi";

		StringBuffer sb = new StringBuffer(str1);
		
		sb.reverse();

		str1 = sb.toString();

		System.out.println(str1);

	}
}

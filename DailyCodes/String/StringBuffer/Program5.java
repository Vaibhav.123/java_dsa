
class Demo {

	public static void main(String[] args)  {

		StringBuffer sb = new StringBuffer();

		System.out.println(sb.capacity());	// 16

		System.out.println(sb);			//  blank

		sb.append("Gawali");

		System.out.println(System.identityHashCode(sb)); 	// 1000

		System.out.println(sb.capacity());	// 16

		System.out.println(sb);			//  Gawali
		
		sb.append("Vaibhav");
		
		System.out.println(System.identityHashCode(sb));	// 1000
		
		System.out.println(sb.capacity());	// 16

		System.out.println(sb);			//  GawaliVaibhav

		sb.append("Navanath");
		
		System.out.println(System.identityHashCode(sb));	// 1000
		
		System.out.println(sb.capacity());	// 34 = [(16+1) * 2]

		System.out.println(sb);			//  GawaliVaibhavNavanth
	}
}

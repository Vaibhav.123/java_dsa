

class StringBufferDemo  {

	public static void main(String[] args)  {
	
		String str1 = "Shashi";

		String str2 = new String("Bagal");

		StringBuffer str3 = new StringBuffer("Core2Web");

//		String str4 = str1.concat(str3);  // error: because we cannot pass StringBuffer object to String class method concat

		StringBuffer str5 = str3.append(str2);  // but we can pass String class object to StringBuffer to append

		System.out.println(str3);
		
		System.out.println(str5);
	}
}

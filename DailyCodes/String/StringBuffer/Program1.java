
import java.util.*;

class StringBufferDemo  {

	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("enter string");
		String str1 = sc.next();
		
		System.out.println("enter string");
		String str2 = sc.next();
		
		String str3 = str1.concat(str2);   //  new object

		System.out.println(str1);
		System.out.println(str3);
		
	}
}

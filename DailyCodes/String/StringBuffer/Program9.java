
import java.io.*;

class StringBufferDemo {

	public static void main(String[] args) throws IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter string");

		String str1 = br.readLine();

		System.out.println("Enter string");

		String str2 = new String(br.readLine());

		System.out.println("Enter string");

		StringBuffer str3 = new StringBuffer(br.readLine());

		str1.concat(str2);

		str3.append(str2);

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);

	}
}

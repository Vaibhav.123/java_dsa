//  hashCode()
//  this method directly check the content of string and create hashCode
//  if the strings are same then hashCode will be same for all same strings

class Demo {

	public static void main(String[] args)  {
	
		String str1 = "Vaibhav";

		String str2 = new String("Vaibhav");

		String str3 = "Vaibhav";

		String str4 = new String("Vaibhav");

		String str5 = "Shashi";

		String str6 = new String("Shashi");

		String str7 = "Shashi";

		String str8 = new String("Shashi");
		
		System.out.println("Vaibhav:");
		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
		
		System.out.println("Shashi:");
		System.out.println(str5.hashCode());
		System.out.println(str6.hashCode());
		System.out.println(str7.hashCode());
		System.out.println(str8.hashCode());

	}
}

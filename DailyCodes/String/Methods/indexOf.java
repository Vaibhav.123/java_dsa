

// parameters : char ch, int fromIndex
// find first insatnce of character in given string
// return type : int


class Demo {

	public static void main(String[] args){
	
		String str = "Vaibhav";

		System.out.println(str.indexOf('a',0));  // 1
		System.out.println(str.indexOf('v',1));  // 6
	}
}

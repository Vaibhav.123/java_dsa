
//  parameters : char ch, int ToIndex(means kuthparynt find search kr as)
//  return type : int
//  it finds the last index of character 
class LastIndexOf {

	public static void main(String[] args){
	
		String str = "Vaibhav";

		System.out.println(str.lastIndexOf('a',6));  // 5
		System.out.println(str.lastIndexOf('a',3));  // 1
	}
}

//  parameters : int (index of String) or int (start index, end index)
//  return type : String
//

class Demo  {

	public static void main(String[] rgs){
		
		String str = "Vaibhav Gawali";

		System.out.println(str.substring(0));  //  Vaibhav Gawali
		System.out.println(str.substring(8));  //  Gawali
		System.out.println(str.substring(0,7));  // Vaibhav
	}
}


//  parameters : char (old char), char (new char)
//  return type : String


class ReplaceDemo  {

	public static void main(String[] args)  {
	
		String str = "Vaibhav";

		System.out.println(str.replace('v','u'));  //  Vaibhau
	}
}

// ignoreCase()
// it compares two strings (case unsensitive)
// 

class Demo  {

	public static void main(String[] args)  {
	
		String str1 = "VAIBHAV";

		String str2 = new String("Vaibhava");

		System.out.println(str1.compareToIgnoreCase(str2));
	}
}

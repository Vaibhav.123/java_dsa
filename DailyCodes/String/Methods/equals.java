
// .equals()
// it compares the actual data of the string
// if the two strings are equal = true
// if not equal = false


class Demo  {

	public static void main(String[] args) {
	
		String str1 = "Vaibhav";

		String str2 = new String("Vaibhav");

		System.out.println(str1.equals(str2));
	}
}

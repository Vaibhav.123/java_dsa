//  compareTo()
//  this compares two strings (case sensitive)
//  if both strings are equal = 0
//  if two strings are not equal = it gives diff between the ASCII values


class Demo  {

	public static void main(String[] agrs)  {
	
		String str1 = "Vaibhav";

		String str2 = new String("vaibhav");

		System.out.println(str1.compareTo(str2));
	}
}



class StringDemo  {

	public static void main(String[] args)  {
	
		String str1 = "Vaibhav";  // 1000
		
		String str2 = "Vaibhav";   // 1000

		String str3 = new String("Vaibhav");  // 2000
		
		String str4 = new String("Vaibhav");  // 3000
		
		String str5 = new String("Sagar");   // 4000

		String str6 = "Sagar";   // 5000


	}
}



class Demo {

	public static void main(String[] args)  {
	
		String str1 = "Vaibhav";

		String str2 = "Gawali";

		System.out.println(str1);  // Vaibhav
		System.out.println(str2);  // Gawali

		str1.concat(str2);

		System.out.println(str1);  // Vaibhav
		System.out.println(str2);  // Gawali

	}
}  


//  in the below code  
//  when we print name opf character array then it will print string
//  but when we print name of array i.e concat with another string then it will gives us address
//
//  because when we concat object with string the it will internally call [toString()] method
//
//  so it will print address
class StringDemo {

	public static void main(String[] args)  {
	
		char arr[] = {'A','B','C'};

		System.out.println(arr);	//  ABC
		
		System.out.println(arr.toString());  // address

		System.out.println("Array = "+arr);	//  address
		
		System.out.println("Array = "+arr.toString());  // address
	}
}

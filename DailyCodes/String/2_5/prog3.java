

class Demo  {

	public static void main(String[] args)  {
	
		String str1 = "Vaibhav";

		String str2 = str1;

		String str3 = new String(str2);

		System.out.println(str1);  // Vaibhav
		
		System.out.println(str2);   // Vaibhav 
		
		System.out.println(str3);  // Vaibhav
	}
}


import java.io.*;

class ReplaceDemo  {
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	String myReplace(String str, char ch, char ch1) throws IOException {
	
		char arr[] = str.toCharArray();

		int flag=0;

		for(int i=0; i<arr.length; i++){
		
			if(arr[i]==ch){
			
				arr[i]=ch1;
				flag=1;
			}
		}

		String str1 = new String(arr);
		if(flag==0)
			return null;
		else
			return str1;
		
	}

	public static void main(String[] args) throws IOException {
		
		System.out.println("Enter String");
		String str = br.readLine();

		Demo obj = new Demo();

		System.out.println("Enter Char which you want to replace");
		char ch = (char)br.read();
		br.skip(1);

		System.out.println("Enter char to replace");
		char ch1 = (char)br.read();
		
		String str1 = obj.myReplace(str,ch,ch1);

		System.out.println("After replace string : "+str1);
	}
}

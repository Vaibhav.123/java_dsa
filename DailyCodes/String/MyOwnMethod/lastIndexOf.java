import java.io.*;
class LastIndexOfDemo  {
	
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	int myLastIndexOf(String str, char ch, int z) throws IOException {
		
		char arr[] = str.toCharArray();
		int y=-1;
		int flag=0;

		for(int i=0; i<=z; i++)  {
		
			if(arr[i] == ch){
				flag=1;
				y = i;
			}
		}
		if(flag==1)
			return y;
		else
			return -1;
	}
	public static void main(String[] args) throws IOException {
		
		System.out.println("Enter String");
		String str = br.readLine();

		Demo obj = new Demo();

		System.out.println("Enter character");
		char ch = (char)br.read();
		
		br.skip(1);
		
		System.out.println("Enter index");
		int z = Integer.parseInt(br.readLine());
		
		int x = obj.myLastIndexOf(str,ch,z);

		if(x!=-1)
			System.out.println("Last index of String "+str + " is "+x);
		else
			System.out.println("Not found");


	}
}

import java.util.*;

class CompareToDemo  {

	int mycompareTo(String str1, String str2)  {
	
		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();
		int flag=0;
		int x=0;
		for(int i=0; i<arr1.length; i++){
		
			if(arr1[i] == arr2[i])
				flag=0;
			else{
				x=arr1[i]-arr2[i];
				flag=1;
				break;
			}
		}
		if(flag==0)
			return 0;
		else{
		
			return x;
		}
	}

	public static void main(String[] args)  {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter string");

		String str1 = sc.next();
		
		System.out.println("Enter string");

		String str2 = sc.next();

		Demo obj = new Demo();

		int x = obj.mycompareTo(str1,str2);
		if(x==0)
			System.out.println("Strings are equal : "+x);
		else
			System.out.println("Strings sre not equal its diff : "+x);

	}
}

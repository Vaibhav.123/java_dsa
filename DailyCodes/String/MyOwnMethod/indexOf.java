
import java.io.*;

class IndexOfDemo {
	
	int myindexOf(String str, char ch, int x)  throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		char arr[] = str.toCharArray();

		int y=-1,flag=0;
		for(int i=x; i<arr.length; i++){
		
			if(arr[i] == ch){
				flag=0;
				y=i;
				break;
			}else{
			
				flag=1;
			}
		}
		if(flag==0)
			return y;
		else
			return -1;

	}
	public static void main(String[] as)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		String str = br.readLine();

		Demo obj = new Demo();
		
		System.out.println("enter character to find 1st insatnce");
		char ch = (char)br.read();
		
		System.out.println("enter index to starts from");
		br.skip(1);
		int  x = Integer.parseInt(br.readLine());

		int ret = obj.myindexOf(str,ch,x);

		if(ret==-1)
			System.out.println("not found at");
		else
			System.out.println("found at index : "+ret);
	}
}

import java.util.*;

class CharAt  {
	
	char mycharAt(String str,int x)  {
	
		char arr[] = str.toCharArray();

		return arr[x];
	}

	public static void main(String[] args)  {
	
		Scanner sc= new Scanner(System.in);

		System.out.println("Enter string");

		String str = sc.next();

		Demo obj = new Demo();
		System.out.println("Enter index");
		int x = sc.nextInt();

		char ch = obj.mycharAt(str,x);

		System.out.println(ch);
	}
}

import java.io.*;
class SubstringDemo  {
	
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	String mysubstring1(String str,int x, int y) throws IOException{
		
		char arr[] = str.toCharArray();
		char arr1[] = new char[arr.length];
		
		for(int i=x; i<y; i++){
			
			arr1[i]=arr[i];
			
		}
		String str1 = new String(arr1);
		return str1;
	}
	
	String mysubstring2(String str,int x) throws IOException{
		
		char arr[] = str.toCharArray();
		char arr1[] = new char[arr.length];
		
		for(int i=x; i<arr.length; i++){
			
			arr1[i]=arr[i];
			
		}
		String str1 = new String(arr1);
		return str1;
	}
	public static void main(String[] args) throws IOException {
	
		System.out.println("Enter String");
		String str = br.readLine();

		Demo obj = new Demo();

		System.out.println("Enter index from start");
		int x = Integer.parseInt(br.readLine());
		
		System.out.println("Enter index from start");
		int y = Integer.parseInt(br.readLine());

		if(x!=0 && y==0){
			String str2 = obj.mysubstring2(str,x);
			System.out.println("Substring = "+str2);
		}
		else if(x==y){
		
			System.out.println("No substring");
		}
		else{
			String str1 = obj.mysubstring1(str,x,y);

			System.out.println("Substring = "+str1);
		}
	}
}

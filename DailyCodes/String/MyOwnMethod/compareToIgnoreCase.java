
import java.util.*;

class CompareToIgnoreCaseDemo  {
	
	int mycompareToIgnoreCase(String str1, String str2)  {
	
		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();
		int flag=0;
		int x=0;
		for(int i=0; i<arr1.length; i++)  {
			if(arr1.length == arr2.length){
				if(arr1[i] == arr2[i] || arr1[i] == arr2[i]+32 || arr1[i]+32 == arr2[i]) {
				
					flag=0;
				
				}else{
				
					flag=1;
					break;
				}
			}else{
			
				flag=2;
			}
		}
		if(flag==0)
			return 0;
		else if(flag==1)
			return 1000;
		else
			return arr1.length-arr2.length;

	
	}
	public static void main(String[] args)  {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter string");
		String str1 = sc.next();
		
		System.out.println("Enter string");
		String str2 = sc.next();

		Demo obj = new Demo();
		int x = obj.mycompareToIgnoreCase(str1,str2);
		if(x==0)
	 		System.out.println("both strings are equal : "+x);
		else if(x==1000)
			System.out.println("Not equal but length equal : 0");
		else
			System.out.println("Diff : "+x);

	}
}

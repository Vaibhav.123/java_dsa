class Solution {
    public void sortColors(int[] nums) {

        int j = 0;
        int num = nums.length - 1;

        for (int i = 0; i <= num;){
            
            if (nums[i] == 0)
                swap(nums, i++, j++);
            
            else if (nums[i] == 1)
                ++i;
            
            else
                swap(nums, i, num--);
        }
    }

    void swap(int[] nums, int i, int k) {
        int temp = nums[i];
        nums[i] = nums[k];
        nums[k] = temp;
    }
}

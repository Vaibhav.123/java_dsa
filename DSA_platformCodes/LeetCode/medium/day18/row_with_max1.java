
class Solution {
    int rowWithMax1s(int arr[][], int n, int m) {
        
        int count = 0;
        int index  =-1;
        for(int i=0; i<n; i++){
            int count1 = 0;
            for(int j=0; j<m; j++){
                
                if(arr[i][j] == 1){
                    count1++;
                }
            }
            if(count < count1){
                count = count1;
                index = i;
            }
        }
        return index;
    }
}

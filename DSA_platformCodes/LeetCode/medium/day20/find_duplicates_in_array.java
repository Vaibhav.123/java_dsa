class Solution {
    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> ll = new ArrayList<Integer>();

        Map<Integer,Integer> al = new HashMap<Integer,Integer>();

        for(int i=0; i<nums.length; i++){
            int count = 1;
            if(al.containsKey(nums[i])){
                count++;
            }
            if(count > 1){
                ll.add(nums[i]); 
                count = 1;
            }
            al.put(nums[i],i);
        }

        return ll;
    }
}

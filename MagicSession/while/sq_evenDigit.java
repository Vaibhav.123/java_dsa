//  WAP to print the square root of even digit of given number
//
//
//
class Square{
	
	public static void main(String[] s){
		
		int N=123452;
		int x=N;

		while(N!=0){
			int rem=N%10;

			if(rem%2==0){
			
				System.out.println(rem*rem);
			}
			N=N/10;
		}
	}
}

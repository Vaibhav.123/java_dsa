//  WAP to print the reverse number of given number
//


class Rev {
	
	public static void main(String[] s){
		
		int N=12345;
		int x=N;
		int rev=0;

		while(N!=0){
			
			int rem=N%10;
			rev=rev*10+rem;
			N=N/10;
		}
		System.out.println("Reverse number of "+x+" is ==> "+rev);
	}
}

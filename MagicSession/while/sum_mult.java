//  WAP to print the sum of even number and multiplication of odd number 
//  between given range 1 to 10
//



class Range {
	
	public static void main(String[] s){
		
		int i=15;
		int x=i;
		int j=10;
		int sum=0;
		int mult=1;
		if(i<j){
			while(i<=j){
			
				if(i%2==0){
					sum=sum+i;	
				}else{
					mult=mult*i;
				}
				i++;
			}
		}else 
		{
		
			while(i>=j){
			
				if(i%2==0){
					sum=sum+i;	
				}else{
					mult=mult*i;
				}
				i--;
			}
		}
		System.out.println("Sum of even numbers between "+x+" and "+j+" is "+sum);
		System.out.println("Multiplication of even numbers between "+x+" and "+j+" is "+mult);
	}
}

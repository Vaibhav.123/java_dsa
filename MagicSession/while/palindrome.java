//  WAP to check given number is palindrome or not


class Palindrome {
	
	public static void main(String[] s){
		
		int N=1232;
		int original=N;
		int rev=0;

		while(N!=0){
			
			int rem=N%10;
			rev=rev*10+rem;
			N=N/10;
		}
		if(original==rev){
			
			System.out.println(rev+ " is palindrome number");
		}else{
			
			System.out.println(original+" is not palindrome number");
		}
	}
}

//  WAP to calculate count odd digit of number 




class Odd{
	
	public static void main(String[] s){
		
		int N=123456;
		int x=N;
		int count=0;

		while(N!=0){
			
			int rem=N%10;

			if(rem%2!=0){
				count++;
			}
			N=N/10;
		}
		System.out.println("Odd digit count of "+x+ " is "+count);
	}
}

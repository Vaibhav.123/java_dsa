/* 
     5  4  3  2  1
     8  6  4  2
     9  6  3
     8  4
     5

     */


import java.io.*;

class Demo {
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number of rows");

		int row=Integer.parseInt(br.readLine());
		int num=row;
		int count=1;

		for(int i=1; i<=row; ){
			
			for(int j=row; j>=i; j--){

				System.out.print(num+"    ");	
				num=num-i;
				count++;
			}
			i++;
			num=i*(count-2);
			count=1;
			
	
			System.out.println();
		}
	}
}

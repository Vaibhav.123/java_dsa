/*
     0
     1   1
     2   3   5
     8   13  21  34
   */




import java.util.*;

class Demo  {
 
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter number of rows: ");
		int row=sc.nextInt();
		int prev=0;
		int curr=1;
		int sum=0;

		for(int i=1; i<=row; i++){
			
			for(int j=1; j<=i; j++){
				
				System.out.print(sum+"   ");
				prev=curr;
				curr=sum;
				sum=prev+curr;		

			}
			System.out.println();
		}
	}
}

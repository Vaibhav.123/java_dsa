
import java.io.*;

class Demo  {
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter number");
		int num=Integer.parseInt(br.readLine());
		int fact=1;
		int sum=0;

		while(num!=0){
			
			int rem=num%10;
			for(int i=1; i<=rem; i++){
				
				fact=fact*i;
			}

			sum=fact+sum;
			num=num/10;


		}
		System.out.println("Sum : "+sum);
	}
}

//  take range from user and print even numbver in reverse order and
//  print odd number standard way

import java.io.*;

class Demo  {
	
	public static void main(String [] args) throws IOException {
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("enter start :  ");
		int start = Integer.parseInt(br.readLine());
		System.out.print("enter end :  ");
		int end = Integer.parseInt(br.readLine());
		
		System.out.println("Odd numbers:  ");
		for(int i=start; i<=end; i++){
			
			if(i%2!=0){
				
				System.out.print(i+"  ");
			}
		}
		System.out.println("\n"+"Even Numbers:  ");
		for(int j=end; j>=start; j--){
			
			if(j%2==0){
		
				System.out.print(j+"  ");
					
			}
		}
		System.out.println();
		
	}
}

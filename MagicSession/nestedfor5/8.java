/* 
	$
	@   @
	&   &   &
      #   #   #   #
	$   $   $   $   $
	@   @   @   @   @   @
	&   &   &   &   &   &   &
      #   #   #   #   #   #   #   #
*/


import java.util.*;

class Demo  {
	
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows: ");
		int row=sc.nextInt();

		for(int i=1; i<=row; i++){
			
			for(int j=1; j<=i; j++){
			
				if(i%2==1)
					System.out.print("$   ");
				else if(i%2==0)
					System.out.print("@   ");
				else if(i%3==0)
					System.out.print("&   ");
				else if(i%4==0)
					System.out.print("#   ");
			}
			System.out.println();
		}
	}
}

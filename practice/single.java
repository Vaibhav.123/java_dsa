/*  1  2  3 
 *  1  2  3
 *  1  2  3
 */


class Demo  {
	
	public static void main(String[] args){
		int x=1;
		for(int i=1; i<=9; i++){
			
			System.out.print(x++ +"  ");

			if(i%3==0){
				
				System.out.println();
				x=1;
			}
		}
	}
}
